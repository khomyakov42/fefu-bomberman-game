from urllib import urlencode
from urllib2 import urlopen, HTTPError
from websocket import create_connection
from functools import wraps
import simplejson
import unittest
import time
TEST_URL = None


STATUS_OK = 'ok'
STATUS_MALFORMED = 'malformed_request'
STATUS_BAD_ACTION = 'unsupported_action'
STATUS_USER_EXIST = 'user_already_exists'
STATUS_BAD_LOGIN_OR_PASSWORD = 'invalid_credentials'
STATUS_BAD_SID = 'invalid_sid'
STATUS_NOT_IN_GAME = 'not_in_game'
STATUS_ALREADY_IN_GAME = 'already_in_game'
STATUS_MAP_NOT_FOUND = 'map_not_found'
STATUS_BONUS_NOT_SPECIFIED = 'item_not_specified'
STATUS_INVALID_BONUS = 'invalid_item_description'
STATUS_BAD_MAX_PLAYER = 'invalid_player_count'
STATUS_GAME_NOT_FOUND = 'game_not_found'
STATUS_GAME_STARTED = 'game_already_started'
STATUS_BAD_PASSWORD_GAME = 'wrong_game_password'
STATUS_GAME_FULL = 'game_is_full'
STATUS_ALREADY_STARTED = 'game_already_started'
STATUS_MAP_NOT_FOUND = 'map_not_found'
EVENT_AUTH_ERROR = 'auth_error'
EVENT_AUTH_SUCCESS = 'auth_success'
EVENT_PLAYER_JOINED = 'player_joined_game'
EVENT_PLAYER_LEFT = 'player_left_game'
EVENT_NEW_MESSAGE = 'new_message'
EVENT_UPDATE_MAP = 'game_state'
EVENT_GAME_STARTED = 'game_started'
EVENT_GAME_ACTION = 'game_action'
EVENT_PROTOCOL_VIOLATION = 'protocol_violation'
EVENT_SERVER_ERROR = 'internal_server_error'
EVENT_ANOTHER_CONNECTION_OPEN = 'another_connection_opened'
EVENT_GAME_END = 'game_end'
EVENT_DISCONNECTION = 'disconnection'

EXTENSIONS = 'extensions'
REGISTER = 'register'
LOGIN = 'login'
LOGOUT = 'logout'
PLAYER_STATE = 'player_state'
SEND_MESSAGE = 'send_message'
CREATE_GAME = 'create_game'
LIST_GAMES = 'list_games'
JOIN_GAME = 'join_game'
LEAVE_GAME = 'leave_game'
START_GAME = 'start_game'
GET_MAP = 'get_map'
UPLOAD_MAP = 'upload_map'
LIST_MAPS = 'list_maps'
AUTH = 'auth'
GAME_ACTION = 'game_action'


DEBUG_MODE = 'game_set_debug_mode'
SET_TICK = 'game_set_tick'
DEBUG_ACTION = 'debug_game_action'
RESET = 'reset'


def register_and_login(usernames):
    def wrapper(func):
        @wraps(func)
        def decorator(self, *args, **kwargs):
            for username in usernames:
                self.register(username, username)
                self.login(self.clients[-1])
            return func(self, *args, **kwargs)
        return decorator
    return wrapper


class Client(object):
    def __init__(self, login, password):
        self.login = login
        self.password = password
        self.sid = None
        self.game_id = None
        self.socket_url = None
        self.socket = None
        self.timeout = None
        self.socket_responses = []


    def set_timeout(self, timeout):
        self.timeout = timeout


    def socket_connect(self, url):
        self.socket_url = url
        self.socket = create_connection(self.socket_url, self.timeout)


    def socket_connected(self):
        return self.socket is not None and self.socket.connected


class Tests(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        self.game_server_url = TEST_URL
        self.timeout = 10000
        self.clients = []
        self.response = None
        self.socket_url = None
        super(Tests, self).__init__(*args, **kwargs)


    def setUp(self):
        self.reset()


    def tearDown(self):
        for client in self.clients:
            if client.socket and client.socket.connected:
                client.socket.close()

        self.clients = []
        self.response = None


    def http(self, action, data=None):
        data = data or {}
        data['action'] = action
        data = simplejson.dumps(data)
        cb = 'callback'
        try:
            response = urlopen(self.game_server_url + '?' + urlencode(dict(q=data, callback=cb)), timeout=self.timeout)
            body = response.read()
            self.response = simplejson.loads(body[body.index('(') + 1:body.rindex(')')])
        except (HTTPError, simplejson.JSONDecodeError), e:
            self.response = None
            self.fail(str(e))
        return self.response


    def send_socket(self, client, action, data):
        if not client.socket_connected():
            client.set_timeout(self.timeout)
            client.socket_connect(self.socket_url)

        data = data or {}
        data['event'] = action
        data = simplejson.dumps(data)
        client.socket.send(data)


    def wait_socket_action(self, client, action):
        s = time.time()
        self.response = None
        for i in xrange(len(client.socket_responses)):
            if client.socket_responses['event'] == action:
                self.response = client.socket_responses.pop(i)
                return

        if not client.socket_connected():
            client.socket_connect(self.socket_url)
            if action != AUTH:
                self.auth(client)
                self.auth_success(client)

        while time.time() - s < self.timeout and client.socket.connected:
            data = client.socket.recv()
            try:
                data = simplejson.loads(data)
                if 'event' in data and data['event'] == action:
                    self.response = data
                    return
                client.socket_responses.append(data)
            except simplejson.JSONDecodeError:
                self.fail('%s is not json' % data)

        self.fail('Connection timeout')


    def check_game_settings(self, response):
        self.assertIsInstance(response, dict)
        for field, type in dict(max_slide_length=int, player_base_speed=int, bomb_speed=int, bomb_delay=int,
            bomb_chain_reaction_delay=int, blast_wave_duration=int, cell_decay_duration=int, item_drop_probability=int,
            items=dict).items():
            self.assertIn(field, response)
            self.assertIsInstance(response[field], type)

        self.assertIn('items', response)
        self.check_bonus_setting(response['items'])


    def check_bonus_setting(self, response):
        self.assertIsInstance(response, dict)
        bonuses = ['bomb', 'flame', 'goldenflame', 'speed', 'kicker', 'throw']
        diseases = ['invert', 'lightspeed', 'autobomb', 'nobomb', 'turtle']

        for bonus in bonuses + diseases:
            self.assertIn(bonus, response)
            self.assertIsInstance(response[bonus], dict)

            if bonus in diseases:
                add = ['duration']
            else:
                add = []

            if bonus in ['lightspeed', 'turtle']:
                add.append('speed')
            elif bonus == 'speed':
                add.append('speed_bonus')
            elif bonus in ['flame', 'goldenflame']:
                add.append('flame_bonus')
            elif bonus == 'bomb':
                add.append('bomb_bonus')

            for field in ['weight', 'start_count', 'max_count'] + add:
                self.assertIn(field, response[bonus])
                self.assertIsInstance(response[bonus][field], int)
                self.assertGreaterEqual(response[bonus][field], 0)
                #TODO check max count


    def check_server_map(self, response):
        self.assertIsInstance(response, dict)
        self.assertIn('id', response)
        self.assertIsInstance(response['id'], int)
        self.assertIn('max_player_count', response)
        self.assertIsInstance(response['max_player_count'], int)
        self.check_map(response)


    def check_map(self, response):
        self.assertIsInstance(response, dict)
        self.assertIn('name', response)
        self.assertIsInstance('name', unicode)
        self.assertIn('settings', response)
        self.check_game_settings(response['settings'])
        self.assertIn('field', response)
        self.assertIsInstance(response['field'], dict)
        self.check_game_field(response['field'])


    def check_game_field(self, response):
        self.assertIsInstance(response, list)
        l = None
        for line in response:
            try:
                self.assertIsInstance(line, unicode)
            except:
                self.assertIsInstance(line, str)
            if l in None:
                l = len(line)
            else:
                self.assertEqual(line, l)


    def assertStatus(self, status):
        self.assertIn('status', self.response)
        self.assertEqual(self.response['status'], status)


    def assertPosition(self, player_index, x, y):
        self.assertEqual(self.response['game_state']['players'][player_index]['x'], x)
        self.assertEqual(self.response['game_state']['players'][player_index]['y'], y)


    def extensions(self, check=True):
        self.http(EXTENSIONS)
        if check:
            self.assertIn('status', self.response)
            self.assertEqual(self.response['status'], STATUS_OK)
            self.assertIn('extensions', self.response)
            self.assertIsInstance(self.response['extensions'], dict)
            for k, v in self.response['extensions'].items():
                try:
                    self.assertIsInstance(k, unicode)
                except:
                    self.assertIsInstance(k, str)

            self.assertIn('core.v0', self.response['extensions'])
            self.socket_url = self.response['extensions']['core.v0']['websocket_url']
            self.core = self.response['extensions']['core.v0']


    def register(self, login, password, check=True):
        self.http(REGISTER, dict(
            username=login,
            password=password
        ))

        if check:
            self.assertIn('status', self.response)
            self.assertEqual(self.response['status'], STATUS_OK)
            client = Client(login, password)
            client.set_timeout(self.timeout)
            self.clients.append(client)


    def login(self, client, check=True):
        self.http(LOGIN, dict(
            username=client.login,
            password=client.password
        ))
        if check:
            self.assertIn('status', self.response)
            self.assertEqual(self.response['status'], STATUS_OK)
            self.assertIn('sid', self.response)
            self.assertRegexpMatches(self.response['sid'], r'^[a-zA-Z0-9]{32}$')
            client.sid = self.response['sid']


    def logout(self, client, check=True):
        self.http(LOGOUT, dict(
            sid=client.sid
        ))

        if check:
            self.assertIn('status', self.response)
            self.assertEqual(STATUS_OK, self.response['status'])


    def player_state(self, client, check=True):
        self.http(PLAYER_STATE, dict(
            sid=client.sid
        ))

        if check:
            self.assertIn('status', self.response)
            self.assertEqual(self.response['status'], STATUS_OK)
            self.assertIn('state', self.response)
            self.assertIn(self.response['state'], ['not_in_game', 'in_game', 'in_started_game'])
            if self.response['state'] != 'not_in_game':
                self.assertIn('game_id', self.response)
                self.assertIsInstance(self.response['game_id'], int)
                self.assertIn('game_name', self.response)
                self.assertIsInstance(self.response['game_name'], unicode)


    def send_message(self, client, message, check=True):
        self.http(SEND_MESSAGE, dict(
            sid=client.sid,
            message=message
        ))

        if check:
            self.assertIn('status', self.response)
            self.assertIn(self.response['status'], [STATUS_OK, STATUS_NOT_IN_GAME])


    def create_game(self, client, name, password, map_id, max_player_count, settings=None, check=True):
        self.http(CREATE_GAME, dict(
            sid=client.sid,
            name=name,
            password=password,
            map_id=map_id,
            max_player_count=max_player_count,
            settings=settings or self.core['default_game_settings']
        ))

        if check:
            self.assertIn('status', self.response)
            self.assertIn(self.response['status'], [STATUS_OK])
            self.assertIn('game_id', self.response)
            self.assertIsInstance(self.response['game_id'], int)
            client.game_id = self.response['game_id']


    def list_games(self, client, check=True):
        self.http(LIST_GAMES, dict(
            sid=client.sid
        ))
        if check:
            self.assertIn('status', self.response)
            self.assertIn(self.response['status'], [STATUS_OK])
            self.assertIn('games', self.response)
            self.assertIsInstance(self.response['games'], list)
            for game in self.response['games']:
                for field, type in [('id', int), ('name', unicode), ('total_slots', int),
                                    ('free_lots', int), ('has_password', bool), ('map_id', int)]:
                    self.assertIn(field, game)
                    self.assertIsInstance(game[field], type)


    def join_game(self, client, game_id, password='', check=True):
        self.http(JOIN_GAME, dict(
            sid=client.sid,
            game_id=game_id,
            password=password
        ))

        if check:
            self.assertIn('status', self.response)
            self.assertIn(self.response['status'], [STATUS_OK])
            self.assertIn('map_id', self.response)
            self.assertIsInstance(self.response['map_id'], int)
            self.assertIn('settings', self.response)
            self.check_game_settings(self.response['settings'])


    def leave_game(self, client, check=True):
        self.http(LEAVE_GAME, dict(
            sid=client.sid
        ))

        if check:
            self.assertIn('status', self.response)
            self.assertEqual(self.response['status'], STATUS_OK)


    def start_game(self, client, check=True):
        self.http(START_GAME, dict(
            sid=client.sid
        ))

        if check:
            self.assertIn('status', self.response)
            self.assertEqual(self.response['status'], STATUS_OK)


    def get_map(self, client, map_id, check=True):
        self.http(GET_MAP, dict(
            sid=client.sid,
            map_id=map_id
        ))
        if check:
            self.assertIn('status', self.response)
            self.assertEqual(self.response['status'], STATUS_OK)
            self.check_server_map(self.response)



    def list_maps(self, client, check=True):
        self.http(LIST_MAPS, dict(
            sid=client.sid
        ))

        if check:
            self.assertIn('status', self.response)
            self.assertEqual(self.response['status'], STATUS_OK)
            self.assertIn('maps', self.response)
            self.assertIsInstance(self.response['maps'], list)
            for map in self.response['maps']:
                self.assertIn('map_id', map)
                self.assertIsInstance(map['map_id'], int)
                self.assertIn('name', map)
                self.assertRegexpMatches(map['name'], r'^.{1,32}$')


    def upload_map(self, client, map, check=True):
        if 'settings' not in map:
            map['settings'] = self.core['default_game_settings']

        req = dict(
            sid=client.sid
        )
        req.update(map)
        self.http(UPLOAD_MAP, req)

        if check:
            self.assertIn('status', self.response)
            self.assertEqual(self.response['status'], STATUS_OK)
            self.assertIn('map_id', self.response)
            self.assertIsInstance(self.response['map_id'], int)


    def new_message(self, client):
        self.wait_socket_action(client, EVENT_NEW_MESSAGE)


    def player_joined_game(self, client):
        self.wait_socket_action(client, EVENT_PLAYER_JOINED)


    def vote_start(self, client):
        self.wait_socket_action(client, 'vote_start')


    def player_left_game(self, client):
        self.wait_socket_action(client, EVENT_PLAYER_LEFT)


    def game_started(self, client):
        self.wait_socket_action(client, EVENT_GAME_STARTED)


    def protocol_violation(self, client):
        self.wait_socket_action(client, EVENT_PROTOCOL_VIOLATION)


    def internal_server_error(self, client):
        self.wait_socket_action(client, EVENT_SERVER_ERROR)


    def auth_error(self, client):
        self.wait_socket_action(client, EVENT_AUTH_ERROR)


    def auth_success(self, client):
        self.wait_socket_action(client, EVENT_AUTH_SUCCESS)


    def another_connection_opened(self, client):
        self.wait_socket_action(client, EVENT_ANOTHER_CONNECTION_OPEN)


    def game_state(self, client):
        self.wait_socket_action(client, EVENT_UPDATE_MAP)


    def game_end(self, client):
        self.wait_socket_action(client, EVENT_GAME_END)


    def auth(self, client):
        self.send_socket(client, AUTH, dict(
            sid=client.sid
        ))


    def game_action(self, client, direction, bombing=False):
        self.send_socket(client, GAME_ACTION, dict(
            direction=direction,
            action='place_bomb' if bombing else 'none'
        ))


    def game_set_debug_mode(self, client):
        self.http(DEBUG_MODE, dict(
            sid=client.sid,
            random_seed=1
        ))
        self.assertStatus(STATUS_OK)


    def set_tick(self, client, tick):
        self.http(SET_TICK, dict(
            sid=client.sid,
            tick=tick
        ))

        self.assertStatus(STATUS_OK)


    def debug_game_action(self, client, direction, bombing=False):
        self.http(DEBUG_ACTION, dict(
            sid=client.sid,
            game_action=dict(
                direction=direction,
                action='place_bomb' if bombing else 'none'
            )
        ))

        self.assertStatus(STATUS_OK)


    def reset(self):
        self.http(RESET)
        self.assertStatus(STATUS_OK)