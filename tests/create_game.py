from utils import *

class UploadMap(Tests):
    @register_and_login(['test'])
    def test_simple(self):
        self.extensions()
        self.upload_map(self.clients[0], dict(
            name='test',
            field=[
                '0.....',
                '......',
                '..1...'
            ]
        ))



class CreateGame(Tests):
    @register_and_login(['test'])
    def test_simple(self):
        self.extensions()
        self.upload_map(self.clients[0], dict(
            name='test',
            field=[
                '0.....',
                '......',
                '..1...'
            ]
        ))
        self.create_game(self.clients[0], 'test', '', self.response['map_id'], 2)
