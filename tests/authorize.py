from utils import *


class Register(Tests):
    def test_simple(self):
        self.register('test', 'test', check=False)
        self.assertStatus(STATUS_OK)


    def test_double_register(self):
        self.register('test', 'test')
        self.register('test', 'test', check=False)
        self.assertStatus(STATUS_USER_EXIST)


    def test_register_user_exist(self):
        self.register('test', 'sadasda')
        self.register('test', 'sadsagf', check=False)
        self.assertStatus(STATUS_USER_EXIST)


    def test_username_is_empty(self):
        self.register('', 'test', check=False)
        self.assertStatus(STATUS_MALFORMED)


    def test_username_is_to_long(self):
        self.register(''.join(['t'] * 16), 'test', check=False)
        self.assertStatus(STATUS_MALFORMED)


    def test_username_is_to_short(self):
        self.register('tt', 'test', check=False)
        self.assertStatus(STATUS_MALFORMED)


    def test_password_is_to_long(self):
        self.register('test', ''.join(['t' * 257]), check=False)
        self.assertStatus(STATUS_MALFORMED)


    def test_password_is_empty(self):
        self.register('test', '', check=False)
        self.assertStatus(STATUS_MALFORMED)


    def test_pass_password_and_username(self):
        self.http(REGISTER, dict())
        self.assertStatus(STATUS_MALFORMED)



class Login(Tests):
    def test_simple(self):
        self.register('test', 'test')
        self.login(self.clients[0])


    def test_username_is_empty(self):
        self.register('test', 'test')
        self.clients[0].login = ''
        self.login(self.clients[0], check=False)
        self.assertStatus(STATUS_MALFORMED)


    def test_username_is_to_long(self):
        self.register('test', 'test')
        self.clients[0].login = ''.join(['t'] * 200)
        self.login(self.clients[0], check=False)
        self.assertStatus(STATUS_MALFORMED)


    def test_username_is_to_short(self):
        self.register('test', 'test')
        self.clients[0].login = 'te'
        self.login(self.clients[0], check=False)
        self.assertStatus(STATUS_MALFORMED)


    def test_password_is_to_long(self):
        self.register('test', 'test')
        self.clients[0].password = ''.join(['t' * 257])
        self.login(self.clients[0], check=False)
        self.assertStatus(STATUS_MALFORMED)


    def test_password_is_empty(self):
        self.register('test', 'test')
        self.clients[0].password = ''
        self.login(self.clients[0], check=False)
        self.assertStatus(STATUS_MALFORMED)


    def test_pass_password_and_username(self):
        self.register('test', 'test')
        self.http(LOGIN, dict())
        self.assertStatus(STATUS_MALFORMED)


    def test_double_login(self):
        self.register('test', 'test')
        self.login(self.clients[0])
        self.login(self.clients[0])


class Logout(Tests):
    def test_simple(self):
        self.register('test', 'test')
        self.login(self.clients[0])
        self.logout(self.clients[0])


    def test_is_not_logged_in(self):
        self.register('test', 'test')
        self.http(LOGOUT,  dict(
            sid='1234567890123456789012'
        ))
        self.assertStatus(STATUS_MALFORMED)


    def test_sid_is_to_less(self):
        self.register('test', 'test')
        self.http(LOGOUT, dict(
            sid='123456789012345678901'
        ))
        self.assertStatus(STATUS_MALFORMED)


    def test_sid_is_to_long(self):
        self.register('test', 'test')
        self.http(LOGOUT, dict(
            sid='12345678901234567890123'
        ))
        self.assertStatus(STATUS_MALFORMED)

    def test_bad_sid(self):
        self.register('test', 'test')
        self.login(self.clients[0])
        self.http(LOGOUT, dict(
            sid=self.clients[0].sid[::-1]
        ))
        self.assertStatus(STATUS_BAD_SID)


    def test_sid_is_empty(self):
        self.register('test', 'test')
        self.login(self.clients[0])
        self.http(LOGOUT, dict())
        self.assertStatus(STATUS_MALFORMED)