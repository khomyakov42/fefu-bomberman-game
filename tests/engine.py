from utils import *
from functools import wraps
from itertools import *

EMPTY_MAP_3x3 = [
    '0.....',
    '......',
    '....1.'
]

GAME_SETTINGS = {
    'max_slide_length': 7000,
    'player_base_speed': 500,
    'bomb_speed': 900,
    'bomb_delay': 40,
    'bomb_chain_reaction_delay': 4,
    'blast_wave_duration': 20,
    'cell_decay_duration': 10,
    'item_drop_probability': 45,
    'items': {
        'bomb': {
            'weight': 100,
            'start_count': 0,
            'max_count': 20,
            'bomb_bonus': 1
        },

        'flame': {
            'weight': 100,
            'start_count': 0,
            'max_count': 100,
            'flame_bonus': 1
        },

        'goldenflame': {
            'weight': 100,
            'start_count': 0,
            'max_count': 100,
            'flame_bonus': 100
        },

        'speed': {
            'weight': 100,
            'start_count': 0,
            'max_count': 28,
            'speed_bonus': 500
        },

        'kicker': {
            'weight': 0,
            'start_count': 0,
            'max_count': 100
        },

        'throw': {
            'weight': 0,
            'start_count': 0,
            'max_count': 100
        },

        'invert': {
            'weight': 100,
            'start_count': 0,
            'max_count': 100,
            'duration': 450
        },

        'lightspeed': {
            'weight': 50,
            'start_count': 0,
            'max_count': 450,
            'speed': 10000,
            'duration': 1000
        },

        'autobomb': {
            'weight': 50,
            'start_count': 0,
            'max_count': 100,
            'duration': 450
        },

        'nobomb': {
            'weight': 50,
            'start_count': 0,
            'max_count': 100,
            'duration': 450
        },

        'turtle': {
            'weight': 50,
            'start_count': 0,
            'max_count': 100,
            'speed': 100,
            'duration': 450
        }
    }
}

def create_users_and_game(usernames, field, settings=None, setting_fields=None):
    def wrapper(func):
        @wraps(func)
        @register_and_login(usernames)
        def decorator(self, *args, **kwargs):
            self.extensions()
            req = dict(
                name='test',
                field=field
            )

            if settings:
                kv = setting_fields or {}
                for key, val in kv.items():
                    d = settings
                    keys = key.split('.')
                    for i, k in enumerate(keys):
                        if d.has_key(k):
                            if len(keys) - 1 == i:
                                d[k] = val
                                break
                            else:
                                d = d[k]
                        else:
                            break


                req.update(dict(settings=settings))

            self.upload_map(self.clients[0], req)
            map_id = self.response['map_id']
            self.create_game(self.clients[0], 'test', '', map_id, 2, settings)
            game_id = self.response['game_id']
            self.game_set_debug_mode(self.clients[0])

            for client in self.clients[1:]:
                self.join_game(client, game_id)

            for client in self.clients:
                self.auth(client)
                self.auth_success(client)
                self.start_game(client)

            for client in self.clients:
                self.game_started(client)
                client.number = self.response['current_player_index']

            return func(self, *args, **kwargs)
        return decorator
    return wrapper



class Move(Tests):
    def get_player_by_position(self, x, y):
        self.set_tick(self.clients[0], 0)
        pl_pos = self.response['game_state']['players']
        return filter(lambda c: pl_pos[c.number]['x'] == x and pl_pos[c.number]['y'] == y, self.clients)[0]


    @create_users_and_game(['pl1', 'pl2'], EMPTY_MAP_3x3, GAME_SETTINGS)
    def test_simple(self):
        player = self.get_player_by_position(0, 0)
        delta = 20000 / GAME_SETTINGS['player_base_speed']
        t = 0
        for d, x, y in (('right', 20000, 0), ('down', 20000, 20000), ('up', 20000, 0), ('left', 0, 0)):
            self.debug_game_action(player, d)
            t += delta
            self.set_tick(player, t)
            self.assertPosition(player.number, x, y)


    @create_users_and_game(['pl1', 'pl2'], EMPTY_MAP_3x3, GAME_SETTINGS)
    def test_out_of_range(self):
        pl = self.get_player_by_position(0, 0)

        delta = 10
        dt = 20000 / GAME_SETTINGS['player_base_speed'] + delta
        t = 0
        for d, x, y in (('down', 0, 20000), ('right', 20000, 20000), ('up', 20000, 0), ('left', 0, 0)):
            t += dt
            self.debug_game_action(pl, d)
            self.set_tick(pl, t)
            self.assertPosition(pl.number, x, y)


    @create_users_and_game(['pl1', 'pl2'], [
        '1.....',
        '..##..',
        '....0.'
    ], GAME_SETTINGS, dict(player_base_speed=10000))
    def test_move_to_well(self):
        pl = self.get_player_by_position(0, 0)
        self.debug_game_action(pl, 'down')
        self.set_tick(pl, 1)
        self.assertPosition(pl.number, 0, 10000)
        self.debug_game_action(pl, 'right')
        self.set_tick(pl, 10)
        self.assertPosition(pl.number, 0, 10000)


    @create_users_and_game(['pl1', 'pl2'], [
        '0.##..',
        '......',
        '....1.'
    ], GAME_SETTINGS, dict(player_base_speed=100, max_slide_length=9900))
    def test_simple_slide(self):
        pl = self.get_player_by_position(0, 0)
        t = 1
        y = 100
        self.debug_game_action(pl, 'down')
        self.set_tick(pl, t)
        self.debug_game_action(pl, 'right')
        for i in xrange(9900 / 100):
            y += 100
            t += 1
            self.set_tick(pl, t)
            self.assertPosition(pl.number, 0, y)

        t += 1
        self.set_tick(pl, t)
        self.assertPosition(pl.number, 100, 10000)


    @create_users_and_game(['pl1', 'pl2'], [
        '0.##..',
        '..##..',
        '....1.'
    ], GAME_SETTINGS, dict(player_base_speed=100, max_slide_length=9900))
    def test_not_slide_two_well(self):
        pl = self.get_player_by_position(0, 0)
        t = 1
        y = 100
        self.debug_game_action(pl, 'down')
        self.set_tick(pl, t)

        self.debug_game_action(pl, 'right')
        t += 9900 / 100
        self.set_tick(pl, t)
        t += 1
        self.set_tick(pl, t)
        self.assertPosition(pl.number, 0, 100)


