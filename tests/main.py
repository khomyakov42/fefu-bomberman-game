import unittest
from authorize import *
from create_game import *
from engine import *
import utils
import sys



if __name__ == "__main__":
    URL = 'http://127.0.0.1:8080/'
    for arg in sys.argv[1:]:
        if arg[:4] == 'url=':
            URL = str(arg.split('=')[1])
            sys.argv.remove(arg)
        else:
            print 'url=\turl server, default 127.0.0.1:8080'

    utils.TEST_URL = URL

    unittest.main()