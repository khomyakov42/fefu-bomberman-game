from games.map import *

map = Map(3, 5)
map.set_cell(0, 0, StartPoint(0, Empty()))
map.set_cell(1, 0, Bomb(Empty()))
map.set_cell(2, 0, BreakableWall(Empty()))
map.set_cell(1, 1, BreakableWall(Empty()))
map.set_cell(0, 2, UnbreakableWall())
map.set_cell(1, 2, BreakableWall(Empty()))
map.set_cell(2, 2, UnbreakableWall())
map.set_cell(1, 3, BreakableWall(Empty()))
map.set_cell(0, 4, BreakableWall(Empty()))
map.set_cell(1, 4, BreakableWall(Bomb(Empty())))
map.set_cell(2, 4, StartPoint(1, Empty()))

'''
0. b. %.
.. %. ..
## %. ##
.. %. ..
%. B. 1.
'''


def test(map):
    s = map.representation()
    d = Map.parse(map.representation()).representation()

    if len(s) != len(d):
        return False

    for i, l in enumerate(s):
        if len(l) != len(d[i]):
            return False
        for j, ch in enumerate(l):
            if ch != d[i][j]:
                return False

    return True


print test(map)

map.write()
map.step()
map.write()
map.step()
map.write()





