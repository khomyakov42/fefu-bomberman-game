from main.decorators import *
from auth.models import User
from auth.decorators import check_auth
from lobby.models import Room, Map
from events_and_status import *
from debug.models import DebugMode
from games.manager import GameManager
from connection import WsConnection


@http_event
def reset(conn, db, data):
    GameManager().reset()
    WsConnection.reset()
    db.query(Room).delete()
    db.query(User).delete()
    db.query(Map).delete()
    db.query(DebugMode).delete()
    db.commit()
    conn.send(STATUS_OK)


@http_event
@check_auth
def game_set_debug_mode(conn, db, data):
    if conn.user.room is None:
        return conn.send(STATUS_NOT_IN_GAME)

    if conn.user.room.game_started:
        return conn.send(STATUS_GAME_STARTED)

    try:
        db.add(DebugMode(seed=int(data.get('random_seed', '')), room_id=conn.user.room_id))
        db.commit()
    except ValueError:
        return conn.send(STATUS_MALFORMED)

    conn.send(STATUS_OK)


@http_event
@check_auth
def game_set_tick(conn, db, data):
    try:
        tick = int(data.get('tick', ''))
    except ValueError:
        return conn.send(STATUS_MALFORMED)

    if not conn.user.room_id:
        return conn.send(STATUS_NOT_IN_GAME)

    GameManager().get_games()[0].set_step(tick)

    state = GameManager().get_games()[0].get_debug_state()
    conn.send(STATUS_OK, state)