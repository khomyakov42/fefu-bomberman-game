from settings import BASE_MODEL
from sqlalchemy.orm import *
from sqlalchemy import *

class DebugMode(BASE_MODEL):
    __tablename__ = 'debug'

    id = Column(Integer(), primary_key=True)
    seed = Column(Integer())
    room_id = Column(Integer, ForeignKey('room.id'))


    def __init__(self, seed, room_id):
        self.seed = seed
        self.room_id = room_id