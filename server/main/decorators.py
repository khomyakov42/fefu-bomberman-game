from connection import HttpConnection, WsConnection
from functools import wraps
from events_and_status import STATUS_MALFORMED



def socket_event(func):
    WsConnection.register_global_event(func.__name__, func)
    return func

def http_event(func):
    HttpConnection.register_event(func.__name__, func)
    return func

def check_request(**types):
    def decor(func):
        @wraps(func)
        def wrap(conn, db, data, *args, **kwargs):
            eq_types = {
                str: [unicode],
                unicode: [str]
            }

            for field, type in types.items():
                if sum([
                    data.has_key(field) and isinstance(data[field], t)
                    for t in eq_types.get(type, []) + [type]
                ]) == 0:
                    return conn.send(STATUS_MALFORMED)

            return func(conn, db, data, *args, **kwargs)
        return wrap
    return decor

