from main.decorators import *
from settings import PROJECT_HOST, WEBSOCKET_HOST
from events_and_status import *
from games.default_settings import settings
import socket


@http_event
def extensions(conn, db, data):
    if not WEBSOCKET_HOST:
        try:
            url = socket.gethostbyname_ex(socket.gethostname())[2][0]
        except:
            raise Exception('Unable to get server ip address. Change "WEBSOCKET_HOST" variable in server settings manually.')
    else:
        url = WEBSOCKET_HOST

    url += ':' + PROJECT_HOST.split(':')[1]
    if conn.request.remote_ip == '127.0.0.1':
        url = '127.0.0.1:8080'

    conn.send(STATUS_OK, {
        'extensions': {
            'core.v0': {
                'websocket_url': 'ws://%s/ws' % url,
                'cell_size': 10,
                'min_map_size': 3,
                'max_map_size': 5000,
                'default_game_settings': settings
            },
            'debug.v0': {}
        },
    })



