
from sqlalchemy.ext.declarative import declarative_base
import os

ROOT_DIR = os.path.dirname( __file__ )
BASE_MODEL = declarative_base()


DEBUG = False
TESTING = False


PROJECT_HOST = '0.0.0.0:8080'
WEBSOCKET_HOST = None

DB = 'sqlite:///db_.db'

MODULES = (
    'debug',
    'auth',
    'main',
    'lobby',
    'games'
)