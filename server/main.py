#!/usr/bin/env python
from tornado import web
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
import settings
from utils import import_module
from tornado import ioloop
from connection import HttpConnection, WsConnection
import os
import sys
import traceback
import logging
import threading
import time

os.sys.path.insert(0, os.path.dirname(os.path.abspath(__file__)))


def startTornado():
    for module_name in settings.MODULES:
        for file in ['listeners', 'models']:
            try:
                module = import_module(module_name + '.' + file)
            except ImportError, e:
                ex_type, ex, tb = sys.exc_info()
                if settings.DEBUG:
                    print e, ''.join(traceback.format_tb(tb))



    engine = create_engine(settings.DB, echo=False)
    settings.BASE_MODEL.metadata.create_all(engine)
    SessionDB = sessionmaker(engine)

    app = web.Application([
        (r'/ws', WsConnection),
        (r'/*', HttpConnection)
    ])

    app.SessionDB = SessionDB

    if settings.DEBUG:
        for logger_name in ['game', 'game_manager', 'conn']:
            logger = logging.getLogger(logger_name)
            logger.setLevel(logging.DEBUG)

    address, port = settings.PROJECT_HOST.split(':')
    app.listen(int(port), address)
    ioloop.IOLoop.instance().start()

def stopTornado():
    ioloop.IOLoop.instance().stop()


if __name__ == '__main__':
    for arg in sys.argv[1:]:
        if arg in ('-d', '--debug'):
            settings.DEBUG = True
        elif arg in ("-t", "--test"):
            settings.TESTING = True
        else:
            print '\
                -d --debug\tdisplays detailed information about the work\n\
                -t --test\tset the test mode\n\
            '
            sys.exit()

    try:
        threading.Thread(target=startTornado).start()
        print "Server is running. Server host %s, websocket host %s" % (settings.PROJECT_HOST, settings.WEBSOCKET_HOST)
        while True:
            time.sleep(1)
    except (KeyboardInterrupt, SystemExit):
        stopTornado()
        print "Server stop"
        sys.exit()