#coding=utf-8
import logging
from tornado.escape import json_encode, json_decode
from tornado.websocket import WebSocketHandler
from tornado.web import RequestHandler
from auth.models import User
import simplejson
from events_and_status import STATUS_MALFORMED, STATUS_BAD_ACTION, EVENT_ANOTHER_CONNECTION_OPEN

logger = logging.getLogger('conn')

class Connection(object):
    def __init__(self):
        self.user = None


class WsConnection(WebSocketHandler, Connection):
    clients = {}
    auth_clients = {}
    events = {}
    count_created = 0


    def __init__(self, app, *args, **kwargs):
        WebSocketHandler.__init__(self, app, *args, **kwargs)
        Connection.__init__(self)
        self.app = app
        self.db = app.SessionDB()
        WsConnection.count_created += 1
        self.id = 'client_%s' % self.count_created
        self.events = {}
        self.user_id = None
        self._cache = dict(original=None, json=None)
        WsConnection.clients[self.id] = self


    def open(self):
        self._process_callbacks('connection', {})
        logger.info('socket connected %s' % self.id)


    def on_message(self, message):
        self._update_user()
        try:
            data = json_decode(message)
        except Exception:
            data = {}

        event_name = data.get('event', None)
        logger.info('ws message %s' % data)
        self._process_callbacks(event_name, data)


    def on_close(self):
        logger.info('disconnection %s user_id %s' % (self.id, self.user_id))

        self._process_callbacks('disconnection', {})

        conn = WsConnection.auth_clients.get(self.user_id, None)
        if self.user and conn and conn.id == self.id:
            WsConnection.auth_clients.pop(self.user_id)

        WsConnection.clients.pop(self.id, None)



    def terminate(self):
        self.client_terminated = True
        self.stream.close()


    def set_user(self, user):
        self.user = user
        self.user_id = user.id
        if WsConnection.auth_clients.has_key(self.user_id) and WsConnection.auth_clients[self.user_id].id != self.id:
            conn = WsConnection.auth_clients[self.user_id]
            logger.info('delete auth connection from user %s' % self.user_id)
            if conn.ws_connection:
                conn.send(EVENT_ANOTHER_CONNECTION_OPEN)
                conn.user_id = None
                conn.close()

        WsConnection.auth_clients[self.user_id] = self
        self.db.commit()


    def _update_user(self):
        if self.is_auth():
            user = self.db.query(User).filter_by(id=self.user_id).first()
            self.db.commit()
            self.set_user(user)

            self.db.commit()


    def _process_callbacks(self, name, data):
        if WsConnection.events.has_key(name):
            for cb in WsConnection.events[name]:
                try:
                    cb(self, self.db, data)
                except:
                    pass

        if self.events.has_key(name):
            for cb in self.events[name]:
                cb(self, self.db, data)


    def is_auth(self):
        return getattr(self, 'user', None)


    def send(self, name, data=None):
        data = data or {}
        data['event'] = name
        try:
            self.write_message(data)
        except:
            pass


    def write_message(self, message, binary=False):
        logger.info("ws send %s to %s" % (message, self.id))

        if isinstance(message, dict):
            if self._cache['original'] != message:
                self._cache['original'] = message
                message = json_encode(message)
                self._cache['json'] = message
            else:
                message = self._cache['json']

        self.ws_connection.write_message(message, binary=binary)


    def send_all(self, name, data=None):
        data = data or {}
        WsConnection.send_all(name, data)


    @staticmethod
    def get_users_connection(user_ids):
        result = []
        for k, v in  WsConnection.auth_clients.items():
            if k in user_ids:
                result.append(v)

        return result


    @staticmethod
    def send_broadcast(name, data=None, user_ids=None):
        data = data or {}
        user_ids = user_ids or WsConnection.auth_clients.keys()
        for id in user_ids:
            if WsConnection.auth_clients.has_key(id):
                WsConnection.auth_clients[id].send(name, data)


    def register_event(self, name, func):
        if not self.events.has_key(name):
            self.events[name] = []
        self.events[name].append(func)



    def unregister_event(self, name, func):
        if self.events.has_key(name):
            for i in xrange(self.events[name]):
                if self.events[name][i] == func:
                    self.events[name].pop(i)
                    break

    @staticmethod
    def register_global_event(name, func):
        if not WsConnection.events.has_key(name):
            WsConnection.events[name] = []
        WsConnection.events[name].append(func)


    @classmethod
    def reset(cls):
        for cl in cls.clients:
            try:
                cl.terminate()
            except:
                pass

        cls.auth_clients = {}
        cls.clients = {}


    def __repr__(self):
        return self.id



class HttpConnection(RequestHandler, Connection):
    events = {}


    def __init__(self, app, *args, **kwargs):
        self.db = app.SessionDB()
        self.ws = WsConnection
        RequestHandler.__init__(self, app, *args, **kwargs)


    def get(self, *args, **kwargs):
        self.jsonp_callback = self.get_argument('callback', '', strip=False)


        try:
            data = simplejson.loads(self.get_argument('q', None, strip=False))
            if not isinstance(data, dict):
                raise Exception()
        except Exception:
            return self.send(STATUS_MALFORMED)

        logger.info("http request %s" % data)
            
        if data.get('sid', False):
            users = self.db.query(User).filter_by(session_id=data.get('sid', None))
            if users.count():
                self.user = users.first()

        if data.has_key('action') and isinstance(data['action'], unicode):
            if HttpConnection.events.has_key(data['action']):
                HttpConnection.events[data['action']](self, self.db, data)
            else:
                return self.send(STATUS_BAD_ACTION)
        else:
            return self.send(STATUS_MALFORMED)


    def send(self, status, data=None):
        data = data or {}
        self.set_header('Content-Type', 'application/javascript')
        data['status'] = status
        logger.info('http send %s' % data)
        self.finish(self.jsonp_callback + '(' + json_encode(data) + ')')


    @staticmethod
    def register_event(name, func):
        HttpConnection.events[name] = func