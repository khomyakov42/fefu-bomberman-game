from sqlalchemy import *
from sqlalchemy.orm import *
from settings import BASE_MODEL
from auth.models import User
import simplejson

SETTINGS_GAME_LENGTH = 4096

class Map(BASE_MODEL):
    __tablename__ = 'map'

    id = Column(Integer, primary_key=True)
    name = Column(String(32))
    settings = Column(String(SETTINGS_GAME_LENGTH))
    fields = Column(String(100 * 100 * 2 * 2))
    max_player_count = Column(Integer(), default=1)


    def __init__(self, name, settings, fields, max_player_count):
        self.name = name
        self.settings = simplejson.dumps(settings)
        self.fields = simplejson.dumps(fields)
        self.max_player_count = max_player_count


class Room(BASE_MODEL):
    __tablename__ = 'room'

    id = Column(Integer, primary_key=True)
    name = Column(String(32))
    password = Column(String(256))
    game_started = Column(Boolean(), default=False)
    users = relationship(User, backref="room", lazy="dynamic")
    map_id = Column(Integer, ForeignKey('map.id'))
    map = relation("Map", primaryjoin="Room.map_id==Map.id")
    settings = Column(String(SETTINGS_GAME_LENGTH))
    max_player_count = Column(Integer)

    def __init__(self, name, password, map_id, settings, max_player_count):
        self.name = name
        self.password = password
        self.map_id = map_id
        self.settings = simplejson.dumps(settings)
        self.max_player_count = max_player_count


    def free_slots(self):
        return self.max_users_count - self.users.count()




