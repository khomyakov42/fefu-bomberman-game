from main.decorators import http_event, socket_event, check_request
from auth.decorators import check_auth
from models import Room, Map
from hashlib import sha1
from datetime import datetime
from auth.models import User
from events_and_status import *
import time
import re
from games.game_settings import Settings
from games.field.map import Map as GameMap
import simplejson
from debug.models import DebugMode
from sqlalchemy.sql.expression import asc
from settings import DEBUG, TESTING
from games.manager import GameManager
import time


@http_event
@check_auth
def list_games(conn, db, data):
    games = [dict(
        id=x.id,
        name=x.name,
        total_slots=x.max_player_count,
        free_slots=x.max_player_count - x.users.count(),
        has_password=x.password != sha1('').hexdigest(),
        map_id=x.map.id
    ) for x in db.query(Room).all() if x.map.max_player_count != x.users.count() and not x.game_started]

    conn.send(STATUS_OK, {'games': games})


@http_event
@check_auth
@check_request(
    name=str,
    password=str,
    map_id=int,
    settings=dict,
    max_player_count=int)
def create_game(conn, db, data):
    name = data['name']
    password = data['password']
    map_id = data['map_id']
    settings = data['settings']
    max_player_count = data['max_player_count']

    if not re.match('^.{3,32}$', name, flags=re.DOTALL) or not re.match('^.{0,256}', password):
        return conn.send(STATUS_MALFORMED)

    try:
        Settings.parse(settings)
    except ValueError, e:
        return conn.send(STATUS_MALFORMED)

    if conn.user.room:
        return conn.send(STATUS_ALREADY_IN_GAME)

    map = db.query(Map).filter_by(id=map_id).first()

    if not map:
        return conn.send(STATUS_MAP_NOT_FOUND)

    if max_player_count > map.max_player_count or max_player_count < 2:
        return conn.send(STATUS_BAD_MAX_PLAYER)

    room = Room(name=name, password=sha1(password).hexdigest(), map_id=map_id,
        settings=settings, max_player_count=max_player_count)
    conn.user.last_join = datetime.now()
    db.add(room)
    db.commit()
    room.users.append(conn.user)
    db.commit()
    conn.ws.send_broadcast(EVENT_PLAYER_JOINED, {'username': conn.user.login}, [conn.user.id])
    conn.send(STATUS_OK, dict(game_id=room.id))
    db.commit()



@http_event
@check_auth
@check_request(game_id=int, password=str)
def join_game(conn, db, data):
    room = db.query(Room).filter_by(id=data['game_id']).first()
    if not room:
        return conn.send(STATUS_GAME_NOT_FOUND)

    if room.password != sha1(data['password']).hexdigest():
        return conn.send(STATUS_BAD_PASSWORD_GAME)

    if conn.user.room:
        return conn.send(STATUS_ALREADY_IN_GAME)

    if room.game_started and room.users.count() == 0:
        db.query(Room).filter_by(id=room.id).update(dict(game_started=False))
        db.commit()

    if room.game_started:
        return conn.send(STATUS_GAME_STARTED)

    if room.max_player_count == room.users.count():
        return conn.send(STATUS_GAME_FULL)


    for pl in room.users.order_by(asc('last_join')).all():
        conn.ws.send_broadcast(EVENT_PLAYER_JOINED, {'username': pl.login}, [conn.user.id])
        if pl.ready:
            conn.ws.send_broadcast(EVENT_VOTE, {'username': pl.login}, [conn.user.id])

    conn.user.last_join = datetime.now()
    room.users.append(conn.user)
    db.commit()
    users = db.query(Room).filter_by(id=room.id)[0].users.all()
    conn.ws.send_broadcast(EVENT_PLAYER_JOINED, {'username': conn.user.login}, map(lambda u: u.id, users))
    conn.send(STATUS_OK, dict(map_id=room.map.id, settings=simplejson.loads(room.settings)))
    db.commit()



def run_game(ws, db, users, room):
    if all([u.ready for u in users]) and (len(users) > 1 or (DEBUG and not TESTING)):
        db.query(Room).filter_by(id=room.id).update(dict(game_started=True))
        debug = db.query(DebugMode).filter_by(room_id=room.id).first()

        seed=int(time.clock()*1000)
        if debug:
            seed = debug.seed
            db.delete(debug)
            db.commit()

        connections = {}
        for user in users:
            conns = ws.get_users_connection([user.id])
            if len(conns):
                connections[user.id] = conns[0]

        m = room.map
        field = simplejson.loads(m.fields)
        settings = simplejson.loads(room.settings)
        db.commit()
        GameManager().create_game(users, connections, field, settings, bool(debug), seed)
        db.commit()


@http_event
@check_auth
def leave_game(conn, db, data):
    if conn.user.room is None:
        return conn.send(STATUS_NOT_IN_GAME)

    
    room = conn.user.room

    if conn.user.room.users.count() == 1:
        db.delete(conn.user.room)
    else:
        conn.user.room.users.remove(conn.user)

    db.query(User).filter_by(id=conn.user.id).update(dict(ready=False))
    db.commit()

    GameManager().process_event(conn, EVENT_PLAYER_LEFT)
    users = room.users.all()
    conn.ws.send_broadcast(EVENT_PLAYER_LEFT, {'username': conn.user.login}, [u.id for u in users])
    run_game(conn.ws, db, users, room)

    db.commit()

    return conn.send(STATUS_OK)


@http_event
@check_auth
def start_game(conn, db, data):
    if not conn.user.room:
        return conn.send(STATUS_NOT_IN_GAME)

    if conn.user.room.game_started:
        return conn.send(STATUS_GAME_STARTED)

    db.query(User).filter_by(id=conn.user.id).update(dict(ready=True))
    conn.send(STATUS_OK)
    conn.ws.send_broadcast(EVENT_VOTE, {'username': conn.user.login}, [u.id for u in conn.user.room.users.all()])
    db.commit()

    users = conn.user.room.users.all()
    run_game(conn.ws, db, users, conn.user.room)


@http_event
@check_auth
@check_request(message=str)
def send_message(conn, db, data):
    text = data['message']
    if not re.match('^.{1,256}$', text):
        return conn.send(STATUS_MALFORMED, {'fields': ['message']})

    if not conn.user.room:
        return conn.send(STATUS_NOT_IN_GAME)

    data = dict(
        text=text,
        username=conn.user.login,
        timestamp=time.mktime(datetime.now().timetuple())
    )

    conn.ws.send_broadcast(EVENT_NEW_MESSAGE, data, [u.id for u in conn.user.room.users.all()])
    db.commit()
    conn.send(STATUS_OK)


@socket_event
@check_auth
def disconnection(conn, db, data):
    if conn.user.room and not TESTING:
        conn.send_broadcast(EVENT_PLAYER_LEFT, {'username': conn.user.login})
        conn.user.room.users.remove(conn.user)
        if conn.user.room.users.count() == 0:
            db.delete(conn.user.room)

        db.commit()
        GameManager().process_event(conn, EVENT_DISCONNECTION)
        db.commit()



@http_event
@check_auth
@check_request(name=str, settings=dict, field=list)
def upload_map(conn, db, data):
    if not re.match(r'^.{1,32}$', data['name'], flags=re.DOTALL):
        return conn.send(STATUS_MALFORMED)
    try:
        s = Settings.parse(data['settings'])
    except ValueError, e:
        return conn.send(STATUS_MALFORMED)

    try:
        max_player_count = len(GameMap.parse(data['field'], s).start_position)
    except ValueError, e:
        return conn.send(STATUS_MALFORMED)

    m = Map(data['name'], data['settings'], data['field'], max_player_count)
    db.add(m)
    db.commit()
    conn.send(STATUS_OK, dict(map_id=m.id))


@http_event
@check_auth
@check_request(map_id=int)
def get_map(conn, db, data):
    try:
        m = db.query(Map).filter_by(id=data['map_id']).first()
    except ValueError:
        return conn.send(STATUS_MALFORMED)

    if not m:
        return conn.send(STATUS_MAP_NOT_FOUND)

    conn.send(STATUS_OK, dict(
        id=m.id,
        name=m.name,
        settings=simplejson.loads(m.settings),
        field=simplejson.loads(m.fields),
        max_player_count=m.max_player_count
    ))


@http_event
@check_auth
def list_maps(conn, db, data):
    maps = db.query(Map).all()
    conn.send(STATUS_OK, dict(
        maps=map(lambda m: dict(map_id=m.id, name=m.name), maps)
    ))


@http_event
@check_auth
def player_state(conn, db, data):
    res = dict(state='in_game')

    if not conn.user.room:
        res['state'] = 'not_in_game'
    else:
        if conn.user.room.game_started:
            res['state'] = 'in_started_game'

        res['game_id'] = conn.user.room.id
        res['game_name'] = conn.user.room.name

    conn.send(STATUS_OK, res)




