import sys

def _resolve_name(name, package, level):
    if not hasattr(package, 'rindex'):
        raise ValueError("'package' not set to a string")
    dot = len(package)
    for x in xrange(level, 1, -1):
        try:
            dot = package.rindex('.', 0, dot)
        except ValueError:
            raise ValueError("attempted relative import beyond top-level "
                             "package")
    return "%s.%s" % (package[:dot], name)


def import_module(name, package=None):
    if name.startswith('.'):
        if not package:
            raise TypeError("relative imports require the 'package' argument")
        level = 0
        for character in name:
            if character != '.':
                break
            level += 1
        name = _resolve_name(name[level:], package, level)
    __import__(name)
    return sys.modules[name]


class LazyDB(object):
    def __init__(self, cl, cl_m, *args, **kwargs):
        self.args = args
        self.kwargs = kwargs
        self.cl = cl
        self.cl_m = cl_m
        self.obj = None

    def __getattr__(self, item):
        if item in ('obj', 'cl', 'args', 'kwargs', 'cl_m'):
            return object.__getattribute__(self, item)

        if not self.obj:
            self.obj = self.cl(*self.args, **self.kwargs)


        if item == 'query':
            return self.obj.query(self.cl_m)

        return getattr(self.obj, item)
