from models import User

def check_auth(func):
    def wrap(conn, db, data, *args, **kwargs):
        if db.query(User).filter_by(sid=data.get('sid', '')).count():
            return func(conn, db, data, *args, **kwargs)
        return None

    return wrap