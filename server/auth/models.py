from sqlalchemy import *
from settings import BASE_MODEL

class User(BASE_MODEL):
    __tablename__ = 'user'

    id = Column(Integer, primary_key=True)
    login = Column(String)
    session_id = Column(String(32))
    password = Column(String(32))
    room_id = Column(Integer, ForeignKey('room.id'))
    ready = Column(Boolean(), default=False)
    last_join = Column(DateTime())

    def __init__(self, login, password):
        self.login = login
        self.password = password

