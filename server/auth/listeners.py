from main.decorators import http_event, socket_event, check_request
from .decorators import check_auth
from events_and_status import *
from models import User
from hashlib import sha1
import re
import datetime
import random
import string
from games.manager import GameManager

@http_event
@check_request(username=str, password=str)
def login(conn, db, data):
    login = data['username']
    password = data['password']

    if not re.match('^[A-Za-z0-9_]{3,15}$', login):
        return conn.send(STATUS_MALFORMED)

    if not re.match('^.{1,256}$', password, flags=re.S):
        return conn.send(STATUS_MALFORMED)

    user = db.query(User).filter_by(login=login, password=sha1(password).hexdigest()).first()
    if not user:
        return conn.send(STATUS_BAD_LOGIN_OR_PASSWORD)

    letters = string.ascii_letters + string.digits
    sid = ''.join([random.choice(letters) for _ in xrange(32)])
    db.query(User).filter_by(id=user.id).update(dict(session_id=sid, ready=False))
    if user.room:
        user.room.users.remove(user)
    db.commit()
    conn.send(STATUS_OK, {'sid': sid})


@socket_event
@check_request(sid=str)
def auth(conn, db, data):
    try:
        sid = data['sid']
        if not re.match('^[a-zA-Z0-9]{32}$', sid):
            raise ValueError()
    except (KeyError, ValueError):
        conn.send(EVENT_PROTOCOL_VIOLATION)
        return conn.terminate()

    user = db.query(User).filter_by(session_id=sid).first()


    if not user:
        conn.send(EVENT_AUTH_ERROR)
        return conn.close()

    conn.set_user(user)
    conn.send(EVENT_AUTH_SUCCESS)
    db.commit()


@http_event
@check_auth
def logout(conn, db, data):
    db.query(User).filter_by(id=conn.user.id).update(dict(session_id=None))
    if conn.user.room:
        conn.user.room.users.remove(conn.user)
        if conn.user.room.users.count() == 0:
            db.delete(conn.user.room)

    db.commit()
    conn.send(STATUS_OK)


@http_event
@check_request(username=str, password=str)
def register(conn, db, data):
    login = data['username']
    password = data['password']

    if not re.match('^[A-Za-z0-9_]{3,15}$', login):
        return conn.send(STATUS_MALFORMED)

    if not re.match('^.{1,256}$', password, flags=re.S):
        return conn.send(STATUS_MALFORMED)

    if db.query(User).filter_by(login=login).count():
        return conn.send(STATUS_USER_EXIST)

    db.add(User(login=login, password=sha1(password).hexdigest()))
    db.commit()

    conn.send(STATUS_OK)
