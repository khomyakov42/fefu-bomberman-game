from models import User
from connection import HttpConnection, WsConnection
from events_and_status import *
from main.decorators import check_request
import re


def check_auth(func):
    def wrap(conn, db, data, *args, **kwargs):
        if isinstance(conn, HttpConnection):
            try:
                sid = data['sid']
                if not isinstance(sid, unicode) or not re.match('^[a-zA-Z0-9]{32}$', sid):
                    raise ValueError()
            except (ValueError, KeyError):
                return conn.send(STATUS_MALFORMED)

            user = db.query(User).filter_by(session_id=sid).first()

            if not user:
                return conn.send(STATUS_BAD_SID)
            else:
                setattr(conn, 'user_id', user.id)
                setattr(conn, 'user', user)

        if isinstance(conn, WsConnection) and not conn.is_auth():
            return

        return func(conn, db, data, *args, **kwargs)

    wrap.__name__ = func.__name__
    return wrap