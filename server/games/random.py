from itertools import cycle
def mt19937(seed):
    mt = [seed]
    for i in range(1, 624):
        mt.append((0x6c078965 * (mt[i-1] ^ (mt[i-1] >> 30)) + i) & 0xffffffff)

    for i in cycle(range(624)):
        if i == 0:
            for j in range(624):
                y = (mt[j] & 0x80000000) + (mt[(j+1) % 624] & 0x7fffffff)
                mt[j] = mt[(j + 397) % 624] ^ (y >> 1)
                if y % 2 != 0:
                    mt[j] ^= 0x9908b0df
        y = mt[i]
        y ^= y >> 11
        y ^= (y << 7) & 0x9d2c5680
        y ^= (y << 15) & 0xefc60000
        y ^= y >> 18
        yield y
