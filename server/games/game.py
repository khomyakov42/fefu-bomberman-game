from multiprocessing import Process
from threading import Thread
from games.field import Bomb, Explosion, Movable, Player, Map
from games.game_settings import Settings
from games.event_manager import *
from events_and_status import *
from settings import DEBUG, TESTING
import logging
import time

logger = logging.getLogger('game')


class Game(Thread if DEBUG or TESTING else Process):
    TIME = 33

    def __init__(self, connection, players, map, settings, debug_mode=False, seed=0):
        super(Game, self).__init__()
        self.conn = connection
        self.stop = False
        self.debug_mode = debug_mode
        self.seed = seed

        self.players = players
        self.event_manager = None
        self.map = map
        self.settings = settings
        self.init = False


    def __rinit__(self):
        self.event_manager = EventManager()
        self.map = Map.parse(self.map, Settings.parse(self.settings), self.event_manager, self.seed)

        self.last_change_direction_player = {}
        for index, pl in enumerate(self.players):
            self.map.add_player(index)

        logger.info("game initialized")
        self.init = True



    def _process_events(self):
        directions = {
            'left': EVENT_LEFT,
            'up': EVENT_UP,
            'right': EVENT_RIGHT,
            'down': EVENT_DOWN,
            'none': EVENT_NONE
        }

        events = []
        while self.conn.poll(0):
            events.append(self.conn.recv())

        for i, t in enumerate(events):
            player, event, data = t
            if event == EVENT_GAME_ACTION:
                if data.has_key('direction') and directions.has_key(data['direction']) and data.has_key('action'):
                    self.event_manager.process_event(player, directions[data['direction']], {})
                    self.last_change_direction_player[player] = self.map.step_number
                    if data['action'] == 'place_bomb':
                        self.event_manager.process_event(player, EVENT_START_BOMBING, {})
                    else:
                        self.event_manager.process_event(player, EVENT_STOP_BOMBING, {})

            elif event in (EVENT_PLAYER_LEFT, EVENT_DISCONNECTION, EVENT_AUTH_SUCCESS, EVENT_AUTH_ERROR):
                for pl in self.map.get_layers_by_class(Player):
                    if pl.number == player:
                        pl.dead = True
                        break


    def _get_game_state(self):
        bombs = map(lambda b: dict(
            x=b.get_absolute_position()[0],
            y=b.get_absolute_position()[1],
            created_at=b.created_at_step_number + 1,
            destroy_at=b.destroy_at_step_number + 1,
            flame_length=b.radius
        ), self.map.get_layers_by_class(Bomb))

        explosions = map(lambda e: dict(
            x=e.get_absolute_position()[0],
            y=e.get_absolute_position()[1],
            created_at=e.created_at_step_number + 1,
            destroy_at=e.destroy_at_step_number + 1,
            up_length=e.shock_wave['up'],
            down_length=e.shock_wave['down'],
            left_length=e.shock_wave['left'],
            right_length=e.shock_wave['right']
        ), self.map.get_layers_by_class(Explosion))

        str_dir = {
            Movable.DIRECTION_LEFT: 'left',
            Movable.DIRECTION_RIGHT: 'right',
            Movable.DIRECTION_UP: 'up',
            Movable.DIRECTION_DOWN: 'down',
            Movable.DIRECTION_NONE: 'none'
        }

        players = []
        count_dead = 0
        for pl in sorted(self.map.get_layers_by_class(Player), cmp=lambda x, y: cmp(x.number, y.number)):
            if pl.dead:
                count_dead += 1

            players.append(dict(
                x=pl.get_absolute_position()[0],
                y=pl.get_absolute_position()[1],
                look_direction=str_dir[pl.look_direction],
                movement_direction=str_dir[pl.direction * pl.direction_multiplier],
                direction_changed_at=self.last_change_direction_player.get(pl.number, 0),
                speed=pl.speed,
                dead=pl.dead,
                remaining_bombs=pl.bomb_count,
                flame_length=pl.flame
            ))

        winner = False
        if count_dead >= len(players) - 1 and not DEBUG or count_dead == len(players):
            winner = ''
            winner_number = map(lambda x: x[0], filter(lambda pl: not pl[1]['dead'], enumerate(players)))
            if len(winner_number):
                winner = self.players[winner_number[0]]['username']

        return winner != False, winner, dict(
            field=self.map.representation(),
            players=players,
            bombs=bombs,
            explosions=explosions,
            tick=self.map.step_number,
        )



    def set_step(self, step):
        if not self.init:
            self.__rinit__()

        if step < self.map.step_number:
            return

        logger.info('set tick %s, current tick %s' % (step, self.map.step_number))
        self._process_events()
        for _ in xrange(step - self.map.step_number):
            self.map.step()
            e, w, r = self._get_game_state()
            logger.info(
                '\n-------------PLAYERS-------------\n' +\
                'number:\t' + '\t'.join([str(i) for i in xrange(len(r['players']))]) + '\n' +\
                'pos:\t' + '\t'.join(['%s|%s' % (p['x'], p['y']) for p in r['players']]) + '\n' +\
                'direct:\t' + '\t'.join([str(p['movement_direction']) for p in r['players']]) + '\n'
            )


    def get_debug_state(self):
        is_end, winner, resp = self._get_game_state()
        return dict(
            tick=self.map.step_number,
            game_state=resp,
            game_ended=is_end,
            winner=winner or '',
        )


    def run(self):
        self.__rinit__()

        player_names = [pl['username'] for pl in self.players]
        for number, player in enumerate(self.players):
            self.conn.send((number, EVENT_GAME_STARTED, dict(
                players=player_names,
                current_player_index=number
            )))

        logger.info("game started %d players" % len(self.players))

        dt = Game.TIME / 1000.0
        last_time = time.time()
        start_time = time.time()
        times = 0

        while not self.debug_mode and not self.stop:
            game_end, winner_name, data = self._get_game_state()

            for number in xrange(len(self.players)):
                self.conn.send((number, EVENT_UPDATE_MAP, data))

            if game_end and len(data['explosions']) == 0 and len(data['bombs']) == 0:
                data = dict(winner_name=winner_name)
                for number in xrange(len(self.players)):
                    self.conn.send((number, EVENT_GAME_END, data))

                logger.info('game end, winner %s' % winner_name)
                break

            self._process_events()
            self.map.new_step()

            work_time = time.time() - last_time

            if time.time() - last_time < dt:
                time.sleep(dt - work_time)

            last_time = time.time()
            logger.info('work time %s' % work_time)
            times += 1
            if time.time() - start_time >= 5:
                times = 0
                start_time = time.time()




    def terminate(self):
        self.stop = True
        if hasattr(super(Game, self), 'terminate'):
            super(Game, self).terminate()

