from games.field.bonus import Bonus
from games.field.utils import parse_positive_integer

from copy import deepcopy

class Settings(object):

    def __init__(self):
        self.settings = {}
        self.total_weight = 0
        self.drop_probability = None


    def register_settings(self, class_name, settings):
        self.settings[class_name] = settings
        if issubclass(class_name, Bonus):
            self.total_weight += settings['weight']


    def compute_cumulative_weights(self):
        cl_bonuses = {cl.ITEM_INDEX: cl for cl in self.settings.keys() if issubclass(cl, Bonus)}

        cw = 0
        l = []
        for i in sorted(cl_bonuses.keys()):
            cw = self.settings[cl_bonuses[i]]['weight'] + cw
            self.settings[cl_bonuses[i]]['cumulative_weight'] = cw
            l.append(cl_bonuses[i])


    def get_settings(self, class_name):
        try:
            return self.settings[class_name]
        except KeyError:
            return {}


    @staticmethod
    def parse(data):
        from games.field.map import Map
        settings = Settings()
        for cl_layer in Map.get_registered_layers():
            s = cl_layer.parse_settings(data)
            settings.register_settings(cl_layer, s)

        s = {}
        parse_positive_integer(s, data, 'item_drop_probability')
        settings.drop_probability = s['item_drop_probability']

        settings.compute_cumulative_weights()
        return settings