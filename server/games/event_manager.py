EVENT_EXIT = 'exit'
EVENT_UP = 'up'
EVENT_DOWN = 'down'
EVENT_LEFT = 'left'
EVENT_RIGHT = 'right'
EVENT_NONE = 'none'
EVENT_START_BOMBING = 'bomb'
EVENT_STOP_BOMBING = 'unbomb'

class EventManager(object):
    def __init__(self):
        self.listeners = {}
        self.events = dict()


    def process_event(self, number_player, event, data=None):
        data = {} if data is None else data
        for handler, layer in self.listeners.get(event, {}).get(number_player, []):
            handler(data)


    def on(self, event, number_player, handler, layer):
        if not self.listeners.has_key(event):
            self.listeners[event] = {}

        if not self.listeners[event].has_key(number_player):
            self.listeners[event][number_player] = []

        self.listeners[event][number_player].append((handler, layer))


    def off(self, event, number_player, layer):
        if self.listeners.has_key(event) and self.listeners[event].has_key(number_player):
            self.listeners[event][number_player] = filter(lambda e: e[1] != layer, self.listeners[event][number_player])