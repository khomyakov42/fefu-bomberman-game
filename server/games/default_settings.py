settings = {
    'max_slide_length': 7000,
    'player_base_speed': 833,
    #'player_resurrect_time': 1000,
    'bomb_speed': 900,
    'bomb_delay': 80,
    'bomb_chain_reaction_delay': 4,
    'blast_wave_duration': 20,
    'cell_decay_duration': 10,
    'item_drop_probability': 45,
    'items': {
        'bomb': {
            'weight': 100,
            'start_count': 1,
            'max_count': 100,
            'bomb_bonus': 1
        },

        'flame': {
            'weight': 100,
            'start_count': 1,
            'max_count': 100,
            'flame_bonus': 1
        },

        'goldenflame': {
            'weight': 100,
            'start_count': 0,
            'max_count': 100,
            'flame_bonus': 100
        },

        'speed': {
            'weight': 100,
            'start_count': 0,
            'max_count': 28,
            'speed_bonus': 500
        },

        'kicker': {
            'weight': 0,
            'start_count': 0,
            'max_count': 100
        },

        'throw': {
            'weight': 0,
            'start_count': 0,
            'max_count': 100
        },

        'invert': {
            'weight': 100,
            'start_count': 0,
            'max_count': 100,
            'duration': 450
        },

        'lightspeed': {
            'weight': 50,
            'start_count': 0,
            'max_count': 450,
            'speed': 10000,
            'duration': 1000
        },

        'autobomb': {
            'weight': 50,
            'start_count': 0,
            'max_count': 100,
            'duration': 450
        },

        'nobomb': {
            'weight': 50,
            'start_count': 0,
            'max_count': 100,
            'duration': 450
        },

        'turtle': {
            'weight': 50,
            'start_count': 0,
            'max_count': 100,
            'speed': 100,
            'duration': 450
        }
    }
}

