from collections import Iterable


def parse_positive_integer(settings, data, item, to_item=None):
    try:
        d = data
        for i in item.split('.')[:-1]:
            d = d[i]
        item = item.split('.')[::-1][0]
        if d[item] < 0:
            raise ValueError()
        settings[to_item or item] = int(d[item])
    except (ValueError, KeyError):
        raise ValueError('can not parse "%s"' % item)


class Vector(tuple):
    '2D Vector with game-specific features'

    def __new__ (cls, *args):
        if len(args) == 1 and isinstance(args[0], Iterable):
            args = args[0]
        return super(Vector, cls).__new__(cls, args)

    def __add__(self, v):
        return Vector(i + j for i, j in zip(self, v))

    def __sub__(self, v):
        return Vector(i - j for i, j in zip(self, v))

    def sign(self):
        return Vector(x and x // abs(x) for x in self)

    @property
    def ort(self):
        return Vector(reversed(self))

    def to_border(self, dir):
        if dir[0] >= 0 and dir[1] >= 0:
            return self
        return self - dir

    def _mul(self, c):
        return Vector(x * c for x in self)

    def _dot(self, v):
        return sum((i * j) for i, j in zip(self, v))

    def __mul__(self, x):
        return self._dot(x) if isinstance(x, tuple) else self._mul(x)

    def __and__(self, mask):
        return tuple(j and i for i, j in zip(self, mask))

    def __bool__(self):
        from movable import Movable
        return self != Movable.DIRECTION_NONE

    @staticmethod
    def from_cell(cell):
        return Vector(cell.x, cell.y)