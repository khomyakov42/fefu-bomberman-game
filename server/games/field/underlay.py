from games.field.layer import Layer, DynamicLayer, StaticLayer
from games.field.bonus import Bonus
from games.field.utils import parse_positive_integer
import re


class Underlay(Layer): pass

class DynamicUnderlay(DynamicLayer): pass

class StaticUnderlay(StaticLayer): pass


class Empty(StaticUnderlay):
    code = '.'


class UnbreakableWall(StaticUnderlay):
    code = '#'


class ArrowUp(StaticUnderlay):
    code = '^'


class ArrowDown(StaticUnderlay):
    code = 'v'


class ArrowRight(StaticUnderlay):
    code = '>'


class ArrowLeft(StaticUnderlay):
    code = '<'

class BlackHole(DynamicUnderlay):
    code = '*'

class StartPoint(StaticUnderlay):
    def __init__(self, player_number, *args, **kwargs):
        super(StartPoint, self).__init__(*args, **kwargs)
        self.code = str(player_number)
        self.number = player_number


    def get_number(self):
        return self.number


    def representation(self):
        code = self.child.representation()
        return code


    @classmethod
    def parse(cls, code, map):
        try:
            return StartPoint(int(code[0]), map), code[1:]
        except ValueError:
            pass


class BreakableWall(DynamicUnderlay):
    code = '%'


    def __init__(self, *args, **kwargs):
        super(BreakableWall, self).__init__(*args, **kwargs)
        self.time_before_demolition = None


    def blow_up(self):
        if self.time_before_demolition is None:
            self.time_before_demolition = self.settings['time_before_demolition']


    def step(self):
        if self.time_before_demolition is None:
            return []

        self.time_before_demolition -= 1
        if self.time_before_demolition < 0:
            self.cell.pop_layer(self)
            return []
        return [self]


    def representation(self):
        if self.time_before_demolition is not None:
            self.code = '$'

        return super(BreakableWall, self).representation()


    @classmethod
    def parse(cls, code, map):
        if re.match(r'^[A-Z]', code[0]):
            return BreakableWall(map), code[0].lower() + code[1:]
        elif code[0] == BreakableWall.code:
            return BreakableWall(map), code[1:]


    @classmethod
    def parse_settings(cls, data):
        s = {}
        parse_positive_integer(s, data, 'cell_decay_duration', 'time_before_demolition')
        return s



class BreakableBonusWell(BreakableWall):
    def step(self):
        cell = self.cell
        child = self.child

        res = super(BreakableBonusWell, self).step()

        if self.time_before_demolition is not None and self.time_before_demolition < 0:
            if not isinstance(child, Bonus):
                b = cell.map.drop_bonus()
                if b is not None:
                    cell.set_top_layer(b, child)
                    return [b]
        return res


    @classmethod
    def parse(cls, code, map):
        if code == cls.code + cls.code:
            return BreakableBonusWell(map=map), Empty.code


class Explosion(DynamicUnderlay):
    def __init__(self, created_at_step_number, blast_radius=1, explosion_time=None, *args, **kwargs):
        super(Explosion, self).__init__(*args, **kwargs)
        self.explosion_time = self.settings['explosion_time'] if explosion_time is None else explosion_time
        self.created_at_step_number = created_at_step_number
        self.destroy_at_step_number = self.created_at_step_number + self.explosion_time
        self.blast_radius = blast_radius
        self.shock_wave = dict(left=blast_radius, right=blast_radius, up=blast_radius, down=blast_radius)


    def _blow_up_cell(self, cell):
        from games.field.movable import Player
        result = []
        layer = cell.top_layer

        while layer:
            layer.blow_up()
            found = False
            for c in (BreakableWall, UnbreakableWall, Bomb, Bonus, Player):
                if isinstance(layer, c):
                    result.append(layer)
                    found = True
                    break

            if not found:
                break

            layer = layer.child

        return result


    def step(self):
        self.explosion_time -= 1

        direct = {
            'left': (-1, 0),
            'right': (1, 0),
            'up': (0, -1),
            'down': (0, 1)
        }

        breakable_wall = []
        l = self._blow_up_cell(self.cell)
        if l:
            breakable_wall.append(l)

        for d, l in self.shock_wave.items():
            for i in xrange(1, l + 1):
                pos = self.cell.x + i * direct[d][0], self.cell.y + i * direct[d][1]
                cell = self.cell.map.get_cell(*pos)
                if not cell:
                    self.shock_wave[d] = i - 1
                    break

                l = self._blow_up_cell(cell)
                if len(l):
                    self.shock_wave[d] = i - 1
                    breakable_wall += l
                    break

        if self.explosion_time < 0:
            self.cell.pop_layer(self)
            return []

        return [self] + breakable_wall


    def representation(self):
        return self.child.representation()


    @classmethod
    def parse_settings(cls, data):
        s = {}
        parse_positive_integer(s, data, 'blast_wave_duration', 'explosion_time')
        return s



class Bomb(DynamicUnderlay):
    PRIORITY = 1

    def __init__(self, created_at_step_number, player, radius=1, left_before_explosion=None, *args, **kwargs):
        super(Bomb, self).__init__(*args, **kwargs)
        self.left_before_explosion = self.settings['left_before_explosion'] if left_before_explosion is None \
                                                                            else left_before_explosion
        self.radius = radius
        self.player = player
        self.created_at_step_number = created_at_step_number
        self.destroy_at_step_number = self.created_at_step_number + self.left_before_explosion


    def step(self):
        self.left_before_explosion -= 1

        if self.left_before_explosion != 0:
            return [self]

        self.player.bomb_count += 1
        explosion = Explosion(self.cell.map.step_number, self.radius, map=self.cell.map)
        self.cell.set_underlay(explosion, self)
        self.cell.pop_layer(self)
        return explosion.step()


    def blow_up(self):
        self.destroy_at_step_number = min(self.destroy_at_step_number, self.cell.map.step_number + self.settings['chain_reaction_delay'])
        self.left_before_explosion = min(self.left_before_explosion, self.settings['chain_reaction_delay'])


    def representation(self):
        return self.child.representation()

    @classmethod
    def parse_settings(cls, data):
        s = {}
        parse_positive_integer(s, data, 'bomb_speed', 'speed')
        parse_positive_integer(s, data, 'bomb_delay', 'left_before_explosion')
        parse_positive_integer(s, data, 'bomb_chain_reaction_delay', 'chain_reaction_delay')
        return s

