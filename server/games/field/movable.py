# encoding=utf-8
import math
import itertools
from games.field.layer import DynamicLayer
from games.field.cell import Cell
from games.field.bonus import Bonus, Flame, GoldenFlame
from games.field.durations import Duration
from games.field.utils import Vector
from games.field.utils import parse_positive_integer
from underlay import BreakableWall, UnbreakableWall, Bomb, StaticUnderlay, BlackHole


from games.event_manager import (
        EVENT_DOWN, EVENT_LEFT, EVENT_RIGHT,
        EVENT_UP, EVENT_NONE, EVENT_START_BOMBING, EVENT_STOP_BOMBING,)



def get_cell_center(cell):
    return (cell * 2 + (1, 1,)) * (Cell.SIZE // 2)


class Movable(DynamicLayer):
    DIRECTION_LEFT = Vector(-1, 0)
    DIRECTION_RIGHT = Vector(1, 0)
    DIRECTION_UP = Vector(0, -1)
    DIRECTION_DOWN = Vector(0, 1)
    DIRECTION_NONE = Vector(0, 0)

    obstacles = ()


    def __init__(self, *args, **kwargs):
        super(Movable, self).__init__(*args, **kwargs)
        self.direction = self.DIRECTION_NONE
        self.speed = 0
        self.direction_multiplier = 1


    def _get_offset(self):
        return Vector(self.x, self.y)

    def _set_offset(self, v):
        self.x, self.y = v

    offset = property(_get_offset, _set_offset)

    @property
    def location(self):
        return Vector(self.cell.x, self.cell.y)

    @property
    def absolute_offset(self):
        return self.location * Cell.SIZE + self.offset


    def set_direction(self, dir):
        self.direction = dir

    def set_speed(self, speed):
        self.speed = speed

    def set_direction_multiplier(self, multiplier):
        self.direction_multiplier = multiplier

    def get_direction_multiplier(self):
        return self.direction_multiplier

    def jump(self, v):
        CELL_HALF = Cell.SIZE // 2
        v = Vector(v)
        d = v.sign()
        next_pos = map(abs, self.offset + v)

        if next_pos[0] > CELL_HALF or next_pos[1] > CELL_HALF:
            self.cell.pop_layer(self)
            next_cell = Vector.from_cell(self.cell) + d
            self.cell.map.get_cell(*next_cell).set_top_layer(self)
            self.offset -= d * Cell.SIZE
            layer = self.cell.top_layer##TODO move this to player
            while layer:
                if isinstance(layer, BlackHole):
                    self.dead = True
                layer = layer.child
        
        self.offset += v

    def move(self, d, dir):
        if not d or not dir:
            return [], [], 0

        CELL_HALF = Cell.SIZE // 2
        pos = Vector(self.location)
        cell_center = Vector(CELL_HALF, CELL_HALF)
        offset = self.DIRECTION_NONE if dir * self.offset <= 0 else dir

        realpos = pos * Cell.SIZE + cell_center + self.offset + dir * CELL_HALF
        obstacle = pos + dir + offset #coordinates of next obstacle
        border = obstacle.to_border(dir) #border of obstacle
        dir_ort = dir.ort
        diff = (border * Cell.SIZE - realpos - dir * d) * dir

        if diff  < 0:
            cells = (obstacle + dir_ort, obstacle, obstacle - dir_ort)
            obstacles = []
            intersections = []

            for cell in cells:
                map_cell = self.cell.map.get_cell(*cell)
                adjacent = get_cell_center(cell)
                dist = abs(sum((realpos - adjacent) & dir_ort))

                if self.is_obstacle(map_cell) and dist < Cell.SIZE:
                    obstacles.append(map_cell)
                    intersections.append(dist)

            if len(obstacles):
                self.jump(dir * (d + diff))
                return obstacles, intersections, abs(diff)

        self.jump(dir * d)
        return [], [], 0


    def step(self):
        _move_res = self.move(self.speed, self.direction * self.direction_multiplier)
        obstacles, intersections, remain = _move_res

        if len(obstacles) != 1 or remain <= 0 or obstacles[0] is None:
            return [self]
        
        CELL_HALF = Cell.SIZE // 2
        pos = self.absolute_offset + (CELL_HALF, CELL_HALF,)
        cell = get_cell_center(Vector.from_cell(obstacles[0]))
        slide = self.direction * self.direction_multiplier + (pos - cell).sign()
        intersection = Cell.SIZE - intersections[0]

        if intersection <= self.settings['slide']:
            slideLen = min(intersection, remain)
            self.jump(slide * slideLen)

            if remain > slideLen:
                self.jump(self.direction * ((remain - slideLen)*self.direction_multiplier))

        return [self]


    def is_obstacle(self, cell):
        if cell is None:
            return True

        layer = cell.top_layer
        while layer is not None:
            for obstacle in self.obstacles:
                if isinstance(layer, obstacle):
                    return True
            layer = layer.child

        return False


    def representation(self):
        return self.child.representation()


    @classmethod
    def parse_settings(cls, data):
        s = {}
        parse_positive_integer(s, data, 'player_base_speed', 'base_speed')
        parse_positive_integer(s, data, 'max_slide_length', 'slide')
        return s


class Player(Movable):
    obstacles = (
       BreakableWall,
       UnbreakableWall,
       Bomb,
    )

    PRIORITY = -1

    def __init__(self, number, *args, **kwargs):
        super(Player, self).__init__(*args, **kwargs)
        self.number = number
        self.speed = self.settings['base_speed']
        self.dead = False
        self.bombing = False
        self.look_direction = Movable.DIRECTION_DOWN
        self.cancel_bombing_in_next_tick = False
        self.bomb_count = 0
        self.flame = 0
        self.bonuses = []
        self.bonus_count = {}
        self.diseases = []
        self.resurrect_time = None


    def set_direction(self, dir):
        if dir != Movable.DIRECTION_NONE:
            self.look_direction = dir

        super(Player, self).set_direction(dir)


    def take_bonus(self, bonus):
        if self.bonus_count.get(bonus.__class__, 0) >= bonus.settings['max_count']:
            return

        if isinstance(bonus, Bomb):
            self.bomb_count += 1

        if isinstance(bonus, Duration):
            for d in self.diseases:
                d.cancel(self)
            self.diseases = [bonus]
        elif isinstance(bonus, Bonus):
            self.bonus_count[bonus.__class__] = self.bonus_count.get(bonus.__class__, 0) + 1
            self.bonuses.append(bonus)

        bonus.apply(self)


    def reset_status(self):
        self.dead = False
        self.speed = self.settings['base_speed']
        self.direction = Movable.DIRECTION_NONE
        self.bombing = False
        self.flame = 0
        self.bomb_count = 0
        self.bonuses = []
        self.bonus_count = dict()
        self.diseases = []
        self.resurrect_time = None
        self.cancel_bombing_in_next_tick = False


    def apply_bonuses(self):
        to_remove = []

        for i, bonus in enumerate(self.bonuses):
            if bonus.spend():
                bonus.cancel(self)
                to_remove.append(i)
            else:
                bonus.apply(self)

        for i, index in enumerate(to_remove):
            bonus = self.bonuses.pop(index - i)
            self.bonus_count[bonus.__class__] -= 1


    def apply_disease(self):
        to_remove = []

        for i, disease in enumerate(self.diseases):
            if disease.spend():
                disease.cancel(self)
                to_remove.append(i)
            else:
                disease.apply(self)

        for i, index in enumerate(to_remove):
            self.diseases.pop(index - i)


    def _on_enable_bombs(self):
        self.bombing = True
        self.cancel_bombing_in_next_tick = False


    def _on_disable_bombs(self):
        #self.bombing = False
        self.cancel_bombing_in_next_tick = True

    _event_directions = (
        (EVENT_LEFT, Movable.DIRECTION_LEFT),
        (EVENT_RIGHT, Movable.DIRECTION_RIGHT),
        (EVENT_DOWN, Movable.DIRECTION_DOWN),
        (EVENT_UP, Movable.DIRECTION_UP),
        (EVENT_NONE, Movable.DIRECTION_NONE)
    )

    _event_delegates = \
        [(e, (set_direction, d)) for e, d in _event_directions] + [
            (EVENT_START_BOMBING, (_on_enable_bombs,),),
            (EVENT_STOP_BOMBING, (_on_disable_bombs,),),
        ]

    _actionable_events = [
        (e, h,) for e, h in _event_delegates
            if e != EVENT_NONE
    ]


    def delegate_events(self):
        def swallow(f, *real_args):
            return lambda *args, **kwargs: f(self, *real_args)

        for event, handler_info in self._event_delegates:
            h = swallow(*handler_info)
            self.cell.map.event_manager.on(event, self.number, h, self)



    def step(self):
        self.direction_multiplier = 1

        if self.dead:
            return []

        result = super(Player, self).step()

        for layer in self.cell.iterate():
            if not isinstance(layer, DynamicLayer):
                break
            if isinstance(layer, Bonus):
                self.take_bonus(layer)
                self.cell.pop_layer(layer)

        self.apply_bonuses()
        self.apply_disease()

        if self.bombing:
            self.put_bomb()

        if self.cancel_bombing_in_next_tick:
            self.bombing = False
            self.cancel_bombing_in_next_tick = False

        return result



    def undelegate_events(self):
        for e, h in self._actionable_events:
            self.cell.map.event_manager.off(e, self.number, self)


    def put_bomb(self):
        if self.dead or self.bomb_count == 0:
            return

        layer = self
        while layer.child is not None and not isinstance(layer.child, StaticUnderlay):
            if isinstance(layer.child, Bomb):
                return
            layer = layer.child

        self.bomb_count -= 1

        self.cell.set_underlay(Bomb(self.cell.map.step_number, self, self.flame, map=self.cell.map), layer)


    def blow_up(self):
        self.dead = True


    @classmethod
    def parse_settings(cls, data):
        s = super(Player, cls).parse_settings(data)
        try:
            parse_positive_integer(s, data, 'player_resurrect_time', 'resurrect_time')
        except ValueError:
            pass
        return s
