from bonus import Bonus
from games.field.utils import parse_positive_integer

class Duration(Bonus):
    affect_time = 0


    def __init__(self, *args, **kwargs):
        super(Duration, self).__init__(*args, **kwargs)
        self.affect_time = self.settings['affect_time']


    def apply(self, layer):
        super(Duration, self).apply(layer)
        self.affect_time -= 1


    def spend(self):
        return self.affect_time <= 0


    @classmethod
    def parse_settings(cls, data):
        s = super(Duration, cls).parse_settings(data)
        parse_positive_integer(s, data, 'items.%s.duration' % cls.ITEM_NAME, 'affect_time')
        return s


class Invert(Duration):
    code = 'i'
    ITEM_NAME = 'invert'
    ITEM_INDEX = 6

    def __init__(self, *args, **kwargs):
        super(Invert, self).__init__(*args, **kwargs)


    def apply(self, layer):
        super(Invert, self).apply(layer)
        layer.set_direction_multiplier(-layer.get_direction_multiplier())


class AbstractSpeedDuration(Duration):
    def __init__(self, *args, **kwargs):
        super(AbstractSpeedDuration, self). __init__(*args, **kwargs)
        self.player_speed = None


    def apply(self, layer):
        super(AbstractSpeedDuration, self).apply(layer)
        if self.player_speed is None:
            self.player_speed = layer.speed
        layer.speed = self.settings['speed']


    def cancel(self, layer):
        layer.speed = self.player_speed


    @classmethod
    def parse_settings(cls, data):
        s = super(AbstractSpeedDuration, cls).parse_settings(data)
        parse_positive_integer(s, data, 'items.%s.speed' % cls.ITEM_NAME, 'speed')
        return s



class LightSpeed(AbstractSpeedDuration):
    code = 'c'
    ITEM_NAME = 'lightspeed'
    ITEM_INDEX = 7



class Turtle(AbstractSpeedDuration):
    code = 'u'
    ITEM_NAME = 'turtle'
    ITEM_INDEX = 10



class AutoBomb(Duration):
    code = 'a'
    ITEM_NAME = 'autobomb'
    ITEM_INDEX = 8


    def apply(self, layer):
        super(AutoBomb, self).apply(layer)
        layer.bombing = True



class NoBomb(Duration):
    code = 'n'
    ITEM_NAME = 'nobomb'
    ITEM_INDEX = 9


    def apply(self, layer):
        super(NoBomb, self).apply(layer)
        layer.bombing = False




