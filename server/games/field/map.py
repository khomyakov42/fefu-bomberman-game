from games.field import bonus, underlay, movable, durations
from games.field.layer import DynamicLayer, StaticLayer
from games.field.bonus import Bonus
from games.field.underlay import Empty, StartPoint
from games.field.cell import Cell
from games.field.movable import Player
from games.event_manager import EventManager
from games.random import mt19937
import logging

logger = logging.getLogger('game')

class Map(object):
    layers = []
    bonuses = []

    def __init__(self, width, height, event_manager, settings, seed=0):
        self.width = width
        self.height = height
        self.event_manager = event_manager
        self.map = [[None for _ in xrange(self.height)] for _ in xrange(self.width)]
        self.start_position = []
        self.free_position = {}
        self.step_number = 0
        self.settings = settings
        self.random = mt19937(seed)

        self.layer_group = {}
        self.to_remove = set()
        self.to_append = set()
        self.updatable_layers = {}

        for i in xrange(self.width):
            for j in xrange(self.height):
                self.set_cell(i, j, Empty(self))


    def drop_bonus(self):
        f = self.random.next()
        if f % 100 < self.settings.drop_probability and self.settings.total_weight != 0:
            r = self.random.next()
            r %= self.settings.total_weight
            sum = 0
            for cl_bonus in Map.bonuses:
                sum += self.settings.get_settings(cl_bonus)['weight']
                if sum > r:
                    return cl_bonus(map=self)
        return None


    def get_layers_by_class(self, class_layer):
        return self.layer_group.get(class_layer, set())


    def set_cell(self, x, y, cell_layers):
        self._check_coordinate(x, y)
        cell = Cell(x, y, self, cell_layers)
        if isinstance(cell_layers, StartPoint):
            for start_layer in self.start_position:
                if start_layer.get_number() == cell_layers.get_number():
                    raise ValueError('Start point, for player "%s" number, already exist.' % cell_layers.get_number())
            self.start_position.append(cell_layers)
            self.free_position[cell_layers.get_number()] = cell

        self.map[x][y] = cell
        cell.delegate_events()


    def get_cell(self, x, y):
        try:
            if x < 0 or y < 0:
                raise IndexError()
            return self.map[x][y]
        except IndexError:
            return None


    def add_player(self, number):
        pos = self.free_position.pop(number)
        player = Player(number, self)
        pos.set_top_layer(player)
        for cl_b in Map.bonuses:
            for i in xrange(self.settings.get_settings(cl_b)['start_count']):
                player.take_bonus(cl_b(map=self))


    def _check_coordinate(self, x, y):
        if not (0 <= x <= self.width or 0 <= y <= self.height):
            raise IndexError('Coordinates are outside the borders of the map.')


    def representation(self):
        result = []
        for j in xrange(self.height):
            line = ''
            for i in xrange(self.width):
                line += self.map[i][j].representation()
            result.append(line)

        return result


    def process_add_layer(self, layer):
        self.to_append.add(layer)
        if self.step_number == 0:
            self._post_process()


    def process_remove_layer(self, layer):
        self.to_remove.add(layer)
        if self.step_number == 0:
            self._post_process()


    def step(self):
        for class_layer in sorted(self.layer_group.keys(), cmp=lambda x, y: cmp(x.PRIORITY, y.PRIORITY), reverse=True):
            if issubclass(class_layer, DynamicLayer):
                for layer in self.layer_group[class_layer]:
                    if layer not in self.to_remove:
                        layer.step()

        self._post_process()
        self.step_number += 1


    def new_step(self):
        updatable_layers = {}
        count = 0
        for priority in sorted(self.updatable_layers.keys(), reverse=True):
            for layer in self.updatable_layers[priority]:
                if layer not in self.to_remove:
                    count += 1
                    ls = layer.step()
                    for u in ls:
                        if not updatable_layers.has_key(u.PRIORITY):
                            updatable_layers[u.PRIORITY] = set()

                        updatable_layers[u.PRIORITY].add(u)


        self.updatable_layers = updatable_layers
        self._post_process()
        self.step_number += 1
        logger.info('process %s layers' % count)



    def _post_process(self):
        for layer in self.to_remove:
            try:
                self.layer_group.get(layer.__class__, set()).remove(layer)
            except KeyError:
                pass

            for cl in (Player, underlay.Bomb, underlay.Explosion):
                if isinstance(layer, cl):
                    if not self.updatable_layers.has_key(layer.PRIORITY):
                        self.updatable_layers[layer.PRIORITY] = set()
                    self.updatable_layers[layer.PRIORITY] -= set([layer])


        for layer in self.to_append:
            if not self.layer_group.has_key(layer.__class__):
               self.layer_group[layer.__class__] = set()
            self.layer_group[layer.__class__].add(layer)

            for cl in (Player, underlay.Bomb, underlay.Explosion):
                if isinstance(layer, cl):
                    if not self.updatable_layers.has_key(layer.PRIORITY):
                        self.updatable_layers[layer.PRIORITY] = set()
                    self.updatable_layers[layer.PRIORITY].add(layer)
                    break

        self.to_append, self.to_remove = set(), set()


    @classmethod
    def parse(cls, map, settings, event_manager=None, seed=None):
        event_manager = event_manager or EventManager()

        res = cls(len(map[0]) / 2, len(map), event_manager, settings, seed)
        for y, line in enumerate(map):
            for i in xrange(0, len(line), 2):
                x = i / 2
                code = line[i:i+2]
                layer = None
                root_layer = None
                next_layer = True
                while len(code) > 0 and next_layer:
                    next_layer = None
                    for cl_layer in cls.layers:
                        next_layer, new_code = cl_layer.parse(code, res) or (None, None)
                        if next_layer:
                            if root_layer:
                                layer.set_child(next_layer)
                            else:
                                root_layer = next_layer

                            code = new_code
                            layer = next_layer
                            break
                if not root_layer:
                    raise ValueError('Can not parse "%s"' % code)
                res.set_cell(x, y, root_layer)

        return res


    @staticmethod
    def register_layer(cl_layer):
        Map.layers.append(cl_layer)
        if issubclass(cl_layer, Bonus):
            Map.bonuses.append(cl_layer)
            Map.bonuses = sorted(Map.bonuses, cmp=lambda x, y: cmp(x.ITEM_INDEX, y.ITEM_INDEX))

    @staticmethod
    def get_registered_layers():
        return Map.layers


    @staticmethod
    def get_registered_bonus():
        return Map.bonuses


layers = [
    bonus.Speed,
    bonus.Flame,
    bonus.AdditionalBomb,
    bonus.GoldenFlame,
    bonus.Kicker,
    bonus.Throw,

    durations.AutoBomb,
    durations.Invert,
    durations.LightSpeed,
    durations.NoBomb,
    durations.Turtle,

    movable.Player,

    underlay.BreakableBonusWell,
    underlay.BreakableWall,
    underlay.UnbreakableWall,
    underlay.Empty,
    underlay.StartPoint,
    underlay.ArrowUp,
    underlay.ArrowDown,
    underlay.ArrowLeft,
    underlay.ArrowRight,
    underlay.BlackHole,
    underlay.Bomb,
    underlay.Explosion
]

for layer in layers:
    Map.register_layer(layer)

