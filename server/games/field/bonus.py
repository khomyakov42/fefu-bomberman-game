from games.field.layer import DynamicLayer
from games.field.utils import parse_positive_integer


class Bonus(DynamicLayer):
    ITEM_NAME = None
    ITEM_INDEX = None

#    def representation(self):
#        code = self.child.representation()
#        return self.code + code[1:]


    def apply(self, layer):
        pass


    def cancel(self, layer):
        pass


    def spend(self):
        return False


    def blow_up(self):
        from games.field.underlay import BreakableWall
        if not isinstance(self.parent, BreakableWall):
            self.cell.pop_layer(self)


    @classmethod
    def parse_settings(cls, data):
        s = {}
        for f in ('weight', 'max_count', 'start_count'):
            try:
                d = data['items'][cls.ITEM_NAME]
                m = {}
                parse_positive_integer(m, d, f, f)
                if f == 'start_count' and d[f] > d['max_count']:
                    raise ValueError()

                s[f] = int(d[f])
            except (KeyError, ValueError, TypeError):
                raise ValueError('can not parse "%s"' % f)

        return s


class OneApplyBonus(Bonus):
    def __init__(self, *args, **kwargs):
        super(OneApplyBonus, self).__init__(*args, **kwargs)
        self.applied = False


    def apply(self, layer):
        super(OneApplyBonus, self).apply(layer)
        self.applied = True

    def spend(self):
        return self.applied



class AdditionalBomb(OneApplyBonus):
    code = 'b'
    ITEM_NAME = 'bomb'
    ITEM_INDEX = 0


    def apply(self, layer):
        super(AdditionalBomb, self).apply(layer)
        layer.bomb_count += self.settings['additional']


    @classmethod
    def parse_settings(cls, data):
        s = super(AdditionalBomb, cls).parse_settings(data)
        parse_positive_integer(s, data, 'items.%s.bomb_bonus' % cls.ITEM_NAME, 'additional')
        return s


class Flame(OneApplyBonus):
    code = 'f'
    ITEM_NAME = 'flame'
    ITEM_INDEX = 1


    def apply(self, layer):
        super(Flame, self).apply(layer)
        layer.flame += self.settings['flame']


    @classmethod
    def parse_settings(cls, data):
        s =  super(Flame, cls).parse_settings(data)
        parse_positive_integer(s, data, 'items.%s.flame_bonus' % cls.ITEM_NAME, 'flame')
        return s



class GoldenFlame(Flame):
    code = 'g'
    ITEM_NAME = 'goldenflame'
    ITEM_INDEX = 2


class Speed(OneApplyBonus):
    code = 's'
    ITEM_NAME = 'speed'
    ITEM_INDEX = 3


    def apply(self, layer):
        super(Speed, self).apply(layer)
        layer.speed += self.settings['speed']


    @classmethod
    def parse_settings(cls, data):
        s =  super(Speed, cls).parse_settings(data)
        parse_positive_integer(s, data, 'items.%s.speed_bonus' % cls.ITEM_NAME, 'speed')
        return s


class Kicker(OneApplyBonus):
    code = 'k'
    ITEM_NAME = 'kicker'
    ITEM_INDEX = 4


class Throw(OneApplyBonus):
    code = 't'
    ITEM_NAME = 'throw'
    ITEM_INDEX = 5