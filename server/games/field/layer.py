from games.field.cell import Cell

class Layer(object):
    code = None
    PRIORITY = 0

    def __init__(self, map, child=None, parent=None, code=None):
        self.code = self.code or code
        self.parent = None
        self.child = None

        self.map = None
        self.cell = None
        self.settings = None
        self.x, self.y = 0, 0

        self.set_parent(parent)
        self.set_child(child)
        self.set_map(map)


    def set_map(self, map):
        self.map = map
        self.settings = map.settings.get_settings(self.__class__)


    def set_parent(self, parent):
        if self.parent != parent:
            self.parent = parent
            if parent: self.parent.set_child(self)


    def set_child(self, child):
        if self.child != child:
            self.child = child
            if child: self.child.set_parent(self)


    def parent(self):
        return self.parent


    def set_cell(self, cell):
        self.cell = cell


    def step(self):
        return []


    def blow_up(self):
        pass


    def get_priority(self):
        return self.PRIORITY


    def set_position(self, x, y):
        self.x, self.y = x, y


    def delegate_events(self):
        pass


    def undelegate_events(self):
        pass


    def get_absolute_position(self):
        return self.cell.x * Cell.SIZE + self.x, self.cell.y * Cell.SIZE + self.y


    def get_position(self):
        return self.x, self.y


    def representation(self):
        return self.code

    @classmethod
    def parse(cls, code, map):
        if code[0] == cls.code:
            return cls(map), code[1:]


    @classmethod
    def parse_settings(cls, data):
        pass



class DynamicLayer(Layer): pass

class StaticLayer(Layer): pass