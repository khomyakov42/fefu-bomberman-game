
class Cell(object):
    SIZE = 10000

    def __init__(self, x, y, map, top_layer=None):
        self.top_layer = top_layer
        self.x, self.y = x, y
        self.map = map

        self._map_layers(lambda el: el.set_cell(self))
        self._map_layers(lambda el: self._process_layer(el, pop=False))


    def set_top_layer(self, layer, underlay=None):
        underlay = underlay or self.top_layer
        parent = underlay.parent if underlay is not None else None
        layer.set_child(underlay)
        if parent:
            parent.set_child(layer)
        layer.set_cell(self)
        layer.delegate_events()
        self._process_layer(layer, pop=False)

        if underlay == self.top_layer or self.top_layer is None:
            self.top_layer = layer


    def set_underlay(self, underlay, layer):
        child = layer.child
        layer.set_child(underlay)
        if child:
            child.set_parent(underlay)

        underlay.set_cell(self)
        underlay.delegate_events()
        self._process_layer(underlay, pop=False)


    def pop_layer(self, layer):
        child = self.top_layer
        while child is not None:
            if child == layer:
                child.undelegate_events()
                if child == self.top_layer:
                    child.child.set_parent(None)
                    self.top_layer = child.child
                else:
                    parent = child.parent
                    parent.set_child(child.child)

                #child.set_cell(None)
                child.set_parent(None)
                child.set_child(None)
                self._process_layer(child, pop=True)
                break
            child = child.child


    def representation(self):
        return self.top_layer.representation()


    def delegate_events(self):
        self._map_layers(lambda x: x.delegate_events())


    def undelegate_events(self):
        self._map_layers(lambda x: x.undelegate_events())


    def _process_layer(self, layer, pop=True):
        if pop:
            self.map.process_remove_layer(layer)
        else:
            self.map.process_add_layer(layer)


    def _map_layers(self, func):
        child = self.top_layer
        while child is not None:
            func(child)
            child = child.child


    def iterate(self):
        node = self.top_layer
        while node is not None:
            yield node
            node = node.child
