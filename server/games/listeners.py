from auth.decorators import check_auth
from main.decorators import socket_event, http_event
from games.manager import GameManager
from events_and_status import *

@socket_event
@check_auth
def game_action(conn, db, data):
    GameManager().process_event(conn, EVENT_GAME_ACTION, data)


@http_event
@check_auth
def debug_game_action(conn, db, data):
    GameManager().process_event(conn, EVENT_GAME_ACTION, data['game_action'])
    conn.send(STATUS_OK)