from multiprocessing import Pipe
from games.game import Game
import threading
import logging
import math
import time

logger = logging.getLogger('game_manager')


class Sender(threading.Thread):
    def __init__(self, manager, start, count):
        super(Sender, self).__init__()
        self.manager = manager
        self._stop = False
        self.start_index = start
        self.end_index = self.start_index + count


    def stop(self):
        self._stop = True


    def run(self):
        dt = 0.033
        last_time = time.time()
        while not self._stop:
            for game in self.manager.games[self.start_index: self.end_index]:
                conn = self.manager.conn_game[game]
                while conn.poll(0):
                    player_number, event, data = conn.recv()
                    if self.manager.ws_conns_game[game].has_key(player_number):
                        self.manager.ws_conns_game[game][player_number].send(event, data)

            work_time = time.time() - last_time
            while time.time() - last_time < dt:
                time.sleep(dt - work_time)

            last_time = time.time()



class GameManager(object):
    MAX_GAMES_PER_SENDER = 8


    def __new__(cls):
        if not hasattr(cls, 'instance'):
            cls.instance = super(GameManager, cls).__new__(cls)
            cls.cl__init__()

        return cls.instance

    @classmethod
    def cl__init__(cls):
        cls.games = []
        cls.conn_game = {}
        cls.ws_conns_game = {}
        cls.numbers_of_players = {}
        cls.game_user = {}
        cls.senders = []


    def create_game(self, users, connections, map, settings, debug_mode=False, seed=0):
        for index, game in filter(lambda tuple: not tuple[1].is_alive(), enumerate(self.games)):
            try:
                self.games.pop(index)
            except IndexError:
                pass
            self.conn_game.pop(game, None)
            self.ws_conns_game.pop(game, None)
            logger.info('deleted game %s' % game)\


        conn1, conn2 = Pipe()
        game = Game(
            conn2,
            [dict(username=user.login) for user in users],
            map,
            settings,
            debug_mode,
            seed
        )

        ws_conns = {}
        for number, user in enumerate(users):
            self.numbers_of_players[user.id] = number
            self.game_user[user.id] = game
            if connections.has_key(user.id):
                ws_conns[number] = connections[user.id]

        self.conn_game[game] = conn1
        self.games.append(game)
        self.ws_conns_game[game] = ws_conns

        self._allocate_senders()

        game.daemon = True
        game.start()
        logger.info('created game')


    def get_games(self):
        return self.games


    def process_event(self, ws_conn, event, data=None):
        logger.info('process event %s %s from user_id %s' % (event, data, ws_conn.user_id))
        data = data if data is not None else {}
        if not self.game_user.has_key(ws_conn.user_id):
            logger.info('can not find game from user_id %s' % ws_conn.user_id)
            return

        conn = self.conn_game[self.game_user[ws_conn.user_id]]
        conn.send((self.numbers_of_players[ws_conn.user_id], event, data))


    def reset(self):
        for game in self.games:
            game.terminate()
            game.join()

        self._clear_senders()
        self.games = []
        self.conn_game = {}
        self.ws_conns_game = {}
        self.numbers_of_players = {}
        self.game_user = {}
        logger.info('reset')


    def _allocate_senders(self):
        wantage = int(math.ceil(float(len(self.games)) / self.MAX_GAMES_PER_SENDER) - len(self.senders))
        if wantage > 0:
            for i in xrange(wantage):
                sender = Sender(self, len(self.senders) * self.MAX_GAMES_PER_SENDER, self.MAX_GAMES_PER_SENDER)
                sender.start()
                self.senders.append(sender)
            logger.info('allocated %s senders' % wantage)
        elif wantage < 0:
            for sender in self.senders[wantage:]:
                sender.stop()
                sender.join()
            logger.info('remove %s senders')



    def _clear_senders(self):
        for sender in self.senders:
            sender.stop()
            sender.join()

        self.senders = []