#!/usr/bin/env python
from tornado import web
from tornado import ioloop
import threading
import time
import os
import sys


def startTornado(port):
    ROOT_DIR = os.path.dirname( __file__ )
    STATIC_DIR = os.path.join(ROOT_DIR, 'static')

    class IndexHandler(web.RequestHandler):
        def get(self, *args, **kwargs):
            self.render('index.html')


    app = web.Application([
        (r'/static/(.*)', web.StaticFileHandler, {'path': STATIC_DIR}),
        (r'.*', IndexHandler)
    ])
    app.listen(port)
    ioloop.IOLoop.instance().start()


def stopTornado():
    ioloop.IOLoop.instance().stop()


if __name__ == '__main__':
    PORT = 8000
    for arg in sys.argv[1:]:
        if arg[:5] == 'port=':
            port = arg.split('=')
            if len(port) != 2:
                print 'bad port'
            else:
                PORT = port[1]
        else:
            print '\
            port=\trun client port, default=%s\n\
            ' % PORT
            sys.exit()

    try:
        threading.Thread(target=startTornado, args=(PORT,)).start()
        print 'Client is running.'
        while True:
            time.sleep(1)
    except (KeyboardInterrupt, SystemExit):
        stopTornado()