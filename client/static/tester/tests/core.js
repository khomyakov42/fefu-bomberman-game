define([
    'tester/client'
], function(Client) {
    return {
        Register: {
            simple: function(t) {
                Client(0, t, 'test', 'test');
            }
        },

        Login: {
            simple: function(t) {
                Client(0, t, 'test', 'test');
                Client(0).register();
                Client(0).login();
                Client.delete(Client(0));
            }
        },

        UploadMap: {
            simple: function(t) {
                Client(0, t, 'test', 'test');
                Client(0).register();
                Client(0).login();
                Client(0).upload_map('simple', [
                    '0.....',
                    '......',
                    '....1.'
                ]);
                Client.delete(Client(0));
            }
        },

        CreateGame: {
            simple: function(t) {
                Client(0, t, 'test', 'test');
                Client(0).register();
                Client(0).login();

                var map_id = Client(0).upload_map('simple', [
                    '0.....',
                    '......',
                    '....1.'
                ])['map_id'];

                var map = Client(0).get_map(map_id);

                var game_id = Client(0).create_game('game', '', map_id, map.max_player_count, map.settings);

                Client.delete(Client(0));
            }
        },

        StartGame: {
            simple: function(t) {
                Client(0, t, 'pl1', 'pl1');
                Client(0).register();
                Client(0).login();
                Client(0).set_debug_mode();
                Client(1, t, 'pl2', 'pl2');
                Client(1).register();
                Client(1).login();

                var map_id = Client(0).upload_map('simple', [
                    '0.....',
                    '......',
                    '....1.'
                ])['map_id'];

                var map = Client(0).get_map(map_id);

                var game_id = Client(0).create_game('game', '', map_id, map.max_player_count, map.settings);
                Client(1).join_game(game_id, '');
                Client(0).auth();
                Client(1).auth();
                Client(1).on_game_started();
                Client(0).on_game_started();
                Client(0).start_game();
                Client(1).start_game();
                Client(0).wait_game_started();
                Client(1).wait_game_started();
                var game_state = Client(0).set_tick(0);

                Client.delete(Client(0));
            }
        }
    }
});