define([
    '_'
], function(_) {
    var Tester = function(init) {
        this.cl_protocol = init.protocol;
        this.url = init.url;
        this.extensions = init.extensions;
        this.timeout = init.timeout;

        this.ws_response = {};
        this.on_events = {};
        this.last_response = null;
        this.last_action = null;
        this.last_request = null;

        this.protocol = this.create_protocol();
    };

    var Error = function(action, data, res, error, critical) {
        this.action = action;
        this.data = data;
        this.responce = res;
        this.error = error;
        this.critical = critical;
    };

    _.extend(Tester.prototype, {
        create_protocol: function() {
            var complete = false;
            var res = null;
            var self = this;
            setTimeout(function() {
                res = new self.cl_protocol(this.url, this.extensions, function() {
                    complete = true;
                });
            }, 0);

            var s = new Date().getTime();
            while(new Date().getTime() - s < this.timeout + 1) {
                if (complete) {
                    return res;
                }
            }

            this.error('extensions', {'action': 'extensions'}, null, "Timeout");
        },


        error: function(action, data, responce, error, critical) {
            throw new Error(action, data, responce, error, critical);
        },



        write_error: function(group, name, e) {
            console.error(group, name, e.action, e.data, e.responce, e.error);
        },


        write_success: function(group, name) {
            console.info(group, name);
        },


        run: function(test_list) {
            for (var test_group in test_list) {
                var tests = _.isFunction(test_list[test_group]) ? {'': test_list[test_group]} : test_list[test_group];
                for (var test_name in tests) {
                    try {
                        tests[test_name](this);
                        this.write_success(test_group, test_name);
                    } catch (e) {
                        if (e instanceof Error) {
                            this.write_error(test_group, test_name, e);
                            if (e.critical) {
                                return;
                            }
                        } else {
                            throw e;
                        }
                    }
                }
            }
        }
//
//
//        http: function(action, data, protocol) {
//            protocol = protocol || this.protocol;
//
//            var response = null;
//            var error = null;
//            _.extend(data, {action: action});
//            this.last_action = action;
//            this.last_response = null;
//            this.last_request = data;
//
//            protocol.use().request(action, {
//                data: data,
//                success: function(data) {
//                    response = data;
//                },
//                error: function(e, data) {
//                    response = data;
//                }
//            });
//
//            var s = new Date().getTime();
//            while (new Date().getTime() - s < this.timeout) {
//                if (response || error) {
//                    this.last_response = response;
//                    return response;
//                }
//            }
//
//            this.last_action = null;
//            this.last_request = null;
//            this.last_response = null;
//            this.error(action, data, response, "Timeout");
//        },
//
//
//        wsck: function(action, data, protocol) {
//            protocol = protocol || this.protocol;
//            protocol.use()._ws_request(action, data);
//        },
//
//
//        on: function(action, protocol) {
//            protocol = protocol || this.protocol;
//            var self = this;
//            var f = function(data) {
//                self._process_ws_response(action, data);
//            };
//
//            this.on_events[action] = f;
//            protocol.use().on(action, f);
//        },
//
//
//        off: function(action, protocol) {
//            protocol = protocol || this.protocol;
//            if (this.on_events[action] != undefined) {
//                protocol.use().off(action, this.on_events[action]);
//                delete this.on_events[action];
//            }
//        },
//
//
//        off_all: function() {
//            for (var action in this.on_events) {
//                this.off(action);
//            }
//        },
//
//        wait_ws: function(action) {
//            var s = new Date().getTime();
//
//            while (new Date().getTime() - s < this.timeout) {
//                if (this.ws_response[action] != undefined) {
//                    var res = this.ws_response[action];
//                    delete this.ws_response[action];
//                    this.last_action = action;
//                    this.last_response = res;
//                    this.last_request = null;
//                    return res;
//                }
//            }
//
//            this.last_action = null;
//            this.last_request = null;
//            this.last_response = null;
//            this.error(action, '', {}, "Timeout");
//        },
//
//
//        check_status: function(statuses, responce, no_entry) {
//            responce = responce || this.last_response;
//            this.check_item_exist('status', responce);
//
//            if (!(statuses.indexOf(responce.status) == -1 && no_entry)
//                && (statuses.indexOf(responce.status) != -1 && !no_entry)) {
//
//                this.error(this.last_action, this.last_request, responce,
//                    no_entry ?
//                        '"status" should not be "' + statuses[statuses.indexOf(responce.status)] + '"' :
//                        '"status" should be "' + statuses.join(',') + '"'
//                );
//            }
//        },
//
//
//        check_status_in: function(statuses, responce) {
//            this.check_status(statuses, responce, true);
//        },
//
//
//        check_status_not_in: function(statuses, responce) {
//            this.check_status(statuses, responce, false);
//        },
//
//
//        check_item_exist: function(item, responce) {
//            responce = responce || this.last_response;
//            if (responce[item] == undefined) {
//                this.error(this.last_action, this.last_request, responce, 'no field "' + item + '"');
//            }
//        },
//
//
//        check_item_type: function(item, type, responce) {
//            responce = responce || this.last_response;
//            this.check_item_exist(item, responce);
//            var ok = false;
//            var val = responce[item];
//            switch (type) {
//                case 'str':
//                    ok = _.isString(val);
//                    break;
//                case 'int':
//                    ok = _.isNumber(val);
//                    break;
//                case 'dict':
//                    ok = _.isObject(val);
//                    break;
//                case 'list':
//                    ok = _.isArray(val);
//                    break;
//                case 'null':
//                    ok = _.isNull(val);
//                    break;
//            }
//
//            if (!ok) {
//                this.error(this.last_action, this.last_request, responce, 'field "' + item + '"must be of type "' + type + '"');
//            }
//        },
//
//
//        check_regexp: function(item, regexp, responce) {
//            responce = responce || this.last_response;
//            this.check_item_exist(item, responce);
//            this.check_item_type(item, 'str', responce);
//            if (!regexp.test(responce[item])) {
//                this.error(this.last_action, this.last_request, responce, 'field "' + item + '" does not match the regexp "' + regexp + '"');
//            }
//        },
//
//
//        _process_ws_response: function(action, data) {
//            if (this.ws_response[action] == undefined) {
//                this.ws_response[action] = [];
//            }
//            this.ws_response[action].push(data);
//        }
    });



    return Tester;
});