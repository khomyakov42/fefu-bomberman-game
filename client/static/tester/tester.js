define([
    '_'
], function(_) {
    var Tester = function(init, callback) {
        this.cl_protocol = init.protocol;
        this.url = init.url;
        this.extensions = init.extensions;
        this.timeout = init.timeout;
        this.store = {};
        this.last_request = {response: undefined, request: undefined, error: undefined};
        this.protocol = this.create_protocol(function() {callback();});
    };


    function chain(funcs) {
        var cb = function () { };

        if (!funcs.length) {
            return cb;
        }
        for (var i = funcs.length - 1; i >= 0; --i){
            (function(f, callback) {
                cb = function() {
                    f(callback);
                }
            })(funcs[i], cb);
        }
        return cb;
    }


    var ef = function() {};


    _.extend(Tester.prototype, {
        safe_http_request: function(protocol, method, data, error_callback, ext) {
            var success = data.success || ef;
            var error = data.error || ef;
            var self = this;
            var ret = false;


            data.success = function(res) {
                ret = true;
                self.last_request = {response: res, request: data.data, error: undefined};
                success.call(this, data.data || {}, res);
            };

            data.error = function(res) {
                ret = true;
                self.last_request = {response: undefined, request: data.data, error: res};
                error.call(this, data.data || {}, res);
            };

            protocol.use(ext)[method](data);

            setTimeout(function() {
                if (!ret) {
                    var error =  '504 Timeout ' + self.timeout;
                    self.last_request = {response: undefined, request: data.data, error: error};
                    error_callback(data.data || {}, error);
                }
            }, this.timeout);
        },


        create_protocol: function(callback) {
            return new this.cl_protocol(this.url, this.extensions, callback);
        },


        create_test: function(name, file, test) {
            if (_.isFunction(test)) {
                return this.create_test_by_func(name, file, test);
            }
        },

        run: function(test_list) {
            var tests = [];
            for (var test_name in test_list) {
                for (var i = 0; i < test_list[test_name].length; ++i) {
                    var subtest = test_list[test_name][i];
                    tests.push(this.create_test(test_name, '',subtest));
                }
                tests.push(this.realloc_store);
            }

            chain(tests)();
        },


        realloc_store: function(callback) {
            this.store = {};
            callback();
        },


        create_success_callback: function(file, name, callback) {
            return function(req, res, silent) {
                if (!silent) {
                    console.info(file, name, req, res);
                }
                callback();
            }
        },


        create_error_callback: function(file, name, callback) {
            return function(req, res, error, critical) {
                if (!critical) {
                    console.warn(file, name, req, res, error);
                    callback();
                } else {
                    console.error(file, name, req, res, error);
                }
            }
        },

        create_test_by_func: function(name, file, func) {
            var self = this;
            return function(callback) {
                var success = self.create_success_callback(file, name, callback);
                var error = self.create_error_callback(file, name, callback);
                func(self, self.store, success, error);
            }
        }
    });

    return Tester;
});