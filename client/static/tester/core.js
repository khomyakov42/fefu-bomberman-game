define([
    '$'
], function($) {
    var
        BAD_ACTION = 'unsupported_action',
        REGISTER = 'register',
        USER_EXIST = 'user_already_exists',
        SETTINGS = 'extensions',
        LOGIN = 'login',
        INVALID_CRIDENT = 'invalid_credentials',
        LOGOUT = 'logout',
        BAD_SID = 'invalid_sid',
        CREATE_GAME = 'create_game',
        ALREADY_IN_GAME = 'already_in_game',
        LEAVE_GAME = 'leave_game',
        NOT_IN_GAME = 'not_in_game',
        GET_GAME_LIST = 'list_games',
        OK = 'ok'
    ;


    var Store = {
        get: function(field) {
            var self = this;
            return function() {
                return self[field];
            }
        }
    };



    var register_task = {
        type: 'http',
        req: {
            action: REGISTER,
            username: 'test',
            password: 'test'
        }
    };

    var login_task = {
        type: 'http',
            req: {
            action: LOGIN,
                username: 'test',
                password: 'test'
        },
        res: {
            code: OK,
            fields: {
                sid: /^[a-zA-Z0-9]{32}$/
            },
            handler: function(data) {
                Store['sid'] = data.sid
            }
        }
    };


    var create_room = {
        type: 'http',
        req: {
            action: CREATE_GAME,
            sid: Store.get('sid'),
            name: 'first_room',
            password: ''
        },
        res: {
            code: OK,
            fields: {
                game_id: /^\d+$/
            },
            handler: function(data) { Store['game_id'] = data.game_id }
        }
    };


    var leave_game = {
        type: 'http',
        req: {
            action: LEAVE_GAME,
            sid: Store.get('sid')
        },
        res: {
            code: OK
        }
    };


    var login_and_create_room = [
        register_task,
        login_task,
        create_room
    ];


    return [

        ['http jsonp', function(tester, callback) {
            var response = false;
            var error = undefined;
            var result = undefined;

            tester.protocol().request(SETTINGS, {
                success: function(data) {
                    response = true;
                    result = data;
                },
                error: function(data) {
                    response = true;
                    error = data;
                }
            });

            setTimeout(function() {
                if (!response) {
                    callback({
                        critical: true,
                        req: {
                            action: SETTINGS
                        },
                        res: error || result,
                        error: 'Not supported JSONP',
                        ok: false
                    });
                } else {
                    callback({
                        critical: true,
                        req: {
                            action: SETTINGS
                        },
                        res: error || result,
                        ok: true
                    });
                }
            }, 1000);
        }],

        ['bad action', {
            type: 'http',
            req: {
                action: 'asdasda'
            },
            res: {
                code: BAD_ACTION
            }
        }],


        ['register empty login', {
            type: 'http',
            req: {
                action: REGISTER,
                username: '',
                password: '123'
            },
            res: {
                code__not: OK
            }
        }],


        ['register to short login', {
            type: 'http',
            req: {
                action: REGISTER,
                username: '1',
                password: '123'
            },
            res: {
                code__not: OK
            }
        }],

        ['register to long login', {
            type: 'http',
            req: {
                action: REGISTER,
                username: new Array(50).join('a '),
                password: '123'
            },
            res: {
                code__not: OK
            }
        }],

        ['register bad login format', {
            type: 'http',
            req: {
                action: REGISTER,
                username: '!@#$%^&*()_',
                password: '123'
            },
            res: {
                code__not: OK
            }
        }],

        ['register to long password', {
            type: 'http',
            req: {
                action: REGISTER,
                username: '123',
                password: new Array(257).join('1')
            },
            res: {
                code__not: OK
            }
        }],

        ['register to short password', {
            type: 'http',
            req: {
                action: REGISTER,
                username: 'test',
                password: 'test'
            },
            res: {
                code: OK
            }
        }],

        ['register double register user', [
            {
                type: 'http',
                req: {
                    action: REGISTER,
                    username: 'test',
                    password: 'test'
                }
            },
            {
                type: 'http',
                req: {
                    action: REGISTER,
                    username: 'test',
                    password: 'test'
                },
                res: {
                    code: USER_EXIST
                }
            }
        ]],

        ['login', [
            register_task,
            login_task
        ]],


        ['login bad username', [
            register_task,
            {
                type: 'http',
                req: {
                    action: LOGIN,
                    username: 'test1',
                    password: 'test'
                },
                res: {
                    code: INVALID_CRIDENT
                }
            }
        ]],


        ['login bad password', [
            register_task,
            {
                type: 'http',
                req: {
                    action: LOGIN,
                    username: 'test',
                    password: 'test1'
                },
                res: {
                    code: INVALID_CRIDENT
                }
            }
        ]],


        ['login double login', [
            register_task,
            login_task,
            login_task
        ]],


        ['logout', [
            register_task,
            login_task,
            {
                type: 'http',
                req: {
                    action: LOGOUT,
                    sid: Store.get('sid')
                },
                res: {
                    code: OK
                }
            },
            {
                type: 'http',
                req: {
                    action: LOGOUT,
                    sid: Store.get('sid')
                },
                res: {
                    code: BAD_SID
                }
            }
        ]],

        ['logout bad sid', [
            register_task,
            login_task,
            {
                type: 'http',
                req: {
                    action: LOGOUT,
                    sid: 'beb26bbb264b26f39fbca5f88c23ca5f'
                },
                res: {
                    code: BAD_SID
                }
            }
        ]],

        ['create room bad name', [
            register_task,
            login_task,
            {
                type: 'http',
                req: {
                    action: CREATE_GAME,
                    sid: Store.get('sid'),
                    name: '!#$@##@$',
                    password: ''
                },
                res: {
                    code__not: OK
                }
            }
        ]],

        ['create room',
            login_and_create_room.concat([
            {
                type: 'http',
                req: {
                    action: CREATE_GAME,
                    sid: Store.get('sid'),
                    name: 'first_room',
                    password: ''
                },
                res: {
                    code: ALREADY_IN_GAME
                }
            }
        ])],

        ['leave game',
            login_and_create_room.concat([
                {
                    type: 'http',
                    req: {
                        action: LEAVE_GAME,
                        sid: Store.get('sid')
                    },
                    res: {
                        code: OK
                    }
                }
            ])
        ],

        ['double leave game',
            login_and_create_room.concat([
                {
                    type: 'http',
                    req: {
                        action: LEAVE_GAME,
                        sid: Store.get('sid')
                    },
                    res: {
                        code: OK
                    }
                },
                {
                    type: 'http',
                    req: {
                        action: LEAVE_GAME,
                        sid: Store.get('sid')
                    },
                    res: {
                        code: NOT_IN_GAME
                    }
                }
            ])
        ],

        ['get game list',
            login_and_create_room.concat([
            {
                type: 'http',
                req: {
                    action: GET_GAME_LIST,
                    sid: Store.get('sid')
                },
                res: {
                    code: OK,
                    fields: {
                        games: function(games) {
                            if (!games) { return false; }
                            var ok = true;
                            for (var i = 0; i < games.length; ++i) {
                                ok = !_.difference(
                                    ['id', 'name', 'total_slots', 'free_slots'],
                                    _.keys(games[i])).length;
                            }
                            return ok && games.length == 1;
                        }
                    }
                }
            }])
        ],
        [''
        ]
    ]
});
