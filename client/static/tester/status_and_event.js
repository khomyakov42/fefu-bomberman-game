define([
], function() {
    return {
        OK: 'ok',
        REGISTER: 'register',
        LOGIN: 'login',
        UPLOAD_MAP: 'upload_map',
        CREATE_GAME: 'create_game',
        JOIN_GAME: 'join_game',
        GAME_STARTED: 'game_started',
        START_GAME: 'start_game',


        SET_DEBUG: 'game_set_debug_mode',
        SET_TICK: 'game_set_tick',


        AUTH: 'auth',
        AUTH_ERROR: 'auth_error',
        AUTH_SUCCESS: 'auth_success',
        PLAYER_JOINED: 'player_joined_game',
        GAME_ACTION: 'game_action'
    };
});