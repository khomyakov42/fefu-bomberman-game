define([
    'tester/status_and_event'
], function(ST) {
    var Client = function(id, tester, username, password) {
        if (Client.clients[id] == undefined) {
            Client.clients[id] = new Client._Client(tester, username, password);
        }
    };
    Client.clients = {};
    Client.delete = function(client) {
        if (Client.clients[client] != undefined) {
            Client.clients[client].off_all();
            delete Client.clients[client];
        }
    };

    Client._Client = function(tester, username, password) {
        this.username = username;
        this.password = password;
        this.tester = tester;

        this.protocol = this.tester.create_protocol();
        this.last_action = null;
        this.last_request = null;
        this.last_response = null;
        this._ws_response = {};
        this._on_events = {};
    };

    _.extend(Client._Client.prototype, {
        error: function() {
            this.tester.error.apply(this.tester, arguments);
        },

        http: function(action, data) {
            var response = null;
            var error = null;
            _.extend(data, {action: action});
            this.last_action = action;
            this.last_response = null;
            this.last_request = data;

            this.protocol.use().request(action, {
                data: data,
                success: function(data) {
                    response = data;
                },
                error: function(e, data) {
                    response = data;
                }
            });

            var s = new Date().getTime();
            while (new Date().getTime() - s < this.timeout) {
                if (response || error) {
                    this.last_response = response;
                    return response;
                }
            }

            this.last_action = null;
            this.last_request = null;
            this.last_response = null;
            this.error(action, data, response, "Timeout");
        },


        wsck: function(action, data) {
            this.protocol.use()._ws_request(action, data);
        },


        on: function(action, protocol) {
            protocol = protocol || this.protocol;
            var self = this;
            var f = function(data) {
                self._process_ws_response(action, data);
            };

            this._on_events[action] = f;
            protocol.use().on(action, f);
        },

        _process_ws_response: function(action, data) {
            if (this._ws_response[action] == undefined) {
                this._ws_response[action] = [];
            }
            this._ws_response[action].push(data);
        },

        off: function(action) {
            if (this._on_events[action] != undefined) {
                this.protocol.use().off(action, this._on_events[action]);
                delete this._on_events[action];
            }
        },

        off_all: function() {
            for (var action in this._on_events) {
                this.off(action);
            }
        },

        wait_ws: function(action) {
            var s = new Date().getTime();

            while (new Date().getTime() - s < this.timeout) {
                if (this._ws_response[action] != undefined) {
                    var res = this._ws_response[action];
                    delete this._ws_response[action];
                    this.last_action = action;
                    this.last_response = res;
                    this.last_request = null;
                    return res;
                }
            }

            this.last_action = null;
            this.last_request = null;
            this.last_response = null;
            this.error(action, '', {}, "Timeout");
        },

        wait_wss: function(actions) {
            var s = new Date().getTime();

            while (new Date().getTime() - s < this.timeout) {
                for (var i = 0; i < actions.length; ++i) {
                    if (this._ws_response[actions[i]] != undefined) {
                        var res = this._ws_response[actions[i]];
                        delete this._ws_response[actions[i]];
                        this.last_action = actions[i];
                        this.last_response = res;
                        this.last_request = null;
                        return {
                            event: actions[i],
                            response: res
                        };
                    }
                }
            }

            this.last_action = null;
            this.last_request = null;
            this.last_response = null;
            this.error(action, '', {}, "Timeout");
        },

        check_status: function(statuses, responce, no_entry) {
            responce = responce || this.last_response;
            this.check_item_exist('status', responce);

            if (!(statuses.indexOf(responce.status) == -1 && no_entry)
                && (statuses.indexOf(responce.status) != -1 && !no_entry)) {

                this.error(this.last_action, this.last_request, responce,
                    no_entry ?
                        '"status" should not be "' + statuses[statuses.indexOf(responce.status)] + '"' :
                        '"status" should be "' + statuses.join(',') + '"'
                );
            }
        },


        check_status_in: function(statuses, responce) {
            this.check_status(statuses, responce, true);
        },


        check_status_not_in: function(statuses, responce) {
            this.check_status(statuses, responce, false);
        },


        check_item_exist: function(item, responce) {
            responce = responce || this.last_response;
            if (responce[item] == undefined) {
                this.error(this.last_action, this.last_request, responce, 'no field "' + item + '"');
            }
        },


        check_item_type: function(item, type, responce) {
            responce = responce || this.last_response;
            this.check_item_exist(item, responce);
            var ok = false;
            var val = responce[item];
            switch (type) {
                case 'str':
                    ok = _.isString(val);
                    break;
                case 'int':
                    ok = _.isNumber(val);
                    break;
                case 'dict':
                    ok = _.isObject(val);
                    break;
                case 'list':
                    ok = _.isArray(val);
                    break;
                case 'null':
                    ok = _.isNull(val);
                    break;
            }

            if (!ok) {
                this.error(this.last_action, this.last_request, responce, 'field "' + item + '"must be of type "' + type + '"');
            }
        },


        check_regexp: function(item, regexp, responce) {
            responce = responce || this.last_response;
            this.check_item_exist(item, responce);
            this.check_item_type(item, 'str', responce);
            if (!regexp.test(responce[item])) {
                this.error(this.last_action, this.last_request, responce, 'field "' + item + '" does not match the regexp "' + regexp + '"');
            }
        },

        ///////////////////////////////////////HTTP////////////////////////////////

        register: function() {
            var res = this.http({
                action: ST.REGISTER,
                username: this.username,
                password: this.password
            });

            this.check_status_in([ST.OK], res);
            return res;
        },

        login: function() {
            var res = this.http({
                action: ST.LOGIN,
                username: this.username,
                password: this.password
            });

            this.check_status_in([ST.OK], res);
            this.check_regexp('sid', /^[a-zA-Z0-9]{32}$/, res);
            this.sid = res['sid'];
            return res;
        },

        upload_map: function(name, field, settings) {
            settings = settings || this.tester.protocol.use().default_game_settings;

            var res = this.http({
                action: ST.UPLOAD_MAP,
                name: name,
                field: field,
                settings: settings,
                sid: this.sid
            });

            this.check_status_in([ST.OK], res);
            this.check_item_exist('map_id', res);
            this.check_item_type('int', res);
            this.map_id = res['map_id'];
            this.settings = settings;
            return res;
        },


        get_map: function(map_id) {
            var res = this.http({
                map_id: map_id,
                sid: this.sid
            });

            this.check_status_in([ST.OK], res);
            this.check_item_type('id', 'int', res);
            this.check_item_type('max_player_count', 'int', res);
            this.check_regexp('name', /^.{1,32}$/, res);
            this.check_item_type('settings', 'dict', res);
            this.check_item_type('field', 'list', res);
            return res;
        },


        create_game: function(name, password, map_id, max_player_count, settings) {
            var res = this.http({
                action: ST.CREATE_GAME,
                name: name,
                password: password,
                map_id: map_id || this.map_id,
                max_player_count: max_player_count,
                settings: settings || this.settings,
                sid: this.sid
            });

            this.check_status_in([ST.OK], res);
            this.check_item_exist('game_id', res);
            this.check_item_type('game_id', 'int');
            this.game_id = res['game_id'];
            return res;
        },


        join_game: function(game_id, password) {
            var res = this.http({
                action: ST.JOIN_GAME,
                game_id: game_id,
                password: password,
                sid: this.sid
            });

            this.check_status_in([ST.OK], res);
            this.check_item_type('game_id', 'int');
            this.check_item_type('settings', 'dict');
            this.game_id = game_id;
            return res;
        },


        start_game: function() {
            var res = this.http({
                action: ST.START_GAME,
                sid: this.sid
            });

            this.check_status_in([ST.OK], res);
            return res;
        },


        set_debug_mode: function(seed) {
            var res = this.http({
                action: ST.SET_DEBUG,
                sid: this.sid,
                random_seed: seed || 0
            });
            this.check_status_in([ST.OK], res);
            return res;
        },


        set_tick: function(tick) {
            var res = this.http({
                action: ST.SET_TICK,
                sid: this.sid,
                tick: tick
            });
            this.check_status_in([ST.OK], res);
            return res;
        },


        /////////////////////////////////WS//////////////////////////////////

        auth: function() {
            this.on_auth_error();
            this.on_auth_success();
            this.wsck(ST.AUTH, {
                sid: this.sid
            });
            var res = this.wait_wss([ST.AUTH_ERROR, ST.AUTH_SUCCESS]);
            if (res['event'] == ST.AUTH_ERROR) {
                this.error(ST.AUTH, this.last_request, this.last_response, "socket can not auth");
            }
            return res['response'];
        },


        game_action: function(direction, bombing) {
            this.wsck(ST.GAME_ACTION, {
                direction: direction,
                action: bombing ? 'place_bomb' : 'none'
            });
        },

        /////////////////////////////////EVENTS//////////////////////////////

        on_auth_success: function() {
            this.on(ST.AUTH_SUCCESS);
        },

        on_auth_error: function() {
            this.on(ST.AUTH_ERROR);
        },

        on_game_started: function() {
            this.on(ST.GAME_STARTED);
        },

        wait_game_started: function() {
            return this.wait_ws(ST.GAME_STARTED);
        },

        on_player_joined: function() {
            this.on(ST.PLAYER_JOINED);
        },

        wait_player_joined: function() {
            return this.wait_ws(ST.PLAYER_JOINED);
        }
    });

    return Client;
});