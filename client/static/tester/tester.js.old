define([
    '_'
], function(_) {
    var Tester = function(protocol, timeout) {
        this.pr = protocol;
        this.timeout = timeout;
        this.response = null;
        this.data = {};
    };

    function chain(funcs) {
        var cb = function () { };

        if (!funcs.length) {
            return cb;
        }
        for (var i = funcs.length - 1; i >= 0; --i){
            (function(f, callback) {
                cb = function() {
                    f(callback);
                }
            })(funcs[i], cb);
        }
        return cb;
    }


    var http_response_handler = function(add, tester, ex_res, data, callback) {
        var ok = true;
        var error = '';
        ex_res = ex_res || {};

        if (ex_res.code) {
            var codes = _.isArray(ex_res.code) ? ex_res.code : [ex_res.code];

            var st = _.reduce(codes, function(a, b) {
                return a || b == data.status;
            }, false);
            ok = st && ok;

            error = !st ? 'Status "' + data.status + '" not in ("' + codes.join('", "') + '")\n' : '';
        } else if (ex_res.code__not) {
            var codes = _.isArray(ex_res.code_not) ? ex_res.code__not : [ex_res.code__not];

            var st = _.reduce(codes, function(a, b) {
                return a || b != data.status;
            }, false);
            ok = st && ok;

            error = !st ? 'Status "' + data.status + '" in ("' + codes.join('", "') + '")' : '';
        }

        if (ex_res.fields) {
            if (_.isArray(ex_res.fields)) {
                var st = !_.difference(ex_res.fields, _.keys(data)).length;
                ok = ok && st;
                error = st ? 'Response can not contains "' + ex_res.join('", "') + '"\n' : '';
            } else if (_.isObject(ex_res.fields)) {
                for (var field in ex_res.fields) {
                    var valid = true;
                    if (_.isFunction(ex_res.fields[field])) {
                        valid = ex_res.fields[field](data[field]);
                    } else if (_.isRegExp(ex_res.fields[field])) {
                        valid = ex_res.fields[field].test(data[field]);
                    } else {
                        valid = ex_res.fields[field] == data[field];
                    }

                    if (!valid) {
                        error += 'Field "' + field + '" does not match "' + ex_res[field] + '"\n';
                    }
                    ok = ok && valid;
                }
            }
        }


        if (_.isFunction(ex_res.handler) && ok) {
            ex_res.handler(data, tester);
        }

        tester.process_result(true, _.extend(_.clone(add), {
            res: data,
            ok: ok,
            error: ok ? '' : error
        }), callback);
    };


    function create_tester(tester, ext_name, test_name, test_number, test) {
        var add = {
            critical: test.critical || false,
            req: test.req || {},
            ext_name: ext_name,
            test_name: test_name,
            test_number: test_number
        };

        if (_.isFunction(test)) {
            return function(callback) {
                test(tester, function(result) {
                    tester.process_result(true,  _.extend(_.clone(add), result), callback);
                });
            }
        } else if (_.isObject(test)) {
            if (['http'].indexOf(test.type) == -1) {
                throw 'Test ' +  + ' can not contains type in ("http", "sock")';
            }

            if (test.type == 'http') {
                var req = _.clone(test.req);
                var res = _.clone(test.res || {});
                var action = req.action;
                delete req.action;


                return function(callback) {
                    _.each(req, function(v, k) {
                        if (_.isFunction(v)) { req[k] = v();}
                    });
                    add.req = req;
                    tester.response = null;


                    var func = _.isFunction(action) ? action(tester) : function(data) {
                        tester.protocol().request(action, data);
                    };
                    var ret = false;

                    func({
                        data: req,
                        success: function(data) {
                            ret = true;
                            tester.response = data;
                            http_response_handler(add, tester, res, data, callback);
                        },
                        error: function (e_code) {
                            ret = true;
                            http_response_handler(add, tester, res, {status: e_code}, callback);
                        }
                    });

                    setTimeout(function() {
                        if (!ret) {
                            tester.process_result(true, _.extend(_.clone(add), {
                                res: {},
                                ok: false,
                                error: '503 Service Unavailable'
                            }), callback);
                        }
                    }, tester.timeout);
                }
            }
        }
    }


    _.extend(Tester.prototype, {
        alloc: function(name) {
            if (this.data[name] == undefined) {
                this.data[name] = {};
            }
            return this.data[name];
        },


        set_data: function(store_name, item, value) {
            this.alloc(store_name);
            this.data[store_name][item] = value;
        },


        use: function() {
            return this.pr.apply(this, arguments);
        },

        protocol: function() {
            return this.pr;
        },

        run: function(exts, callback) {
            var self = this;
            var paths = _.map(exts || [], function(el) { return 'test/' + el});

            requirejs(paths, function (){
                for (var i = 0; i < arguments.length; ++i) {
                    var tests = arguments[i];
                    var testers = [];

                    for (var n = 1; n <= tests.length; ++n) {
                        var name = tests[n - 1][0], subtests = tests[n - 1][1];
                        if (!_.isArray(subtests)) {
                            subtests = [subtests];
                        }
                        for (var t = 0; t < subtests.length; ++t) {
                            testers.push(create_tester(self, exts[i], name || '', n, subtests[t]));
                        }

                        testers.push(function(callback) {
                            self.protocol().request('reset', {
                                success: function() {callback();}
                            });
                        });
                    }
                    chain(testers)();
                }
            });
        },

        print_result: function(result) {
            console.log(result);
        },

        process_result: function(show_res, result, callback) {
            /*
             result = {
             ext_name - имя тестируемого расширения
             test_name - название теста
             test_number - номер теста


             req - объект запроса
             res - объект ответа
             ok - статус теста
             error - строка с текстом ошибки
             critical - если true и статус теста false, тогда тестирование прекращается
             }
             */
            if (show_res) {
                this.print_result(result);
                console.log(result);
            }

            if (result.critical && !result.ok) {
                return;
            }
            callback();
        }
    });

    return Tester
});