define([
    'router',
    'bn',
    '$',
    'models/User',
    'protocol/protocol',
    'protocol/extensions/core',
    'protocol/extensions/debug'
], function(Router, bn, $, User, Pr, ExtCore, ExtDebug){
    var app = {
        user: null,
        page: null,
        router: null,
        io: null,
        protocol: null,

        SUCCESS: 'ok',

        initialize: function() {
            this.user = new User();
            this.router = new Router({app: this});
            this.consts = {};
        },

        start: function() {
            bn.history.start({pushState: true});
        },

        connect_to_server: function(url, callback) {
            var self = this;
            var pr = new Pr(url, [ExtCore, ExtDebug], function() {
                self.protocol = pr;
                callback();
            });
        },

        server_is_selected: function() {
            return this.protocol;
        },

        set_cookie: function(name, value, expires, path, domain, secure) {
            document.cookie = name + "=" + value +
                ((expires) ? "; expires=" + expires : "") +
                ((path) ? "; path=" + path : "") +
                ((domain) ? "; domain=" + domain : "") +
                ((secure) ? "; secure" : "");
        },

        get_cookie: function(key) {
            var res = document.cookie.match(new RegExp(key + '=([^;]*)'));
            return res ? res[1] : null;
        },

        show_message: function(type, text) {
            if (['error', 'success', 'info'].indexOf(type) == -1) {
                throw "type can only have values \"error\", \"success\", \"info\"";
            }
            var m_container = $('#information-manager');
            var message = $('<div class="alert alert-' + type + '"></div>');
            message.text(text);
            message.append('<button class="close" data-dismiss="alert">×</button>');
            m_container.append(message);
            message.delay(10 * 1000).fadeOut(2 * 1000, function() { $(this).remove(); });
        }
    };

    return app;
});
