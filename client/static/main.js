requirejs.config({
    baseUrl: '/static/',

    paths: {
        $: 'lib/jquery',
        ui: 'jqueryui/jquery-ui.min',
        _: 'lib/underscore',
        bn: 'lib/backbone',
        tw: 'bootstrap/js/bootstrap',
        just: 'lib/just',
        stats: 'lib/stats.min',
        gl_matrix: 'engine/glMatrix-0.9.5.min',
        engine: 'engine/engine',
        glview: 'engine/glview',
        map: 'engine/map',
        map2d: 'engine/2dmap',
        map3d: 'engine/3dmap',
        bomb_model: 'engine/bomb_model',
        sprite: 'engine/sprite',
        scene: 'engine/scene',
        factory: 'engine/factory',
        model: 'engine/model',
        utils: 'engine/utils',
        gameobject: 'engine/gameobject',
        vfx: 'engine/vfx',
        sound: 'engine/sound',
        resources: 'engine/resources',
        game_settings: 'lib/game_settings',
        minimap: 'lib/minimap'
    },

    shim: {
        bn: {
            deps: ['_', '$'],
            exports: 'Backbone'
        },

        _: {
            deps: ['just'],
            exports: function(just) {
                var JUST = new window.JUST({root: '/static/templates', useCache : false, ext: '.html'});
                _.template = JUST.render;
                return _;
            }
        },

        $: {
            exports: '$'
        },
        ui: {
            deps: ['$'],
            exports: '$'
        },

        tw: {
            deps: ['$']
        },

        stats: {
            exports: 'Stats'
        }
    }
});

requirejs(['app', 'tw'], function(app){
    window.App = app;
    App.initialize();
  ///  App.connect_to_server('http://127.0.0.1:8080/', function() {
        App.start();
  //  })

});