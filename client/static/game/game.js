var default_cell_size = 10.0;


var resource_list = {
    tx:
    {
        '2d': {
            player: {
                type: 'tileset',
                'file':'%textures%/2d/bomberman.png',
                'tile':[32, 32]
            },
            explosion: {
                type: 'tileset',
                'file':'%textures%/2d/explosion.png',
                'tile':[64, 64]
            },
            tiles: {
                type: 'tileset',
                'file':'%textures%/2d/map.png',
                'tile':[32, 32]
            },
            extras: {
                type: 'tileset',
                'file':'%textures%/2d/extras.png',
                'tile':[64, 64]
            },
            bomb: {
                type: 'tileset',
                'file':'%textures%/2d/bomb.png',
                'tile':[64, 64]
            },
            border: {
                type: 'texture',
                'file':'%textures%/2d/border.png'
            },
            additional: {
                type: 'tileset',
                'file':'%textures%/2d/additional-tiles.png',
                'tile':[64, 64]
            }
        },
        '3d': {
            tiles: {
                type: 'texture',
                'file':'%textures%/3d/map.png'
            },
            bomb: {
                type: 'texture',
                'file':'%textures%/3d/bomb.png'
            },
            tiles_specular: {
                type: 'texture',
                'file':'%textures%/3d/map-specular.png'
            },
            boom: {
                type: 'texture',
                'file':'%textures%/3d/boom.png'
            }
        }
    },
    sh: {
        '2d': {
            'fragment':{
                tile: {
                    type: 'fragment',
                    file: '%shaders%/2d/tile.fs'
                },
                player: {
                    type: 'fragment',
                    file: '%shaders%/2d/player.fs'
                },
                sprite: {
                    type: 'fragment',
                    file: '%shaders%/2d/sprite.fs'
                },
                floor: {
                    type: 'fragment',
                    file: '%shaders%/2d/floor.fs'
                }
            },
            'vertex':{
                tile: {
                    type: 'vertex',
                    file: '%shaders%/2d/tile.vs'
                },
                player: {
                    type: 'vertex',
                    file: '%shaders%/2d/player.vs'
                },
                sprite: {
                    type: 'vertex',
                    file: '%shaders%/2d/sprite.vs'
                },
                floor: {
                    type: 'vertex',
                    file: '%shaders%/2d/floor.vs'
                }
            },
            'tile': {
                type: 'shader',
                link: [':sh:2d:fragment:tile', ':sh:2d:vertex:tile']
            },
            'keyed': {
                type: 'shader',
                link: [':sh:2d:fragment:player', ':sh:2d:vertex:player']
            },
            'sprite': {
                type: 'shader',
                link: [':sh:2d:fragment:sprite', ':sh:2d:vertex:sprite']
            },
            'floor': {
                type: 'shader',
                link: [':sh:2d:fragment:floor', ':sh:2d:vertex:floor']
            }
        },
        '3d': {
            'fragment':{
                field: {
                    type: 'fragment',
                    file: '%shaders%/3d/field.fs'
                },
                ps: {
                    type: 'fragment',
                    file: '%shaders%/3d/particle_system.fs'
                },
                bomb: {
                    type: 'fragment',
                    file: '%shaders%/3d/bomb.fs'
                }
            },
            'vertex':{
                field: {
                    type: 'vertex',
                    file: '%shaders%/3d/field.vs'
                },
                ps: {
                    type: 'vertex',
                    file: '%shaders%/3d/particle_system.vs'
                },
                bomb: {
                    type: 'vertex',
                    file: '%shaders%/3d/bomb.vs'
                }
            },
            'field': {
                type: 'shader',
                link: [':sh:3d:fragment:field', ':sh:3d:vertex:field']
            },
            'ps': {
                type: 'shader',
                link: [':sh:3d:fragment:ps', ':sh:3d:vertex:ps']
            },
            'bomb': {
                type: 'shader',
                link: [':sh:3d:fragment:bomb', ':sh:3d:vertex:bomb']
            }
        }
    },
    'snd':{
        'explode': {
            type: 'sound',
            file: '%sounds%/bomb-small.mp3'
        },
        'death':  {
            type: 'sound',
            file: '%sounds%/death-scream.mp3'
        }
    }
};

var classes = {
    players: {
        '2d': {
            rect_vertex_buffer: {
                class: 'array_buffer',
                static: true,
                stride: 3,
                v: [0, 0, 0,
                    default_cell_size, 0, 0,
                    default_cell_size, default_cell_size, 0,
                    0, default_cell_size, 0]
            },
            rect_texture_buffer: {
                class: 'array_buffer',
                static: true,
                stride: 2,
                v: [0, 1, 1, 1, 1, 0, 0, 0]
            },
            rect_index_buffer: {
                class: 'index_buffer',
                static: true,
                i: [0, 1, 2, 0, 2, 3]
            },
            default_player: {
                class: 'sprite',
                texture: ':tx:2d:player',
                shader: ':sh:2d:sprite',

                vertex_buffer: '/players/rect_vertex_buffer',
                index_buffer: '/players/rect_index_buffer',
                texture_buffer: '/players/rect_texture_buffer',

                depth: 0.5,
                animations: {
                    death: {
                        fps: 8,
                        frames: [
                            [3, 3], [4, 3], [5, 3],
                            [3, 2], [4, 2], [5, 2],
                            [3, 1], [4, 1], [5, 1],
                            [3, 0], [4, 0]
                        ],
                        sound: ':snd:death',
                        next_animation: 'ghost'
                    },
                    ghost: {
                        fps: 2,
                        loop: true,
                        frames: [[6, 3], [7, 3]]
                    },
                    left: {
                        fps: 10,
                        loop: true,
                        frames: [[0, 2], [1, 2], [2, 2]]
                    },
                    right: {
                        fps: 10,
                        loop: true,
                        frames: [[0, 0], [1, 0], [2, 0]]
                    },
                    up: {
                        fps: 10,
                        loop: true,
                        frames: [[0, 1], [1, 1], [2, 1]]
                    },
                    down: {
                        fps: 10,
                        loop: true,
                        frames: [[0, 3], [1, 3], [2, 3]]
                    }
                }
            }
        },
        '3d': {
            rect_vertex_buffer: {
                class: 'array_buffer',
                static: true,
                stride: 3,
                v: [default_cell_size, 0, default_cell_size,
                    0, 0, default_cell_size,
                    0, -default_cell_size/2, 0,
                    default_cell_size, -default_cell_size/2, 0]
            },
            rect_flip_vertex_buffer: {
                class: 'array_buffer',
                static: true,
                stride: 3,
                v: [0, 0, 0,
                    default_cell_size, 0, 0,
                    default_cell_size, default_cell_size, 0,
                    0, default_cell_size, 0]
            },
            rect_texture_buffer: {
                class: 'array_buffer',
                static: true,
                stride: 2,
                v: [1, 1, 0, 1, 0, 0, 1, 0]
            },
            rect_index_buffer: {
                class: 'index_buffer',
                static: true,
                i: [0, 1, 2, 0, 2, 3]
            },
            default_player: {
                class: 'sprite',
                texture: ':tx:2d:player',
                shader: ':sh:2d:sprite',

                vertex_buffer: '/players/rect_vertex_buffer',
                index_buffer: '/players/rect_index_buffer',
                texture_buffer: '/players/rect_texture_buffer',

                depth: 0.5,
                cheat: true,
                animations: {
                    death: {
                        fps: 8,
                        frames: [
                            [3, 3], [4, 3], [5, 3],
                            [3, 2], [4, 2], [5, 2],
                            [3, 1], [4, 1], [5, 1],
                            [3, 0], [4, 0]
                        ],
                        sound: ':snd:death',
                        next_animation: 'ghost'
                    },
                    ghost: {
                        fps: 2,
                        loop: true,
                        frames: [[6, 3], [7, 3]]
                    },
                    left: {
                        fps: 10,
                        loop: true,
                        frames: [[0, 2], [1, 2], [2, 2]]
                    },
                    right: {
                        fps: 10,
                        loop: true,
                        frames: [[0, 0], [1, 0], [2, 0]]
                    },
                    up: {
                        fps: 10,
                        loop: true,
                        frames: [[0, 1], [1, 1], [2, 1]]
                    },
                    down: {
                        fps: 10,
                        loop: true,
                        frames: [[0, 3], [1, 3], [2, 3]]
                    }
                }
            }
        }
    },
    map: {
        '2d': {
            representation: {
                class: 'representation2d',
                block_size: 8,
                default_cell_size: 10,
                layers: ['/map/ground_layer', '/map/additional_layer', '/map/bonus_layer']
            },
            ground_layer: {
                class: 'map2d.ground_layer',
                depth: 0.0,
                texture: ':tx:2d:tiles',
                shader: ':sh:2d:floor',
                items: {
                    '.': 0,
                    'v': 0,
                    '^': 0,
                    '<': 0,
                    '>': 0,
                    '*': 0,
                    '#': 1,
                    '%': 2,
                    '$': 3,
                    '-': 4
                },
                types_count: 8,
                items_count: 4
            },
            additional_layer: {
                class: 'map2d.dynamic_layer',
                depth: 0.1,
                texture: ':tx:2d:additional',
                shader: ':sh:2d:floor',
                items: {
                    '^': 0,
                    '>': 1,
                    'v': 2,
                    '<': 3,
                    '*': 4
                },
                types_count: 1,
                items_count: 8
            },
            bonus_layer: {
                class: 'map2d.dynamic_layer',
                depth: 0.15,
                texture: ':tx:2d:extras',
                shader: ':sh:2d:floor',
                items: {
                    'b':0,
                    'f':1,
                    'g':2,
                    's':3,
                    'k':4,
                    't':5,
                    'i':6,
                    'c':7,
                    'a':8,
                    'n':9,
                    'u':10
                },
                types_count: 1,
                items_count: 16
            },

            bomb_list: {
                class: 'sprite_list',
                depth: 0.3,
                texture: ':tx:2d:bomb',
                shader: ':sh:2d:tile',
                vertex_buffer: '/players/rect_vertex_buffer',
                index_buffer: '/players/rect_index_buffer',
                texture_buffer: '/players/rect_texture_buffer',
                animations: {
                    default: {
                        fps: 12,
                        loop: true,
                        frames: [[0, 0], [1, 0], [2, 0], [3, 0]]
                    }
                }
            },

            player_cells_list: {
                class: 'cell_sprite_list',
                depth: 0.14,
                texture: ':tx:2d:border',
                shader: ':sh:2d:tile',
                vertex_buffer: '/players/rect_vertex_buffer',
                index_buffer: '/players/rect_index_buffer',
                texture_buffer: '/players/rect_texture_buffer',
                animations: {
                }
            },

            explosion_list: {
                class: 'explosion_list',
                depth: 0.4,
                texture: ':tx:2d:explosion',
                shader: ':sh:2d:tile',
                vertex_buffer: '/players/rect_vertex_buffer',
                index_buffer: '/players/rect_index_buffer',
                texture_buffer: '/players/rect_texture_buffer',
                explode_sound: ':snd:explode',
                animations: {
                    center: {
                        fps: 12,
                        loop: true,
                        frames: [[4, 0], [4, 1], [4, 2], [4, 1], [4, 0]]
                    },
                    up: {
                        fps: 12,
                        loop: true,
                        frames: [[0, 0], [0, 1], [0, 2], [0, 1], [0, 0]]
                    },
                    vertical: {
                        fps: 12,
                        loop: true,
                        frames: [[1, 0], [1, 1], [1, 2], [1, 1], [1, 0]]
                    },
                    horizontal: {
                        fps: 12,
                        loop: true,
                        frames: [[2, 0], [2, 1], [2, 2], [2, 1], [2, 0]]
                    },
                    down: {
                        fps: 12,
                        loop: true,
                        frames: [[3, 0], [3, 1], [3, 2], [3, 1], [3, 0]]
                    }/*,
                    vert: {
                        fps: 12,
                        loop: true,
                        frames: [[1, 0], [1, 1], [1, 2], [1, 3]]
                    },
                    vert: {
                        fps: 12,
                        loop: true,
                        frames: [[1, 0], [1, 1], [1, 2], [1, 3]]
                    }*/
                }
            }
        },
        '3d': {
            explosion: {
                class: 'particle_system',
                shader: ':sh:3d:ps',
                texture: ':tx:3d:boom',
                default_cell_size: 10,
                count: 5
            },
            bomb_list: {
                class: 'bomb_model',
                depth: 7.0,
                shader: ':sh:3d:bomb',
                texture: ':tx:3d:bomb',
                default_cell_size: 10
            },
            representation: {
                class: 'representation3d',
                block_size: 8,
                default_cell_size: 10,
                layers: ['/map/sub_ground_layer', '/map/additional_layer', '/map/bonus_layer', '/map/wall_layer', '/map/destroyable_layer']
            },
            sub_ground_layer: {
                class: 'map2d.ground_layer',
                depth: 0.0,
                texture: ':tx:2d:tiles',
                shader: ':sh:2d:floor',
                flip: true,
                items: {
                    '.': 0,
                    'v': 0,
                    '^': 0,
                    '<': 0,
                    '>': 0,
                    '*': 0,
                    '#': 0,
                    '%': 0,
                    '$': 0,
                    '-': 4
                },
                types_count: 8,
                items_count: 4
            },
            wall_layer: {
                class: 'map3d.ground_layer',
                corner_segments: 4,
                linked: true,
                corner_smooth: true,
                contour: [
                    {
                        points: [[0.5, 0, 0], [0.5, 0.2, 0.1], [0.45, 0.25, 0.2], [0.40, 0.25, 0.3], [0.40, 1.25, 0.7], [0.25, 1.5, 0.8], [0, 1.5, 1]],
                        smooth: true
                    },
                    {
                        points: [[0, 1.5, 1], [0, 0, 0]],
                        smooth: false
                    }
                ],
                texture_rect: [0.25, 0.0, 0.5, 0.125], //left bottom right top
                depth: 1.499,
                texture: ':tx:3d:tiles',
                texture_specular: ':tx:3d:tiles_specular',
                shader: ':sh:3d:field',
                items: [
                    '#'
                ],
                types_count: 8,
                items_count: 4
            },
            destroyable_layer: {
                class: 'map3d.ground_layer',
                corner_segments: 3,
                linked: true,
                corner_smooth: true,
                contour: [
                    {
                        points: [[0.5, 0, 0], [0.5, 0.2, 0.1], [0.45, 0.25, 0.2], [0.40, 0.25, 0.3], [0.40, 0.75, 0.7], [0.15, 1, 0.9], [0, 1, 1]],
                        smooth: true
                    },
                    {
                        points: [[0, 1, 1], [0, 0, 0]],
                        smooth: false
                    }
                ],
                texture_rect: [0.5, 0.0, 0.75, 0.125], //left bottom right top
                depth: 0.5,
                texture: ':tx:3d:tiles',
                texture_specular: ':tx:3d:tiles_specular',
                shader: ':sh:3d:field',
                items: [
                    '%',
                    '$'
                ],
                types_count: 8,
                items_count: 4
            },
            additional_layer: {
                class: 'map2d.dynamic_layer',
                depth: -0.1,
                texture: ':tx:2d:additional',
                shader: ':sh:2d:floor',
                flip: true,
                items: {
                    '^': 0,
                    '>': 1,
                    'v': 2,
                    '<': 3,
                    '*': 4
                },
                types_count: 1,
                items_count: 8
            },
            bonus_layer: {
                class: 'map2d.dynamic_layer',
                depth: -5,
                texture: ':tx:2d:extras',
                shader: ':sh:2d:floor',
                flip: true,
                items: {
                    'b':0,
                    'f':1,
                    'g':2,
                    's':3,
                    'k':4,
                    't':5,
                    'i':6,
                    'c':7,
                    'a':8,
                    'n':9,
                    'u':10
                },
                types_count: 1,
                items_count: 16
            },

            player_cells_list: {
                class: 'cell_sprite_list',
                depth: -0.14,
                texture: ':tx:2d:border',
                shader: ':sh:2d:tile',
                vertex_buffer: '/players/rect_flip_vertex_buffer',
                index_buffer: '/players/rect_index_buffer',
                texture_buffer: '/players/rect_texture_buffer',
                flip: true,
                animations: {
                }
            },

            explosion_list: {
                class: 'explosion_list',
                draw: null,
                depth: -5.4,
                texture: ':tx:2d:explosion',
                shader: ':sh:2d:tile',
                vertex_buffer: '/players/rect_flip_vertex_buffer',
                index_buffer: '/players/rect_index_buffer',
                texture_buffer: '/players/rect_texture_buffer',
                explode_sound: ':snd:explode',
                flip: true,
                animations: {
                    center: {
                        fps: 12,
                        loop: true,
                        frames: [[4, 0], [4, 1], [4, 2], [4, 1], [4, 0]]
                    },
                    up: {
                        fps: 12,
                        loop: true,
                        frames: [[0, 0], [0, 1], [0, 2], [0, 1], [0, 0]]
                    },
                    vertical: {
                        fps: 12,
                        loop: true,
                        frames: [[1, 0], [1, 1], [1, 2], [1, 1], [1, 0]]
                    },
                    horizontal: {
                        fps: 12,
                        loop: true,
                        frames: [[2, 0], [2, 1], [2, 2], [2, 1], [2, 0]]
                    },
                    down: {
                        fps: 12,
                        loop: true,
                        frames: [[3, 0], [3, 1], [3, 2], [3, 1], [3, 0]]
                    }/*,
                     vert: {
                     fps: 12,
                     loop: true,
                     frames: [[1, 0], [1, 1], [1, 2], [1, 3]]
                     },
                     vert: {
                     fps: 12,
                     loop: true,
                     frames: [[1, 0], [1, 1], [1, 2], [1, 3]]
                     }*/
                }
            }
        }
    }
};



define([
    '_',
    'sound',
    'engine',
    'map',
    'map2d',
    'sprite',
    'scene',
    'model',
    'stats'
], function(_, sound, engine, Map, map2d, sprite, scene, model) {
    var Game = function(canvas, game3d) {
        //this.map = data.map;
        this.engine = new engine.engine(canvas)
        this.keys = [];
        this.game3d = game3d;
        this.currentlyPressedKeys = {};
        for (var i = 0; i < 256; i++)
        {
            this.currentlyPressedKeys[i] = false;
        }
    };

    var VK_LEFT = 37, VK_UP = 38, VK_RIGHT = 39, VK_DOWN = 40, VK_SPACE = 32;
    var keysConsts = {
        37: 'left',
        38: 'up',
        39: 'right',
        40: 'down',
        65: 'left',
        87: 'up',
        68: 'right',
        83: 'down'
    };

    _.extend(Game.prototype, {
        prepare: function(data, protocol) {
            this.protocol = protocol;
            this.players = [];
            this.current_player_index = data.current_player_index;
        },
        init: function() {
            var self = this;
            if (this.on_progress)
                this.engine.on_progress = this.on_progress;
            if (this.on_finish)
                this.engine.on_finish = function(success) {
                    if (success) {
                        self.on_engine_init();
                    }
                    self.on_finish(success);
                };
            this.engine.init(this.game3d, resource_list, classes);
        },
        on_engine_init: function() {
            this.scene = new scene.scene(this.engine);
            this.engine.screen_dependent.push(this.scene);
            if (this.engine['3d'])
                this.camera = new scene.camera3d(this.scene);
            else
                this.camera = new scene.camera2d(this.scene);

            this.scene.add_camera(this.camera);
            this.scene.use_camera(0);

            var self = this;
            this.stats = new Stats();
            this.stats.setMode(0); // 0: fps, 1: ms

            this.stats.domElement.style.position = 'absolute';
            this.stats.domElement.style.left = '0px';
            this.stats.domElement.style.top = '50px';

            // Align top-left
            document.body.appendChild( this.stats.domElement );

            this.engine.update_handler = function() {
                self.update();
            };
        },
        start: function(data) {
            var player_sprite;
            this.players = [];

            this.map = new Map.map(data.field, 1);

            this.rep = this.engine.factory.create('/map/representation', undefined, this.map);
            this.map.setRepresentation(this.rep);
            this.scene.new_object(this.rep);

            this.player_cells = this.engine.factory.create('/map/player_cells_list', undefined);
            this.scene.new_object(this.player_cells);
            this.player_cells.bindToObject(data.players);

            this.bombs = this.engine.factory.create('/map/bomb_list', undefined);
            this.scene.new_object(this.bombs);
            this.bombs.bindToObject(data.bombs);

            this.explosions = this.engine.factory.create('/map/explosion_list', undefined);
            this.scene.new_object(this.explosions);
            this.explosions.bindToObject(data.explosions);

            this.explosion_set = [];
            for (var i = 0; i < this.map.height; ++i) {
                var line = [];
                for (var j = 0; j < this.map.width; ++j) {
                    line.push(0);
                }
                this.explosion_set.push(line);
            }

            for (var i = 0; i < data.players.length; i++) {
                var pl = new Map.player(this.map, [data.players[i].x, data.players[i].y], i);
                var sp = this.engine.factory.create('/players/default_player', 'player' + i);//new sprite.sprite(this.engine.scene.res.g('player'), this.engine.scene.res.g('sprite'), [0, 0], 0.5*15);
                sp.bindToObject(pl);
                sp.color = playerColors[i];

                if (i == this.current_player_index)
                    player_sprite = sp;
                else
                    this.scene.new_object(sp);

                this.scene.new_object(pl);
                this.players.push(pl);
            }
            this.scene.new_object(player_sprite);
            this.player = this.players[this.current_player_index];
            this.started = true;

            return;
        },
        applyExplosions: function(list) {
            for (var i = 0; i < list.length; i++) {
                var vy = Math.round(list[i].y/10000), vx = Math.round(list[i].x/10000);
                if (this.explosion_set[vy][vx] != list[i].created_at) {
                    if (this.game3d) {
                        var explosion = this.engine.factory.create('/map/explosion', undefined,
                            list[i]);
                        this.scene.new_object(explosion);
                    }
                    this.explosion_set[vy][vx] = list[i].created_at;
                    if (sound.inited)
                        this.explosions.explode_sound.play([vx*default_cell_size, vy*default_cell_size, 3]);
                }
            }
        },
        destroy: function() {
            this.engine.free();
            this.engine = undefined;
        },
        keydown: function(key, event) {
            if ([VK_LEFT, VK_UP, VK_RIGHT, VK_DOWN, VK_SPACE].indexOf(key) == -1) {
                return;
            }
            event.preventDefault();
            var send_action = false;
            var direction = 'none';
            if (this.keys.length == 2)
                console.log("!!!");
            if (!this.currentlyPressedKeys[key]) {
                send_action = true;
                if (key != VK_SPACE) {
                    this.keys.push(key);
//                    direction = keysConsts[key];
                }
            }
            if (this.keys.length)
                direction = keysConsts[this.keys[this.keys.length - 1]];
            this.currentlyPressedKeys[key] = true;
            var bomb_set = this.currentlyPressedKeys[VK_SPACE]?'place_bomb':'none';

            if (send_action) {
                console.log("game_action", direction, bomb_set);
                this.protocol.use().game_action({data: {direction: direction, action:bomb_set}});
            }
        },
        keyup: function(key, event) {
            console.log(key);
            if (key == 70) {
                this.engine.enter_full_screen();
            }
            this.currentlyPressedKeys[key] = false;
            if ([VK_LEFT, VK_UP, VK_RIGHT, VK_DOWN, VK_SPACE].indexOf(key) == -1) {
                return;
            }
            event.preventDefault();
            var deleted = 0;
            var last_key = this.keys[this.keys.length - 1];
            for (var i = 0; i < this.keys.length; i++)
                if (!this.currentlyPressedKeys[this.keys[i]])
                    this.keys.splice(i - deleted++, 1);

            var bomb_set = this.currentlyPressedKeys[VK_SPACE]?'place_bomb':'none';
            var send_action = false;
            var direction = 'none';
            if (this.keys.length)
                direction = keysConsts[this.keys[this.keys.length - 1]];
            if (key == VK_SPACE) {
                send_action = true;
            }
            else {
                if ((deleted && !this.keys.length) || (last_key != this.keys[this.keys.length - 1]))
                    send_action = true;
            }

            if (send_action) {
                console.log("game_action", direction, bomb_set);
                this.protocol.use().game_action({data: {
                    direction: direction,
                    action:bomb_set
                }});
            }
        },
        update: function() {
            if (this.engine.paused || !this.player)
                return;

            this.stats.begin();

            this.camera.max = [
                this.map.width*this.rep.default_cell_size - this.engine.gl.viewportWidth/(this.camera.zoom),
                this.map.height*this.rep.default_cell_size - this.engine.gl.viewportHeight/(this.camera.zoom),
                this.camera.max[2]];


            this.camera.pos = [this.player.pos[0] + this.rep.default_cell_size*0.5,
                this.player.pos[1] + this.rep.default_cell_size*0.5, 0];

            sound.setListenerPosition([this.camera.pos[0], this.camera.pos[1], 0]);//camera's properties

            if (this.scene)
                this.scene.step();

            this.stats.end();
        },
        update_data: function(data) {
            this.map.applyContents(data.field);
            for (var i = 0; i < data.players.length; ++i) {
                var pl = this.players[i], rpl = data.players[i];
                if (this.players.length <= i) {
                    break;
                }
                pl.setPos(rpl.x, rpl.y);
                pl.setDead(rpl.dead);
                pl.setSpeed(rpl.speed);
                pl.setLookDirection(rpl.look_direction);
                pl.setDirection(rpl.movement_direction);
            }

            this.player_cells.bindToObject(data.players);
            this.bombs.bindToObject(data.bombs);
            this.explosions.bindToObject(data.explosions);
            this.applyExplosions(data.explosions);
        }

    });


    return Game;
});
