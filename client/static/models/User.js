define(['bn'], function(bn) {
    var Model = bn.Model.extend({
        constructor: function(data) {
            this.constructor.__super__.constructor(data);
        },

        login: function(callback) {
            var self = this;
            App.protocol.use().login({
                data: {
                    username: this.get('username'),
                    password: this.get('password')
                },
                success: function (data) {
                    if (data.sid) {
                        self.set('sid', data.sid);
                        App.protocol.use().leave_game({
                            success: function() { callback(data); },
                            error: function() { callback(data); }
                        });
                    }
                },
                error: function(code) {
                    if (code == 'invalid_credentials') {
                        App.show_message('error', 'Bad login or password.');
                    }
                }
            });
        },

        logout: function(callback) {
            var self = this;
            App.protocol.use().logout({
                success: function(data) {
                    self.set('sid', null);
                    callback(data);
                },
                error: function(code) {
                    App.show_message('error', 'Can not logout, please try later.');
                }
            });
        },

        is_auth: function() {
            return this.get('sid');
        }
    });

    return Model;
});
