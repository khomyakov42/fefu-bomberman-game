
define([
    'bn'
], function(bn){
    var Router = bn.Router.extend({
        routes: {
            '': 'index',
            'lobby/': 'lobby',
            'uploadmap/': 'upload_map',
            'room/:id/': 'room',
            'game/': 'game',
            'sandbox/': 'sandbox',
            'debug/': 'debug',
            '.*': 'error_404'
        },

        index: function() {
            if (App.user.is_auth()) {
                this.navigate('lobby/', {trigger: true});
                return;
            }

            if (App.server_is_selected()) {
                console.log('index page server');
                this.create_page('views/Page/Index');
            } else {
                console.log('select server page');
                this.create_page('views/Page/ServerLobby');
            }
        },

        lobby: function() {
            if (this.check_auth()) {
                var self = this;

                if (App.user.get('room_id') != undefined) {
                    App.protocol.use().leave_game({
                        success: function(data) {
                            App.user.set('room_id', undefined);
                            App.router.lobby();
                        }
                    });
                    return;
                }

                App.protocol.use().game_list({
                    success: function(data) {
                        var games = data['games'];

                        App.protocol.use().list_maps({
                            async: false,
                            success: function(data) {
                                var maps = data['maps'];
                                console.log('lobby page');
                                self.create_page('views/Page/Lobby', {
                                    maps: maps,
                                    games: games
                                });
                            }
                        });
                    }
                });
            }
        },

        upload_map: function() {
            if (this.check_auth()) {
                var self = this;

                App.protocol.use().game_list({
                    success: function(data) {
                        console.log('upload map page');
                        self.create_page('views/Page/UploadMap', data);
                    }
                });
            }
        },

        room: function(id) {
            if (this.check_auth() && App.user.get('room_id') == id) {
                console.log('room page');
                this.create_page('views/Page/Room');
            }
        },


        game: function(id) {
            if (this.check_auth() && App.user.get('room_id')) {
                console.log('game page');
                this.create_page('views/Page/Game');
            }
        },

        debug: function() {
            var self = this;
            var login = 'test';
            var password = 'test';

            App.initialize();
            App.connect_to_server('http://127.0.0.1:8080/', function() {
            App.protocol.use().register({
                data: {
                    username: login,
                    password: password
                },
                complete: function() {
            App.user.set({
                username: login,
                password: password
            });

            App.user.login(function(data) { setTimeout(function() {
            if (data.status != App.protocol.use().status['SUCCESS']) {
                alert('can not logged in');
                return;
            }
            App.protocol.use().leave_game({
                complete: function(){

                    App.protocol.use().create_game({
                        data:{
                            name: 'test_room',
                            password: '',
                            map_id: 1,
                            settings: App.protocol.use().default_game_settings, // #TODO GAME SETTINGS,
                            max_player_count: 2
                        },
                        success: function(data) {
                            App.user.set('room_id', data.game_id);
                            var id = data.game_id;
                            self.create_page('views/Page/Debug');
                            App.router.navigate('/debug/');
                        }
                    });
                }
            });
            }, 500);});}});});
        },

        error_404: function() {
            this.create_page('views/Page/Error404');
        },

        check_auth: function() {
            if (!App.user.is_auth()) {
                this.navigate('/', {trigger: true});
                return false;
            }

            return true;
        },

        create_page: function(path, data) {
            var self = this;
            data = data || {};

            requirejs([path], function(Page) {
                var p = new Page(data);
                p.render(function(error, html) {
                    if (error) {
                        console.error(error);
                    }

                    if (App.page) {
                        if (App.page.undelegateEvents)
                            App.page.undelegateEvents();
                        if (App.page.free)
                            App.page.free();}
                    App.page = p;
                    p.undelegateEvents();
                    App.page.delegateEvents();
                    App.page.ready();
                })
            });
        }
    });

	return Router;
});

