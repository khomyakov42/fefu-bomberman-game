#define exports(x)


precision mediump float;

uniform vec4 uObjectColor exports(object_color);
uniform vec2 uPOffset exports(tile_offset);
uniform mat2 uPTileScale exports(tile_scale);

varying vec2 vTextureCoord;

uniform sampler2D uSampler exports(sampler0);

void main(void) {
    vec2 textureCoord = (uPOffset + vTextureCoord - floor(vTextureCoord))*uPTileScale;
    gl_FragColor = uObjectColor*texture2D(uSampler, vec2(textureCoord.s, textureCoord.t));
}
