#define exports(x)


precision mediump float;

varying vec2 vTextureCoord;

uniform sampler2D uSampler exports(sampler0);

void main(void) {
    if (vTextureCoord.s < 0.0) {
        discard;
        //return;
    }
    gl_FragColor = texture2D(uSampler, vec2(vTextureCoord.s, vTextureCoord.t));
}
