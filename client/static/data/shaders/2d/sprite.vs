#define exports(x)


attribute vec3 aVertexPosition exports(vertex);
attribute vec2 aTextureCoord exports(texture);

uniform mat4 uPMatrix exports(projection_matrix);
uniform mat4 uPMatrixTransform exports(view_matrix);

varying vec2 vTextureCoord;

uniform vec2 uPOffset exports(tile_offset);
uniform mat2 uPTileScale exports(tile_scale);

void main(void) {
    gl_Position = uPMatrix * uPMatrixTransform * vec4(aVertexPosition, 1.0);
    vTextureCoord = (uPOffset + aTextureCoord)*uPTileScale;//aTextureCoord;/* - floor(vTextureCoord)*/
}
