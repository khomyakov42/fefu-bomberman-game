precision mediump float;

varying float vLifeTime;
varying vec2 vTextureCoords;

uniform vec4 uObjectColor;
varying vec2 vTextureCoord;
uniform vec2 uPOffset;
uniform mat2 uPTileScale;
//uniform mat2 uPCellScale;


uniform sampler2D uParicleSampler;
uniform sampler2D uColorSampler;

void main(void) {
    gl_FragColor = texture2D(uColorSampler, vec2(vLifeTime, 0.0))*texture2D(uParticleSampler, vec2(vTextureCoord.s, vTextureCoord.t));
}
