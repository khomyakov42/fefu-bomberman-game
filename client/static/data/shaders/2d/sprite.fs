#define exports(x)


precision mediump float;

uniform vec4 uObjectColor exports(object_color);

varying vec2 vTextureCoord;

uniform sampler2D uSampler exports(sampler0);

void main(void) {
    vec4 color = texture2D(uSampler, vec2(vTextureCoord.s, vTextureCoord.t));
    if (color.g + color.b <= 0.0001 && color.r >=0.001)
        color = color.r*uObjectColor;
    gl_FragColor = color;
}
