attribute float aLifetime;
attribute vec3 aStartPosition;
attribute vec3 aAcceleration;
attribute vec3 aStartSpeed;

attribute vec2 aTextureCoords;




attribute vec3 aVertexPosition;
attribute vec2 aTextureCoord;

uniform mat4 uPMatrix;
uniform mat4 uPMatrixTransform;
uniform mat2 uPCellScale;

varying float vLifetime;
varying vec2 vTextureCoord;
//varying vec4 vColor;

void main(void) {
    //gl_PointSize = 4.0;
    gl_Position = uPMatrix * uPMatrixTransform * vec4(aVertexPosition, 1.0);
    vTextureCoord = (aTextureCoord)*uPCellScale;
}
