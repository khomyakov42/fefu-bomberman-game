#define exports(x)


attribute vec3 a_position exports(vertex);
attribute vec3 a_normal exports(normal);
attribute vec2 a_texture exports(texture);

uniform mat4 u_matrix_projection exports(projection_matrix);
uniform mat4 u_matrix_transform exports(view_matrix);
uniform mat3 u_matrix_normal exports(normal_matrix);

uniform float life exports(life);


varying vec3 v_normal;
varying vec4 v_position;
varying vec2 v_texture;

void main(void) {

    v_normal = u_matrix_normal * a_normal;
    float scale = sin(life*10.0)*0.1 + 1.0;
    v_position = u_matrix_transform * vec4(scale*a_position, 1.0);
    v_texture = a_texture;

    gl_Position = u_matrix_projection * v_position;
}
