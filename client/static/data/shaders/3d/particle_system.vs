#define exports(x)

attribute vec2 a_life exports(life);
attribute vec3 a_vertex exports(vertex);
attribute vec3 a_speed exports(speed);
attribute vec3 a_accel exports(accel);

uniform mat4 u_matrix_projection exports(projection_matrix);
uniform mat4 u_matrix_transform exports(view_matrix);
uniform mat3 u_matrix_normal exports(normal_matrix);

uniform float u_time exports(time);

varying float l;

void main(void) {
    l = (u_time - a_life.t)/a_life.s;
    vec3 vertex = a_vertex + l*a_speed + a_accel*l*l*0.5;
    gl_Position = u_matrix_projection * u_matrix_transform * vec4(vertex, 1.0);
    gl_PointSize = (4.0*sqrt(gl_Position.z));
}
