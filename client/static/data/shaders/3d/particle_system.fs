#define exports(x)

precision mediump float;

varying float l;

uniform sampler2D u_sampler0 exports(sampler0);

uniform float active exports(active);

uniform float w exports(w);
uniform float h exports(h);


void main(void) {
    if (l < 0.0 || l > 1.0) {
        discard;
        return;
    }
    float w1 = 1.0/w;
    float h1 = 1.0/h;
    float start_coord_s = floor(h*l);
    float start_coord_t = floor(w*h*l - start_coord_s*w);
    vec2 tex_coord = vec2(w1*(start_coord_t + gl_PointCoord.s), 1.0 - h1*(start_coord_s + gl_PointCoord.t));
    vec4 fragment_color = texture2D(u_sampler0, tex_coord);

    gl_FragColor = vec4(active, active*l, 0, fragment_color.g);//vec4(tex_coord, 1, 1);//vec4(1.0 - l, 0, 0, 1.0 - l);
}
