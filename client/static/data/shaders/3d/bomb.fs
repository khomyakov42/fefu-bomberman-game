#define exports(x)


precision highp float;

varying vec3 v_normal;
varying vec4 v_position;
varying vec2 v_texture;


const vec3 uPointLightingLocation = vec3(10000.0, -10000.0, -10000.0);

const vec3 ambient_color = vec3(0.2, 0.2, 0.2);
const vec3 diffuse_color = vec3(1.0, 1.0, 1.0);
//const vec3 specular_color = vec3(1.0, 1.0, 1.0);

const float shininess = 10.0;

uniform sampler2D u_sampler0 exports(sampler0);

uniform float life exports(life);

//uniform sampler2D u_sampler1 exports(sampler1);


void main(void) {
    vec3 normal = normalize(v_normal);

    vec4 fragment_color = texture2D(u_sampler0, v_texture);//vec4(1.0, 1.0, 1.0, 1.0);//
    //vec3 specular_color = texture2D(u_sampler1, v_texture).xyz;//vec4(1.0, 1.0, 1.0, 1.0);//



    vec3 light_direction = normalize(vec3(1, 1, 1));//(uPointLightingLocation - v_position.xyz));

    vec3 eye_dir = normalize(-v_position.xyz);
    vec3 reflection_dir = reflect(-light_direction, normal);

    float specular = pow(max(dot(reflection_dir, eye_dir), 0.0), shininess);
    float diffuse = max(dot(normal, light_direction), 0.0);

    vec3 light = ambient_color + diffuse_color*diffuse;
    gl_FragColor = vec4(fragment_color.rgb*light + specular, fragment_color.a);
    gl_FragColor.r += life + sin(life*life*100.0)*0.2;


    //vec4 normal =vec4(normalize(normal + vec3(1, 1, 1)), 1.0);//
    //gl_FragColor = normal;//texture2D(uSampler, vec2(vTexture.s, vTexture.t));//uObjectColor;//*texture2D(uSampler, vec2(textureCoord.s, textureCoord.t));
}
