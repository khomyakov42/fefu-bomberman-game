define([
    'ui',
    '_',
    'bn'
], function($, _, bn) {
    var gameSettings = {
        base_params: {
            max_slide_length:{
                min: 0,
                max: 10000, //#TODO MAX_CELL_SIZE
            },
            player_base_speed:{
                min: 1,
                max: 10000, //#TODO MAX_CELL_SIZE
            },
            bomb_speed:{
                min: 0,
                max: 10000, //#TODO MAX_CELL_SIZE
            },
            bomb_delay:{
                min: 0,
                max: 1000, //#TODO ?!!?!?
            },
            bomb_chain_reaction_delay:{
                min: 0,
                max: 1000, //#TODO MAX_CELL_SIZE
            },
            blast_wave_duration:{
                min: 0,
                max: 1000, //#TODO MAX_CELL_SIZE
            },
            cell_decay_duration:{
                min: 0,
                max: 1000, //#TODO MAX_CELL_SIZE
            },
            item_drop_probability:{
                min: 0,
                max: 100, //#TODO MAX_CELL_SIZE
            }
        },
        properties: {
            'weight': {
                min: 0,
                max: 10000
            },
            'start_count': {
                min: 0,
                max: 1000
            },
            'max_count': {
                min: 0,
                max: 1000
            },
            'flame_bonus': {
                min: 0,
                max: 1000
            },
            'bomb_bonus': {
                min: 0,
                max: 1000
            },
            'speed_bonus': {
                min: 0,
                max: 1000
            },
            'duration': {
                min: 0,
                max: 1000
            },
            'speed': {
                min: 0,
                max: 10000
            }
        },


        settings:{},


        default_params:['weight', 'start_count', 'max_count'],


        items: {
            'bomb': [
                'bomb_bonus'
            ],
            'flame': [
                'flame_bonus'
            ],
            'goldenflame': [
                'flame_bonus'
            ],
            'speed': [
                'speed_bonus'
            ],
            'kicker': [],
            'throw': [],
            'invert': [
                'duration'
            ],
            'lightspeed': [
                'speed',
                'duration'
            ],
            'autobomb': [
                'duration'
            ],
            'nobomb': [
                'duration'
            ],
            'turtle': [
                'speed',
                'duration'
            ]
        },


        getCaption: function(id) {
            var res = id.split("_").join(" ");
            return res.charAt(0).toUpperCase() + res.slice(1);
        },


        initSlider: function(slider_id, min_value, max_value, default_value) {
            var range = false;
            var self = this;
            if (max_value)
                range = "max";
            $( "#"+slider_id ).slider({
                range: range,
                value: default_value,
                min: min_value,
                max: max_value,
                slide: function( event, ui ) {
                    $( "#" + slider_id + "_label" ).text( ui.value );
                    self.settings[slider_id] = ui.value;
                }
            });
            this[slider_id] = default_value;
            $( "#" + slider_id + "_label" ).text( $( "#"+slider_id ).slider( "value" ) );
        },


        setSliderValue: function(name, value) {
            $('#' + name).slider("value", value);
            $( "#" + name + "_label" ).text( $( "#"+name ).slider( "value" ) );
            this.settings[name] = value;
        },


        resetBonus: function(c) {
            var items = this.default_params.concat(this.items[c]);
            for (var i = 0; i < items.length; i++) {
                var k = items[i];
                if (!k in this.game_settings.items[c])
                    return;
                this.setSliderValue(c + '_' + k, this.game_settings.items[c][k]);
            }
        },


        resetValues: function() {
            for (var k in this.base_params) {
                this.setSliderValue(k, this.game_settings[k]);
            }
        },


        initForm: function(game_settings, el) {
            if (!game_settings || !game_settings.items)
                return false;
            var self = this;
            this.game_settings = game_settings;
            _.template('page/settings', {self: this}, function(error, html) {
                el.html(html);
                //self.ready();
                for (var k in self.base_params) {
                    if (!k in game_settings)
                        return;
                    var val = self.base_params[k];
                    self.initSlider(k, val.min, val.max, game_settings[k]);
                }
                $('#reset_values').click(function (e) {
                    self.resetValues()
                });
                $('#items a').click(function (e) {
                    e.preventDefault();
                    $(this).tab('show');
                    var x = 0;
                    /*var img = { x1: 0, y1: 0, url: "../static/img/extras.png" };
                     $(this).css({
                     width: 80,
                     height: 80,
                     background: "url('" + img.url + "')",
                     backgroundSize: "800%",
                     backgroundPosition: -img.x1 + "px 0px"
                     //attr("style", "position:absolute; clip: rect(" + x + "px, " + 0 + "px, " + 40 + "px, " + (x + 40) + "px)");
                     });*/
                });
                for (var c in self.items) {
                    if (!c in game_settings.items)
                        return;
                    $('#' + c + '_reset').click(function (e) {
                        self.resetBonus(e.currentTarget.id.slice(0, e.currentTarget.id.length - '_reset'.length));
                    });
                    var items = self.default_params.concat(self.items[c]);
                    for (var i = 0; i < items.length; i++) {
                        var key = items[i];
                        if (!key in game_settings.items[c])
                            return;
                        self.initSlider(c + '_' + key, self.properties[key].min, self.properties[key].max,
                            game_settings.items[c][key]);
                    }
                }
            });
            return true;
        },


        getSettings: function(){
            var settings = {};

            for (var c in this.base_params) {
                settings[c] = +$("#" + c).slider('option', 'value');
            }
            settings.items = {};
            for (var c in this.items) {
                settings.items[c] = {};
                var items =this.default_params.concat(this.items[c]);
                for (var i = 0; i < items.length; i++) {
                    settings.items[c][items[i]] = +$("#" + c + "_" + items[i]).slider('option', 'value');
                }
            }
            console.log(settings);
            return settings;
        }
    };
    return gameSettings;
});