define(['$', 'map'], function($) {
        return {
            get_cell_color: function(t, b) {
                if (t == "#")
                    return [64, 64, 64];
                if (['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'].indexOf(t) != -1) {
                    var color = playerColors[+t];
                    return [color[0]*255, color[1]*255, color[2]*255];
                }
                if (['%', 'B', 'F', 'G', 'S', 'K', 'T', 'I', 'C', 'A', 'N', 'U'].indexOf(t) != -1)
                    return [128, 128, 128];
                if (b == "*")
                    return [0, 0, 0];
                return [255, 255, 255];
            },
            build: function(canvas, contents, width, height, zoom) {
                canvas.attr({width:width*zoom, height:height*zoom, scale:4/*#TODO zoom?!?!?!?!*/});
                var c = canvas[0].getContext("2d");
                var imgData = c.createImageData(width*zoom, height*zoom);
                for (var y = 0; y < height; y++) {
                    for (var x = 0; x < width; x++) {
                        var upperCell = contents[y][x*2];
                        var lowerCell = contents[y][x*2 + 1];
                        var color = this.get_cell_color(upperCell, lowerCell);
                        for (var z = 0; z < zoom*zoom; z++) {
                            imgData.data[((y*zoom + Math.floor(z/zoom))*width*zoom + x*zoom + z%zoom)*4 + 0] = color[0]
                            imgData.data[((y*zoom + Math.floor(z/zoom))*width*zoom + x*zoom + z%zoom)*4 + 1] = color[1]
                            imgData.data[((y*zoom + Math.floor(z/zoom))*width*zoom + x*zoom + z%zoom)*4 + 2] = color[2]
                            imgData.data[((y*zoom + Math.floor(z/zoom))*width*zoom + x*zoom + z%zoom)*4 + 3] = 255;//blend
                        }
                    }
                }
                c.putImageData(imgData, 0, 0);
            }
        };
    }
);
