define(['views/Page/Base'], function(Base) {
    var Page = Base.extend({
        template: 'page/index'
    });

    return Page;
});
