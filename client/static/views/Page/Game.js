define([
    'views/Page/BaseGame',
    '_',
    'engine'
], function(Base, _, engine) {
    var Page = Base.extend({
        events: {
            'keydown': 'on_keydown',
            'keyup': 'on_keyup'
        },

        template: 'page/game',

        delegateEvents: function() {
            Base.prototype.delegateEvents.apply(this, arguments);
            this.dataStream = function(data){
                if (!App.game.success)//#TODO make this with queue
                    return;
                var s = '';
                for (var i = 0; i < data.field.length; ++i) { /// это карта
                    s += data.field[i] + '\n';
                }
                App.game.gameMap.applyMap(data.field);
                App.game.engine.scene.tick = data.tick;
                for (var i = 0; i < data.players.length; ++i) {
                    App.game.players[i].setPosition(
                        data.players[i].x, data.players[i].y, data.players[i].speed, data.players[i].movement_direction,
                        data.players[i].look_direction, data.players[i].dead);
                }
                for (var i = 0; i < data.explosions.length; ++i) {
                    App.game.gameMap.setExplosion(data.explosions[i]);
                }
                App.game.gameMap.useBombs(data.bombs);
            };
            App.protocol.use().on_game_state(this.dataStream);
        },

        undelegateEvents: function() {
            Base.prototype.undelegateEvents.apply(this, arguments);
            App.protocol.use().off_game_state(this.dataStream);
        },

        constructor: function() {
            _.extend(this.events, this.constructor.__super__.events);
            Base.prototype.constructor.apply(this, arguments);


        },

        loaded: function(success) {
            if (!success)
                return;
            App.game.success = success;
            App.game.engine.scene.new_object(App.game.gameMap);

            //for (var i = 0; i < App.game.players.length; i++)
            //    App.game.engine.scene.new_object(App.game.players[i]);
        },

        ready: function() {
            App.game.engine = engine;

            App.game.engine.updateHandler = function(){
                App.game.update();
            };
            App.game.engine.init(this.loaded);
        },

        on_keydown: function(e) {
            var code = e.keyCode ? e.keyCode : e.which;
            //engine.handleKeyDown(e);
            App.game.keydown(code, e);
        },

        on_keyup: function(e) {
            var code = e.keyCode ? e.keyCode : e.which;
            //engine.handleKeyUp(e);
            App.game.keyup(code, e);
        }
    });
    return Page;
});
