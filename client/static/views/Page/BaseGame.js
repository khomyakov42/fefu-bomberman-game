define([
    'views/Page/Base',
    '_'
], function(BasePage, _) {

    var Page = BasePage.extend({
        template: 'page/room',

        events: {
            'click #logout': 'leave_game'
        },

        constructor: function() {
            _.extend(this.events, this.constructor.__super__.events);
            BasePage.prototype.constructor.apply(this, arguments);
        },

        leave_game: function () {
            App.protocol.use().leave_game({
                success: function(data) {
                    App.user.set('room_id', undefined);
                    App.router.navigate('lobby/', {trigger: true})
                }
            })
        }
    });

    return Page;
});