define(['View', '_'], function(View, _) {
    var Page = View.extend({
        template: 'page/base',
        el: 'body',

        LOGIN_EXISTS: 'login_exists',
        NOT_AUTHORIZE: 'bad_credentials',

        events: {
            'change #login': 'check_registration_form',
            'change #password': 'check_registration_form',
            'keyup #confirm_password': 'check_registration_form',

            'click #sign-in': 'authorize',
            'click #register': 'register',
            'click #logout': 'logout'
        },

        _process_logout: false,

        delegateEvents: function() {
            View.prototype.delegateEvents.apply(this, arguments);
            App.protocol.use().on_socket_event('closed', this.socket_closed);
            App.protocol.use().on_socket_event('error', this.socket_error);
        },

        undelegateEvents: function() {
            View.prototype.undelegateEvents.apply(this, arguments);
            App.protocol.use().off_socket_event('closed', this.socket_closed);
            App.protocol.use().off_socket_event('error', this.socket_error);
        },

        socket_error: function(data) {
            App.page.socket_closed(data);
            console.error(data);
        },

        socket_closed: function(data) {
            if (!App.page._process_logout) {
                App.show_message('error', 'Connection interrupted.');
            }
        },

        logout: function(event) {
            this._process_logout = true;
            var self = this;
            App.user.logout(function(data) {
                if (data.status == App.SUCCESS) {
                    App.router.navigate('/', {trigger: true});
                }
                self._process_logout = false;
            })
        },

        authorize: function(event) {
            App.user.set({
                username: this.$('#authorize>input[type="text"]').val(),
                password: this.$('#authorize>input[type="password"]').val()
            });

            App.user.login(function(data) {
                if (data.status != App.SUCCESS) {
                    App.show_message('error', 'Bad login or password.');
                    return;
                }
                console.log('user login');
                App.router.navigate('lobby/', {trigger: true});
            });
            return false;
        },

        register: function(event) {
            if (!this.check_registration_form()) {
                return false;
            }

            var self = this;

            App.protocol.use().register({
                data: {
                    username: self.$('#login').val(),
                    password: self.$('#password').val()
                },
                success: function(data) {
                    self.$('#registration').parent().children('.dropdown-toggle').click();
                    App.show_message('success', 'You have successfully registered.');
                },
                error: function(status) {
                    if (status == App.protocol.use().status['LOGIN_EXISTS']) {
                        self.$('#login').parent().addClass('error');
                        self.$('#login').parent().children('.help-block').text("login busy");
                        return;
                    }
                    console.error('not processed error');
                }
            });
            return false;
        },

        check_registration_form: function(event) {
            var inputs = event ? [$(event.target)] : [$('#login'), $('#password'), $('#confirm_password')];
            var bad = false;
            for (var i = 0; i < inputs.length; ++i) {
                var error = "";
                var val = inputs[i].val();

                switch(inputs[i].attr('id')) {
                    case 'login':
                        error = /^[0-9A-Za-z_]{3,15}$/.test(val) ? "" : "only letters numbers and \"_\" long 3 to 15";
                        break;
                    case 'password':
                        error = /^.{1,256}$/g.test(val) ? "" : "long 1 to 256";
                        break;
                    case 'confirm_password':
                        var pass = $('#password').val();
                        error = val == pass? "" : "passwords do not match";
                        break;
                }

                inputs[i].parent()[error ? 'addClass' : 'removeClass']('error').children('.help-block').text(error);
                bad = error || bad;
            }
            return !bad;
        }
    });

    return Page;
});
