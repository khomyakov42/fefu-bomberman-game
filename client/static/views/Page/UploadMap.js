define([
    'ui',
    'views/Page/Base',
    '_',
    'bn',
    'game_settings',
    'minimap'
], function($, BasePage, _, bn, game_settings, minimap) {
    var Page = BasePage.extend({
        template: 'page/upload_map',

        events: {
            'click #cancel': 'cancel'
        },

        constructor: function() {
            BasePage.prototype.constructor.apply(this, arguments);
            _.extend(this.events, this.constructor.__super__.events);
        },
        checkMap: function(map) {
            //check contents
            //check player positions
            for (var i = 0; i < map.length; i++) {
                map[i] = $.trim(map[i]);
            }
            var res = [map[0].length/2, map.length];
            for (var i = 0; i < map.length; i++) {
                if (map[i].length == 0)
                    break; //#TODO this is wrong!!!
                if (map[i].length != res[0]*2)
                    return undefined;
            }
            return res;
        },

        ready: function() {
            //#TODO CELL_SIZE
            $("#upload").on("change", function handleFileSelect(evt) {
                var files = document.getElementById('upload').files;//do this on upload
                /*if (!files.length) {
                    alert('Please select a file!');
                    return;
                }*/
                // Loop through the FileList and render image files as thumbnails.
                var file = files[0];
                var map_name = file.name;

                var reader = new FileReader();

                reader.onloadend = function(evt) {
                    if (evt.target.readyState == FileReader.DONE) { // DONE == 2
                        var contents = evt.target.result.split('\n');
                        var result = App.page.checkMap(contents);
                        if (!result){
                            $("#map_width").text('?');
                            $("#map_height").text('?');
                            $("#map_size").removeClass().addClass('badge badge-important');
                            App.show_message("error", "Invalid map file");// reset map?
                            return;
                        }
                        $("#map_size").removeClass().addClass('badge badge-success');
                        $("#map_width").text(result[0]);
                        $("#map_height").text(result[1]);
                        var canvas = $("#minimap");
                        var zoom = 4;
                        minimap.build(canvas, contents, result[0], result[1], 4);
                        App.page.mapLoaded = true;
                        App.page.mapData = contents;
                        var len = Math.min(map_name.lastIndexOf('.'), 32);
                        if (len == -1)
                            len = map_name.length;
                        $("#name").val(map_name.substr(0, len));
                    }
                };
                var contents = reader.readAsText(file);
            });
            if (!game_settings.initForm(App.protocol.use().default_game_settings, $("#settings")))
                return;

            $("#submit").click(function (e) {
                var map = {};
                var field = [];
                map.name = $("#name").val();
                if (!map.name) //|| wrong name
                {
                    App.show_message('error', 'Wrong map name');//error no name
                    return;
                }
                if (!App.page.mapLoaded) //|| wrong name
                {
                    App.show_message('error', 'Map was not loaded');//error no name
                    return;
                }
                map.settings = game_settings.getSettings();
                map.field = App.page.mapData;
                App.protocol.use().upload_map({
                    data: map,
                    success: function(data) {
                        App.show_message('info', 'Map loaded successfully.');
                        App.router.navigate('/lobby/', {trigger: true});
                    },
                    error: function(data) {
                        App.show_message('error', 'Bad settings or invalid map');
                        console.log(data);
                    }
                });
                return false
            });
        },

        loaded: function(success) {
            console.log("upload map");
        },

        cancel: function(event) {
            App.router.navigate('/lobby/', {trigger: true});
        }
    });

    return Page;
});
