define([
    'View',
    '$',
    '_'
], function(View, $, _) {
    var Page = View.extend({
        el: 'body',
        template: 'page/server_lobby',
        events: {
            'click #connect': 'connect',
            'click #test': 'test'
        },

        connect: function(event) {
            var url = this.$('#server-list').val();
            App.connect_to_server(url, function() {
                App.router.navigate('//', {trigger: false});
                App.router.navigate('/', {trigger: true});
            });
        },

        test: function() {
            var self = this;
            var url = this.$('#server-list').val();

            App.connect_to_server(url, function() {
                self.$('#content').empty();

                _.template('test_result_table', {}, function(error, html) {
                    if (error) { console.error(error); }
                    self.$('#content').append($(html));

                    /*requirejs(['test/tester'], function(tester) {
                        self.tester = new tester(App.protocol);
                        App.connect_to_server(url, function() {self.run_tests()});
                    });*/
                    requirejs([
                        'tester/new_tester',
                        'tester/tests/core',
                        'protocol/protocol',
                        'protocol/extensions/core',
                        'protocol/extensions/debug'                    ],
                    function(tester, tests, Pr, Core, Debug) {
                        self.tester = new tester({
                            protocol: Pr,
                            extensions: [Core, Debug],
                            url: url,
                            timeout: 5 * 1000
                        });

                        self.tester.run(tests);
                    });
                });
            });
        },

        run_tests: function() {
            var tbody = this.$('#result>tbody');

            this.tester.print_result = function(result) {
                var tr = $('<tr/>');
                var cl =
                tbody.append(tr);
                if (result.critical && !result.ok) {
                    cl = 'test-critical';
                } else {
                    cl = result.ok ? 'test-success': 'test-error';
                }
                var fields = ['ext_name', 'test_number', 'test_name', 'req', 'res', 'ok', 'error'];

                for (var i = 0; i < fields.length; ++i) {
                    var td = $('<td/>').addClass(cl);
                    var val = result[fields[i]];
                    tr.append(td);

                    if (['req', 'res'].indexOf(fields[i]) != -1 && _.isObject(val)) {
                        td.append($('<code/>').text(JSON.stringify(val)));
                    } else {
                        td.text(val);
                    }
                }
            };

            this.tester.run(['core']);
        }
    });

    return Page;
});
