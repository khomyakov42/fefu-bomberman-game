define(['views/Page/Base'], function(BasePage) {
    var Page = BasePage.extend({
        template: 'page/error404'
    });

    return Page;
});
