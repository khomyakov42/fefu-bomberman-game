define([
    'views/Page/BaseGame',
    '_',
    'game/game',
    'engine',
    'map',
    'map2d',
    'sprite',
    'sound'
], function(BasePage, _, Game, Engine, Map, map2d, sprite, sound) {
    var player_statuses = {
        idle: 'idle',
        ready: 'ready',
        game: 'playing',
        dead: 'dead'
    };

    var status_class = {
        idle: 'badge',
        ready: 'badge badge-warning',
        game: 'badge badge-success',
        dead: 'badge badge-important'
    };

    var Page = BasePage.extend({
        template: 'page/room',

        events: {
            'click #send-message': 'send_message',
            'submit #send-message': 'send_message',
            'click #start_game': 'start_game',
            'keydown': 'on_keydown',
            'keyup': 'on_keyup'
        },

        _socket_events: [
            'player_joined_game',
            'new_message',
            'player_left_game',
            'game_started',
            'game_end',
            'vote_start'
        ],


        constructor: function() {
            _.extend(this.events, this.constructor.__super__.events);
            BasePage.prototype.constructor.apply(this, arguments);
        },


        delegateEvents: function() {
            BasePage.prototype.delegateEvents.apply(this, arguments);
            for (var i = 0; i < this._socket_events.length; ++i) {
                App.protocol.use()['on_' + this._socket_events[i]](this['on_' + this._socket_events[i]]);
            }
        },


        undelegateEvents: function() {
            BasePage.prototype.undelegateEvents.apply(this, arguments);
            for (var i = 0; i < this._socket_events.length; ++i) {
                App.protocol.use().off_socket_event(
                    this._socket_events[i],
                    this['on_' + this._socket_events[i]]
                );
            }
            if (!App.page || !App.page.events)
                return;
            for (var i = 0; i < App.page.events.length; ++i)
                App.protocol.use().off_game_state(App.page.events[i]);
        },


        on_new_message: function(data) {
            var self = this;
            App.page.add_chat_message(data.username, data.text);
        },


        on_game_end: function(data) {
            App.page.add_chat_message(data.winner_name.length?data.winner_name:"Nobody", "Won");
        },


        on_player_joined_game: function(data) {
            App.page.add_player(data.username);
            App.page.add_chat_message(data.username, 'has joined the game');
        },
        
        
        on_vote_start: function(data) {
            App.page.set_player_status(data.username, 'ready');
            console.log(data.username);
        },


        on_player_left_game: function(data) {
            App.page.remove_player(data.username);
            App.page.add_chat_message(data.username, 'has left');
        },


        get_color: function(i) {
            var color = playerColors[i];
            var result = '#';
            for (var j = 0; j < 3; j++) {
                color[j] = playerColors[i][j] * 255;
                result += ((color[j]<16)?'0':'') + (color[j]).toString(16);
            }
            return result;
        },


        on_game_started: function(data) {
            if (!App.game || !App.game.engine.is_loaded())
                return;//show some error here

            var players = [];
            var colors = {};
            var statuses = {};

            for (var i = 0; i < data.players.length; i++) {
                players.push(data.players[i]);
                colors[data.players[i]] = App.page.get_color(i);
                statuses[data.players[i]] = 'game';
            }

            App.page.player_colors = colors;
            App.page.players = players;
            App.page.game_players = players;
            App.page.player_status = statuses;

            App.page.update_players();

            $('#game-block').css('background-image', '');

            App.game.prepare(data, App.protocol);

            App.game.started = true;
        },


        on_update_map: function(data) {
            if (!App.game.engine.is_loaded())//#TODO make this with queue
                return;
            if (!App.game.map) {
                App.game.start(data);
            }

            App.game.scene.tick = data.tick;

            App.game.update_data(data);

            for (var i = 0; i < data.players.length; i++) {
                if (i >= App.page.players.length)
                    break;
                App.page.set_player_status(App.page.game_players[i], data.players[i].dead?'dead':'game');
            }

            var pl = data.players[App.game.current_player_index];
            var fields = ['remaining_bombs', 'flame_length'];
            if (!pl)
                return;
            for (var i = 0; i < fields.length; i++) {
                var id = fields[i];
                if (App.game[id] != pl[id]) {
                    App.game[id] = pl[id];
                    $("#" + id).text(pl[id])
                }
            }
            return;
        },


        on_keydown: function(e) {
            if (App.game.started) {
                if (this.chatting) {
                    if ($('#text').is(":focus"))
                        return;
                    this.chatting = false;
                }
                if (!this.chatting) {
                    if ($('#text').is(":focus")) {
                        this.chatting = true;
                        return;
                    }
                }
                var code = e.keyCode;// ? e.keyCode : e.which;
                App.game.keydown(code, e);
            }
        },


        on_keyup: function(e) {
            if (!App.game.started)
                return;
            if (e.keyCode == 13) {
                if (this.chatting) {
                    this.chatting = false;
                    $('#bomberman-canvas').focus();
                    $('#text').blur();
                }
                else {
                    this.chatting = true;
                    $('#text').focus();
                }
            }
            if (this.chatting)
                return;
            if (App.game) {
                var code = e.keyCode;// ? e.keyCode : e.which;
                App.game.keyup(code, e);
            }
        },


        add_player: function(username) {
            if (!this.players)
                this.players = [];

            if (!this.player_status)
                this.player_status = {};

            if (this.players.indexOf(username) == -1) {
                this.players.push(username);

                this.player_status[username] = 'idle';

                this.update_players();
            }
        },


        remove_player: function(username) {
            if (App.game && App.game.started)
                return;

            if (!this.players)
                this.players = [];

            var index = this.players.indexOf(username);
            if (index != -1) {
                this.players.splice(index, 1);

                if (username in this.player_status)
                    delete this.player_status[username];

                this.update_players();
            }
        },


        get_player_status: function(username) {
            //if (!this.player_status)
            //    this.player_status = {};

            return this.player_status[username];
        },


        set_player_status: function(username, status) {
            if (!(username in this.player_status) ||
                !(status in player_statuses) ||
                status == this.player_status[username])

                return;

            this.player_status[username] = status;

            $('#' + username).removeClass().addClass(status_class[status] + ' pull-right').html(player_statuses[status]);
        },


        update_players: function() {
            var result = '';
            for (var i = 0; i < this.players.length; i++) {
                var status = this.get_player_status(this.players[i]);
                var color = '';
                if (this.players[i] in this.player_colors)
                    color = this.player_colors[this.players[i]];
                result += '<div><strong style="color: '+ color + ';">' + this.players[i] + '</strong><span id="' + this.players[i] +
                    '" class="' + status_class[status] + ' pull-right">' + player_statuses[status] + '</span></div>';
            }
            $('#players_list').html(result);
        },


        add_chat_message: function(username, text) {
            var self = this;
            _.template('chat_message', {self: {username: username, text:text}}, function(error, html) {
                self.$('#chat').append($(html));
                $('#chat')[0].scrollTop = 99999;
            });
        },


        send_message: function(e) {
            var text = this.$('#text');
            if (!/.{1,256}/.test(text.val())) {
                return false;
            }

            var self = this;
            this._send_message(text.val(), function() {text.val('');});
            return false;
        },


        _send_message: function(text, success) {
            App.protocol.use().send_message({
                data: {
                    message: text
                },
                success: success,
                error: function() {
                    App.show_message('error', 'Can not send a message, try again later.');
                }
            });
        },


        start_game: function() {
            if (!App.game.engine.is_loaded())
                return;
            this.set_player_status(App.user.get('username'), 'ready');
            $('#bomberman-canvas').focus();
            this.chatting = false;

            var start_button = this.$('#start_game');
            var self = this;
            this.events = [];
            this.events.push(_.bind(App.page.on_update_map, App.page));
            App.protocol.use().on_game_state(this.events[this.events.length - 1]);
            App.protocol.use().start_game({
                success: function() {
                    self._send_message('I am ready!');
                    start_button.addClass('btn-success');
                    start_button.attr('disabled', 'disabled');
                    start_button.text('Waiting other players');
                }
            })
        },

        ready: function() {
            this.players = [];
            this.player_colors = {};
            this.player_status = {};
            for (var i = 0; i < (App.user_in_room || []).length; ++i){
                this.on_player_joined_game({username: App.user_in_room[i]});
            }
            this.add_player(App.user.get('username'));
            
            for (var i = 0; i < (App.user_in_room_vote || []).length; ++i) {
                this.on_vote_start({username: App.user_in_room_vote[i]});
            }


            //_.delay(function() {
            if (App.game) {
                App.game.destroy();
                App.game = undefined;
            }

            App.game = new Game(($("#bomberman-canvas"))[0], true);
            App.game.on_progress = function(progress) {
                $("#progress").css("width", progress+"%");
            };
            App.game.on_finish = function(success){
                $("#progress_bar").removeClass().addClass("progress progress-success progress-striped");
                _.delay(function() {
                    $('#progress_bar').remove();
                    $('#game-block').css('background-image', 'url("/static/img/wait_players.gif")');
                    $('#bomberman-canvas').show();
                }, 1000);
            };
            App.game.init();
            //}, 2000);
        },
        free: function() {
            if (App.game) {
                App.game.destroy();
                App.game = undefined;
            }
            console.log('removed');
        }
    });

    return Page;
});
