define([
    'views/Page/Base',
    '_',
    'bn',
    'game_settings',
    'minimap'
], function(BasePage, _, bn, game_settings, minimap) {
    var Page = BasePage.extend({
        template: 'page/lobby',

        events: {
            'change #name': 'check_create_game_form',
            'change #password': 'check_create_game_form',
            'keyup #confirm_password': 'check_create_game_form',
            'change #map': 'select_map',

            'click #settings-map': 'toggle_settings',

            'click #create-room': 'create_room',
            'click .join-room': 'join_room'
        },

        games: [],
        maps: [],
        game_settings: {},

        constructor: function() {
            BasePage.prototype.constructor.apply(this, arguments);
            _.extend(this.events, this.constructor.__super__.events);
        },


        initialize: function(data) {
            this.games = (data && data.games) || [];
            this.maps = (data && data.maps) || [];
            BasePage.prototype.initialize.apply(this, arguments);
        },


        select_map: function(e) {
            this.check_create_game_form(e);
            var $opt = $(e.target);
            if ($opt.val() == 'none') {
                return;
            }

            var self = this;
            App.protocol.use().get_map({
                data: {
                    map_id: +$opt.val()
                },
                success: function(data) {
                    self.game_settings = data['settings'];
                    if (data.field) {
                        var canvas = $("#minimap");
                        var zoom = 4;
                        var width = data.field[0].length/2;
                        var height = data.field.length;
                        $("#map_width").text(width);
                        $("#map_height").text(height);

                        minimap.build(canvas, data.field, width, height, 4);
                    }
                    if (!game_settings.initForm(data.settings, $("#game-settings-block")))
                        return;
                    $('#max_players_count').val(data.max_player_count);
                    $('#max_players_count').attr('max', data.max_player_count);
                    $('#name').val(App.user.get('username') + '/' + data.name);
                }
            });
        },


        toggle_settings: function(e) {
            $('#game-settings-block').slideToggle("slow");
        },

        user_join: function(data) {
            App.user_in_room.push(data.username);
        },
        
        vote_user: function(data) {
            App.user_in_room_vote.push(data.username);
        },


        join_room: function(event, password) {
            var button = $(event.target);
            var self = this;


            App.user_in_room = [];
            App.user_in_room_vote = [];
            App.protocol.use().off_player_joined_game(this.user_join);
            App.protocol.use().off_vote_start(this.vote_user);
            App.protocol.use().on_player_joined_game(this.user_join);
            App.protocol.use().on_vote_start(this.vote_user);

            App.protocol.use().join_game({
                data: {
                    game_id: +button.attr('id'),
                    password: password || $('#password_room' + button.attr('id')).val()
                },

                success: function(data) {
                    App.user.set('room_id', button.attr('id'));
                    App.router.navigate('/room/' + App.user.get('room_id') + '/', {trigger: true});
                },

                error: function(code) {
                    switch(code) {
                        case 'game_not_found':
                            App.show_message('error', 'Room has been removed.');
                            var index = _.find(self.games, function(game) {
                                return game.id == button.attr('game_id');
                            });
                            return;
                        case 'game_already_started':
                            App.show_message('info','Game has already started.');
                            return;
                        case 'wrong_game_password':
                            App.show_message('error','Bad password.');
                            return;
                    }

                    console.error(code);
                }
            })
        },


        create_room: function() {
            if(!this.check_create_game_form()) {
                return false;
            }

            var self = this;
            var settings = game_settings.getSettings();
            //settings.player_resurrect_time = 1000;

            App.user_in_room = [];
            App.user_in_room_vote = [];
            App.protocol.use().off_player_joined_game(this.user_join);
            App.protocol.use().off_vote_start(this.vote_user);
            App.protocol.use().on_player_joined_game(this.user_join);
            App.protocol.use().on_vote_start(this.vote_user);

            App.protocol.use().create_game({
                data:{
                    name: $('#name').val(),
                    password: $('#password').val(),
                    map_id: +$('#map').val(),
                    settings: settings,
                    max_player_count: +$('#max_players_count').val()
                },

                success: function(data) {
                    App.user.set('room_id', data.game_id);
                    App.router.navigate('/room/' + App.user.get('room_id') + '/', {trigger: true});
                },
                error: function(code) {
                    console.error(code);
                }
            });
            return false;
        },

        check_create_game_form: function(event) {
            var inputs = event ? [$(event.target)] : [$('#name'), $('#password'), $('#confirm_password'), $('#map')];
            var bad = false;
            for (var i = 0; i < inputs.length; ++i) {
                var error = "";
                var val = inputs[i].val();

                switch(inputs[i].attr('id')) {
                    case 'name':
                        error = /^.{3,32}$/.test(val) ? "" : "long 3 to 15";
                        break;
                    case 'password':
                        error = /^.{0,256}$/g.test(val) ? "" : "long 0 to 256";
                        break;
                    case 'confirm_password':
                        var pass = $('#password').val();
                        error = val == pass? "" : "passwords do not match";
                        break;
                    case 'map':
                        error = val != 'none' ? "" : "select a map";
                        break;
                }

                inputs[i].parent()[error ? 'addClass' : 'removeClass']('error').children('.help-block').text(error);
                bad = error || bad;
            }
            return !bad;
        },

        ready: function() {
            var self = this;
            this.interval = setInterval(function() {
                self.update_game_list();
            }, 1000 * 5);
        },

        free: function() {
            clearInterval(this.interval);
        },

        update_game_list: function() {
            var self = this;
            App.protocol.use().game_list({
                success: function(data) {
                    var games = data.games;
                    var exist_games = {};
                    for (var i = 0; i < games.length; ++i) {
                        exist_games[games[i]['id']] = games[i];

                        var $el = $('#' + games[i]['id']).closest('tr');
                        if ($el.length) {
                            $el.children('td').first().text(games[i]['id'])
                                .next().text(games[i]['name'])
                                .next().text(games[i]['free_slots'])
                                .next().text(games[i]['total_slots']);
                        } else {
                            self.games.push(games[i]);
                            $('#rooms tr:last').after(
                                '<tr>'+
                                    '<td>' + games[i]['id'] + '</td>' +
                                    '<td>' + games[i]['name'] + '</td>' +
                                    '<td>' + games[i]['free_slots'] + '</td>' +
                                    '<td>' + games[i]['total_slots'] + '</td>' +
                                    '<td>' +
                                        '<form class="form-inline">' +
                                            '<input type="password" id="password_room' + games[i]['id'] + '">' +
                                            '<button type="button" class="btn join-room" id="' + self.games[i]['id'] + '">Join</button>' +
                                        '</form>' +
                                    '</td>' +
                                '</tr>'
                            );
                        }
                    }

                    for (var i = 0; i < self.games.length; ++i) {
                        if (!exist_games[self.games[i]['id']]) {
                            $('#' + self.games[i]['id']).closest('tr').remove();
                            self.games.splice(i, 1);
                            --i;
                        }
                    }
                }
            })
        }
    });

    return Page;
});
