define([
    '_'
], function(_) {
    var Debug = function (protocol) {
        this.protocol = protocol;
    };

    Debug.extname = 'debug.v0';


    _.extend(Debug.prototype, {
        reset: function(data) {
            this.protocol.request('reset', data);
        },


        game_set_debug_mode: function(data) {
            this.protocol.request('game_set_debug_mode', data);
        },


        game_set_tick: function(data) {
            this.protocol.request('game_set_tick', data);
        }
    });

    return Debug
});
