define([
    '_'
], function(_) {
    var ef = function() { };

    var http_request_wrapper = function(name, auth) {
        return function(data) {
            this._http_request(name, data, auth);
        }
    };

    var ws_send_event_wrapper = function(name) {
        return function(data) {
            this._ws_request(name, data);
        }
    };

    var ws_event_wrapper = function(name, on) {
        var func = on ? 'on_socket_event' : 'off_socket_event';
        return function(callback) {
            this[func](name, callback);
        }
    };


    var Core = function(protocol, data) {
        this.protocol = protocol;
        this.socket = null;
        this.socket_handlers = {};
        for (var d in data) {
            this[d] = data[d];
        }

        _.extend(this.status, this.protocol.status);
    };

    Core.extname = 'core.v0';

    _.extend(Core.prototype, {
        status: {
            LOGIN_EXISTS: 'user_already_exists'
        },
        web_socket_url: null,
        sid: null,

        get_sid: function() {
            return this.sid;
        },

        _http_request: function(name, data, auth) {
            auth = auth || false;
            if (auth) {
                data = data || {};
                data.data = data.data || {};
                data.data.sid = this.sid;
            }

            this.protocol.request(name, data);
        },

        _ws_request: function(name, data) {
            data = data || {};
            var d = data.data || {};
            d.event = name;
            try {
                this.socket.send(JSON.stringify(d));
                (data.success || ef)({});
            } catch (e) {
                (data.error || console.error)(e);
            }
        },

        on_socket_event: function(name, callback) {
            if (this.socket_handlers[name]) {
                for (var i = 0; i < this.socket_handlers[name].length; ++i) {
                    if (this.socket_handlers[name][i] == callback) {
                        return;
                    }
                }
                this.socket_handlers[name].push(callback);
            } else {
                this.socket_handlers[name] = [callback];
            }
        },

        off_socket_event: function(name, callback) {
            var list = this.socket_handlers[name] || [];
            for (var i = 0; i < list.length; ++i) {
                if (list[i] == callback) {
                    list.splice(i, 1);
                    break;
                }
            }
        },

        _connect_socket: function(callback) {
            this.socket = new WebSocket(this.websocket_url);

            var on_message = function(message) {

                var data = JSON.parse(message.data);
                //console.log('on event ' + data.event);
                if (self.socket_handlers[data.event] != undefined && self.socket_handlers[data.event].length) {
                    _.each(self.socket_handlers[data.event] || [], function(func) {
                        func(data || {});
                    });
                }
            };

            this.socket.onmessage = on_message;

            var self = this;
            this.on_socket_event('auth_error', function(data) {
                self.socket.close();
                self.socket = null;
            });


            this.socket.onopen = function() {
                callback();
                on_message({data: JSON.stringify({event:'open'})});
                self._ws_request('auth', {
                    data: {
                        sid: self.sid
                    }
                });
            };

            this.socket.onerror = function() {
                on_message({data: JSON.stringify({event: 'error'})});
            };

            this.socket.onclose = function() {
                on_message({data: JSON.stringify({event: 'closed'})});
            };
        },

        login: function (data) {
            data = data || {};
            var success = data.success || ef;
            var self = this;
            data.success = function(data) {
                if (data.sid) {
                    self.sid = data.sid;
                }
                self._connect_socket(function() {
                    success(data);
                });

            };
            this._http_request('login', data, false);
        },

        logout: function (data) {
            data = data || {};
            var success = data.success || ef;
            var self = this;
            data.success = function(data) {
                self.sid = "";
                self.socket.close();
                success(data);
            };
            this._http_request('logout', data, true);
        },

        game_action: ws_send_event_wrapper('game_action'),

        register: http_request_wrapper('register', false),
        upload_map: http_request_wrapper('upload_map', true),
        send_message: http_request_wrapper('send_message', true),
        game_list: http_request_wrapper('list_games',true),
        create_game: http_request_wrapper('create_game', true),
        join_game: http_request_wrapper('join_game', true),
        leave_game: http_request_wrapper('leave_game', true),
        start_game: http_request_wrapper('start_game', true),
        get_map: http_request_wrapper('get_map', true),
        list_maps: http_request_wrapper('list_maps', true)
    });

    var socket_actions = {};
    _.each(['game_started', 'player_left_game', 'player_joined_game', 'new_message', 'game_state', 'game_end', 'vote_start'], function(act) {
        socket_actions['on_' + act] = ws_event_wrapper(act, true);
        socket_actions['off_' + act] = ws_event_wrapper(act, false);
    });

    _.extend(Core.prototype, socket_actions);

    return Core;
});
