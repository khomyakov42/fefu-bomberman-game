define([
    '$',
    '_',
    'protocol/extensions/core'
],
function($, _, Core) {
    var ef = function() {};
    var DEFAULT_EXTENSION = Core.extname;

    var Protocol = function(url, extensions, callback) {
        var self = this;
        this.url = url;
        this.extensions = {};
        _.each(extensions, function(ext) { return self.extensions[ext.extname] = ext; });

        this.request('extensions', {
            success: function(data) {
                for (var ext in data.extensions) {
                    if (self.extensions[ext]) {
                        self.extensions[ext] = new self.extensions[ext](self, data.extensions[ext]);
                    } else {
                        delete self.extensions[ext];
                    }
                }
                if (!_.keys(self.extensions).length) {
                    App.show_message('error', 'Not supported extensions.');
                    console.error('server extensions', _.keys(data.extensions));
                }
                (callback || ef)(self);
            },
            error: function(code) {
                console.error(code);
            }
        });
    };

    _.extend(Protocol.prototype, {
        status: {
            SUCCESS: 'ok',
            HTTP_ERROR: 'http_error',
            BAD_FORMAT_FIELD: 'malformed_field',
            BAD_FORMAT_REQUEST: 'malformed_request',
            UNSUPPORTED_ACTION: 'unsupported_action'
        },

        use: function() {
            var extensions = arguments.length && _.reduce(arguments, function(x, y) {
                return x && y != undefined;
            }, true)? arguments : [DEFAULT_EXTENSION];

            for (var i = 0; i < extensions.length; ++i) {
                if (this.support(extensions[i])) {
                    return this.extensions[extensions[i]];
                }
            }

            console.error('\'' + extensions.toString() + '\' can not supported');
        },

        support: function(ext_name) {
            return this.extensions[ext_name];
        },

        request: function(request_code, data) {
            var self = this;
            data = data || {};

            var q = data.data || {};
            q.action = request_code;

            data.data = {q: JSON.stringify(q)};
            data.url = this.url;

            var error = data.error || function(e){console.error(e);};
            var success = data.success || ef;
            data.success = function(data) {
                if (data.status != self.status.SUCCESS) {
                    error(data.status, data);
                    return;
                }

                success(data);
            };


            data.error = function() {
                error(self.status.HTTP_ERROR, {});
            };

            data.dataType = 'JSONP';
            data.type = 'GET';
            $.ajax(data);
        },

        error_handler: ef
    });

    return Protocol;
});