var default_cell_size = 10.0;

var border_desc = {
    left_top: 1,
    none_top: 2,
    right_top: 4,
    left_none: 8,
    right_none: 16,
    left_bottom: 32,
    none_bottom: 64,
    right_bottom: 128
};

define(['_', 'gameobject', 'utils', 'map', 'map2d'], function(_, gameobject, utils, Map, map2d){
    var map3d = {
        representation3d: function() {
            map3d.representation3d.superclass.constructor.call(this);
        },
        ground_layer: function() {
            map3d.ground_layer.superclass.constructor.call(this, map3d.ground_sector);
            this.walls = {};
        },
        ground_sector: function(x, y, layer) {
            this.x = x;
            this.y = y;

            this.layer = layer;
            this.object_color = [];
            for (var i = 0; i < 3; i++)
                this.object_color.push(Math.random());
            this.object_color.push(1.0);

            this.rx = x*this.layer.representation.default_cell_size;
            this.ry = y*this.layer.representation.default_cell_size;
            this.changed = false;

            this.field = layer.representation.field;

            this.recreate();

            this.mvMatrix = mat4.create();
            this.nMatrix = mat3.create();
        }
    };

    utils.extend(map3d.representation3d, Map.representation, {
        revive: function(field) {
            map3d.representation3d.superclass.revive.call(this, field);
            this.pos_points = [];
            for (var y = 0; y < this.block_size; ++y) {
                for (var x = 0; x < this.block_size; ++x) {
                    var pos_point = [];
                    pos_point.push((x + 0)*this.default_cell_size, (y + 0)*this.default_cell_size, 0.0);
                    pos_point.push((x + 1)*this.default_cell_size, (y + 0)*this.default_cell_size, 0.0);
                    pos_point.push((x + 1)*this.default_cell_size, (y + 1)*this.default_cell_size, 0.0);
                    pos_point.push((x + 0)*this.default_cell_size, (y + 1)*this.default_cell_size, 0.0);
                    this.pos_points.push(pos_point);
                }
            }
            //creating index buffer
            var ind_points = [];
            for (var i = 0; i < this.block_size*this.block_size; ++i) {
                ind_points.push(i*4 + 0);
                ind_points.push(i*4 + 1);
                ind_points.push(i*4 + 2);

                ind_points.push(i*4 + 0);
                ind_points.push(i*4 + 2);
                ind_points.push(i*4 + 3);
            }

            this.ind_buffer = this.factory.createIndexBuffer(ind_points);
        }
    });

    utils.extend(map3d.ground_layer, map2d.layer, {
        revive: function(representation) {
            this.segments = [];
            this.depth *= representation.default_cell_size;
            var start = this.texture_rect[1], scale = this.texture_rect[3] - start;
            var hstart = this.texture_rect[0], hscale = this.texture_rect[2] - hstart;
            for (var i = 0; i < this.contour.length; i++) {
                var prev;
                for (var j = 0; j < this.contour[i].points.length; ++j) {
                    this.contour[i].points[j][0] *= representation.default_cell_size;
                    this.contour[i].points[j][1] *= representation.default_cell_size;
                    if (prev) {
                        var segment = [prev, this.contour[i].points[j], start + (prev[2])*scale,
                            start + (this.contour[i].points[j][2])*scale, hstart, hscale];
                        var normal = this.calculate_normal(segment);
                        if (this.contour[i].smooth && j >= 2) {
                            var s = this.segments[this.segments.length - 1];
                            normal[0] += s[7][0];
                            normal[1] += s[7][1];
                            this.normalize(normal);
                            s[7] = normal;
                        }
                        segment.push(normal);//#TODO not so good
                        segment.push(this.calculate_normal(segment));//#TODO not so good
                        this.segments.push(segment);
                    }
                    prev = this.contour[i].points[j];
                }
            }
            map3d.ground_layer.superclass.revive.call(this, representation);
        },
        normalize: function(v) {
            var l = Math.sqrt(v[0]*v[0] + v[1]*v[1]);
            v[0] /= l;
            v[1] /= l;
        },
        calculate_normal: function(segment) {
            var v = [segment[1][1] - segment[0][1], segment[0][0] - segment[1][0]];
            this.normalize(v);
            return v;//[v[1]/l, -v[0]/l];
        },
        bind: function() {
            this.texture.bind(0);
            this.texture_specular.bind(1);
        },
        get_border: function(border, rx, ry, offset) {
            var result = {};
            if (!this.walls[border])
                this.walls[border] = this.generate_wall(border)
            var wall = this.walls[border];
            var points = [], indices = [];
            var off = [rx, -ry, 0];
            for (var i = 0; i < wall.points.length; ++i)
                points.push(wall.points[i] + off[i%3])
            for (var i = 0; i < wall.indices.length; ++i)
                indices.push(wall.indices[i] + offset)
            //apply offset for textures
            return {points: points, indices: indices, normals: wall.normals, textures: wall.textures};
        },
        get_inner: function(x, y, o) {
            var result = {};
            result.points = [
                x, -y, this.depth,
                x + this.representation.default_cell_size, -y, this.depth,
                x + this.representation.default_cell_size, -y - this.representation.default_cell_size, this.depth,
                x, -y - this.representation.default_cell_size, this.depth
            ];
            result.normals = [
                0, 0, 1,
                0, 0, 1,
                0, 0, 1,
                0, 0, 1
            ];
            result.textures = [
                this.texture_rect[0], this.texture_rect[3],
                this.texture_rect[2], this.texture_rect[3],
                this.texture_rect[2], this.texture_rect[1],
                this.texture_rect[0], this.texture_rect[1],
            ];
            result.indices = [
                o + 0, o + 1, o + 2,
                o + 2, o + 0, o + 3
            ];
            return result;
        },
        generate_wall: function(border) {
            //#TODO I DON'T LIKE THIS CODE, BUT RIGHT NOW I HAVE NO CHOICE (T____T)
            //#TODO DAMN MAGIC CODE!!!!!!!!!!!!!!!!!!!!
            var n_p = 6;
            var points = [], normals = [], indices = [], textures = [];
            var default_cell_size2 = this.representation.default_cell_size/2;
            var default_cell_size = this.representation.default_cell_size;
            var borders = [border_desc.left_none,
                border_desc.none_top, border_desc.right_none, border_desc.none_bottom, border_desc.left_none,
                border_desc.none_top];
            var forbidden_borders = [0, border_desc.right_top, border_desc.right_bottom, border_desc.left_bottom,
            border_desc.left_top]

            var flip_coord = function(a, o, b, n, c) {
                var ind = (b == border_desc.none_top || b == border_desc.left_none);
                var x = a[o + 0];
                var y = (2*ind - 1)*a[o + 1];

                if (!n )
                    y -= default_cell_size2;

                if (b == border_desc.left_none || b == border_desc.right_none) {
                    var z = x;
                    x = y; y = z;
                }

                if (c) {//#TODO DAMN, NOT GUT
                    if (b == border_desc.left_none) {
                        y = -y;
                        if (!n)
                            y += default_cell_size;
                    }
                    if (b == border_desc.none_bottom) {
                        x = -x;
                        if (!n)
                            x += default_cell_size;
                    }
                }

                if (!n) {
                    x = Math.abs(x);
                    y = -Math.abs(y);
                }

                a[o + 0] = x; a[o + 1] = y;//may be faster
            }
            for (var j = 1; j < borders.length - 1; ++j)
            {
                var start = 0, stop = this.representation.default_cell_size;
                if (borders[j] == border_desc.none_bottom || borders[j] == border_desc.left_none) {
                    var tmp = start;
                    start = stop;
                    stop = tmp;
                }
                var o = points.length/3;
                if ((border & borders[j]) && (border & borders[j + 1]) &&(!(border & forbidden_borders[j]))) {
                    for (var k = 0; k < this.corner_segments; ++k) {
                        var phi1 = Math.PI*(-1 + k/this.corner_segments)/2.0, phi2 = Math.PI*(-1 + (k+1)/this.corner_segments)/2.0;
                        var cp1 = Math.cos(phi1), sp1 = Math.sin(phi1), cp2 = Math.cos(phi2), sp2 = Math.sin(phi2);
                        o = points.length/3;
                        var max_i = 0, max_j = 0, max_z = 0;
                        for (var i = 0; i < this.segments.length; ++i) {
                            if (this.segments[i][0][1] >= max_z) {
                                max_i = i;
                                max_j = 0;
                                max_z = this.segments[i][0][1];
                            }
                            if (this.segments[i][1][1] >= max_z) {
                                max_i = i;
                                max_j = 1;
                                max_z = this.segments[i][1][1];
                            }
                            points.push(
                                default_cell_size - sp1*(this.segments[i][0][0] - default_cell_size2), default_cell_size2 + cp1*(this.segments[i][0][0] - default_cell_size2)/*x1*/, this.segments[i][0][1]/*y1*/,
                                default_cell_size - sp1*(this.segments[i][1][0] - default_cell_size2), default_cell_size2 + cp1*(this.segments[i][1][0] - default_cell_size2)/*x1*/, this.segments[i][1][1]/*y1*/,
                                default_cell_size - sp2*(this.segments[i][0][0] - default_cell_size2), default_cell_size2 + cp2*(this.segments[i][0][0] - default_cell_size2)/*x1*/, this.segments[i][0][1]/*y1*/,
                                default_cell_size - sp2*(this.segments[i][1][0] - default_cell_size2), default_cell_size2 + cp2*(this.segments[i][1][0] - default_cell_size2)/*x1*/, this.segments[i][1][1]/*y1*/);

                            flip_coord(points, points.length - 12, borders[j], false, true);
                            flip_coord(points, points.length - 9, borders[j], false, true);
                            flip_coord(points, points.length - 6, borders[j], false, true);
                            flip_coord(points, points.length - 3, borders[j], false, true);
                            var c1 = cp1, c2 = cp2, s1 = sp1, s2 = sp2;
                            if (!this.corner_smooth) {
                                c1 = (c1 + c2)*0.5;
                                c2 = c1;
                                s1 = (s1 + s2)*0.5;
                                s2 = s1;
                            }

                            normals.push(
                                s1*this.segments[i][n_p][0],     c1*this.segments[i][n_p][0],     this.segments[i][n_p][1],
                                s1*this.segments[i][n_p + 1][0], c1*this.segments[i][n_p + 1][0], this.segments[i][n_p + 1][1],
                                s2*this.segments[i][n_p][0],     c2*this.segments[i][n_p][0],     this.segments[i][n_p][1],
                                s2*this.segments[i][n_p + 1][0], c2*this.segments[i][n_p + 1][0], this.segments[i][n_p + 1][1]);

                            flip_coord(normals, normals.length - 12, borders[j], true, true);
                            flip_coord(normals, normals.length - 9, borders[j], true, true);
                            flip_coord(normals, normals.length - 6, borders[j], true, true);
                            flip_coord(normals, normals.length - 3, borders[j], true, true);

                            textures.push(
                                this.segments[i][4] + k/this.corner_segments*this.segments[i][5],        this.segments[i][2],//i/this.segments.length,
                                this.segments[i][4] + k/this.corner_segments*this.segments[i][5],        this.segments[i][3],//(i + 1)/this.segments.length,
                                this.segments[i][4] + (k + 1)/this.corner_segments*this.segments[i][5],  this.segments[i][2],//i/this.segments.length,
                                this.segments[i][4] + (k + 1)/this.corner_segments*this.segments[i][5],  this.segments[i][3] //(i + 1)/this.segments.length
                            );

                            indices.push(
                                o + i*4 + 0, o + i*4 + 2, o + i*4 + 3,
                                o + i*4 + 3, o + i*4 + 0, o + i*4 + 1);
                        }

                        var o = points.length/3;
                        points.push(
                            default_cell_size - sp1*(this.segments[max_i][0][0] - default_cell_size2), default_cell_size2 + cp1*(this.segments[max_i][0][0] - default_cell_size2)/*x1*/, this.depth,
                            default_cell_size - sp2*(this.segments[max_i][1][0] - default_cell_size2), default_cell_size2 + cp2*(this.segments[max_i][1][0] - default_cell_size2)/*x1*/, this.depth,
                            default_cell_size2, -default_cell_size2/*x1*/, this.depth);///*y1*/,
                            //default_cell_size - sp2*(this.segments[i][1][0] - default_cell_size2), default_cell_size2 + cp2*(this.segments[i][1][0] - default_cell_size2)/*x1*/, this.segments[i][1][1]/*y1*/);

                        flip_coord(points, points.length - 9, borders[j], false, true);
                        flip_coord(points, points.length - 6, borders[j], false, true);
                        //flip_coord(points, points.length - 3, borders[j], false, false);

                        normals.push(
                            0, 0, 1,
                            0, 0, 1,
                            0, 0, 1);

                        textures.push(
                            this.segments[max_i][4] + k/this.corner_segments*this.segments[max_i][5],        this.segments[max_i][2],//i/this.segments.length,
                            //this.segments[max_i][4] + k/this.corner_segments*this.segments[i][5],        this.segments[max_i][3],//(i + 1)/this.segments.length,
                            this.segments[max_i][4] + (k + 1)/this.corner_segments*this.segments[max_i][5],  this.segments[max_i][2],//i/this.segments.length,
                            //this.segments[max_i][4] + (k + 1)/this.corner_segments*this.segments[max_i][5],  this.segments[max_i][3] //(i + 1)/this.segments.length
                            this.segments[max_i][4], this.segments[max_i][2]
                        );

                        indices.push(
                            o + 0, o + 1, o + 2);

                    }
                }
                if (border & borders[j])
                    continue;
                if (!(border & borders[j - 1]))
                    start = default_cell_size2;//swap start and stop sometimes
                if (!(border & borders[j + 1])) {
                    stop = default_cell_size2;
                    for (var k = 0; k < this.corner_segments; ++k) {
                        var phi1 = Math.PI*(k/this.corner_segments)/2.0, phi2 = Math.PI*((k+1)/this.corner_segments)/2.0;
                        var cp1 = Math.cos(phi1), sp1 = Math.sin(phi1), cp2 = Math.cos(phi2), sp2 = Math.sin(phi2);
                        o = points.length/3;
                        for (var i = 0; i < this.segments.length; ++i) {
                            points.push(
                                default_cell_size2 + sp1*this.segments[i][0][0], cp1*this.segments[i][0][0]/*x1*/, this.segments[i][0][1]/*y1*/,
                                default_cell_size2 + sp1*this.segments[i][1][0], cp1*this.segments[i][1][0]/*x1*/, this.segments[i][1][1]/*y1*/,
                                default_cell_size2 + sp2*this.segments[i][0][0], cp2*this.segments[i][0][0]/*x1*/, this.segments[i][0][1]/*y1*/,
                                default_cell_size2 + sp2*this.segments[i][1][0], cp2*this.segments[i][1][0]/*x1*/, this.segments[i][1][1]/*y1*/);

                            flip_coord(points, points.length - 12, borders[j], false, true);
                            flip_coord(points, points.length - 9, borders[j], false, true);
                            flip_coord(points, points.length - 6, borders[j], false, true);
                            flip_coord(points, points.length - 3, borders[j], false, true);
                            var c1 = cp1, c2 = cp2, s1 = sp1, s2 = sp2;
                            if (!this.corner_smooth) {
                                c1 = (c1 + c2)*0.5;
                                c2 = c1;
                                s1 = (s1 + s2)*0.5;
                                s2 = s1;
                            }

                            normals.push(
                                -s1*this.segments[i][n_p][0],     c1*this.segments[i][n_p][0],     this.segments[i][n_p][1],
                                -s1*this.segments[i][n_p + 1][0], c1*this.segments[i][n_p + 1][0], this.segments[i][n_p + 1][1],
                                -s2*this.segments[i][n_p][0],     c2*this.segments[i][n_p][0],     this.segments[i][n_p][1],
                                -s2*this.segments[i][n_p + 1][0], c2*this.segments[i][n_p + 1][0], this.segments[i][n_p + 1][1]);

                            flip_coord(normals, normals.length - 12, borders[j], true, true);
                            flip_coord(normals, normals.length - 9, borders[j], true, true);
                            flip_coord(normals, normals.length - 6, borders[j], true, true);
                            flip_coord(normals, normals.length - 3, borders[j], true, true);

                            textures.push(
                                this.segments[i][4] + k/this.corner_segments*this.segments[i][5],        this.segments[i][2],//i/this.segments.length,
                                this.segments[i][4] + k/this.corner_segments*this.segments[i][5],        this.segments[i][3],//(i + 1)/this.segments.length,
                                this.segments[i][4] + (k + 1)/this.corner_segments*this.segments[i][5],  this.segments[i][2],//i/this.segments.length,
                                this.segments[i][4] + (k + 1)/this.corner_segments*this.segments[i][5],  this.segments[i][3] //(i + 1)/this.segments.length
                            );

                            indices.push(
                                o + i*4 + 0, o + i*4 + 2, o + i*4 + 3,
                                o + i*4 + 3, o + i*4 + 0, o + i*4 + 1);
                        }
                    }
                }
                if (start == stop)// || (borders[j] == border_desc.left_none || borders[j] == border_desc.right_none))
                    continue;
                //continue;

                o = points.length/3;

                for (var i = 0; i < this.segments.length; ++i) {
                    points.push(
                        start, this.segments[i][0][0]/*x1*/, this.segments[i][0][1]/*y1*/,
                        start, this.segments[i][1][0]/*x1*/, this.segments[i][1][1]/*y1*/,
                        stop, this.segments[i][0][0]/*x1*/, this.segments[i][0][1]/*y1*/,
                        stop, this.segments[i][1][0]/*x1*/, this.segments[i][1][1]/*y1*/);

                    flip_coord(points, points.length - 12, borders[j]);
                    flip_coord(points, points.length - 9, borders[j]);
                    flip_coord(points, points.length - 6, borders[j]);
                    flip_coord(points, points.length - 3, borders[j]);

                    normals.push(
                        0, this.segments[i][n_p][0], this.segments[i][n_p][1],
                        0, this.segments[i][n_p + 1][0], this.segments[i][n_p + 1][1],
                        0, this.segments[i][n_p][0], this.segments[i][n_p][1],
                        0, this.segments[i][n_p + 1][0], this.segments[i][n_p + 1][1]);

                    flip_coord(normals, normals.length - 12, borders[j], true);
                    flip_coord(normals, normals.length - 9, borders[j], true);
                    flip_coord(normals, normals.length - 6, borders[j], true);
                    flip_coord(normals, normals.length - 3, borders[j], true);


                    textures.push(
                        this.segments[i][4],                        this.segments[i][2],//i/this.segments.length,
                        this.segments[i][4],                        this.segments[i][3],//(i + 1)/this.segments.length,
                        this.segments[i][4] + this.segments[i][5],  this.segments[i][2],//i/this.segments.length,
                        this.segments[i][4] + this.segments[i][5],  this.segments[i][3] //(i + 1)/this.segments.length
                    );

                    indices.push(
                        o + i*4 + 0, o + i*4 + 2, o + i*4 + 3,
                        o + i*4 + 3, o + i*4 + 0, o + i*4 + 1);

                }
            }
            return {points: points, normals: normals, indices: indices, textures: textures};
        }
    });

    _.extend(map3d.ground_sector.prototype, {
        applyChange: function(x, y, cell) {
            var ex1 = this.layer.items.indexOf(this.field.new_contents[y][x]) == -1;
            var ex2 = this.layer.items.indexOf(this.field.contents[y][x]) == -1;
            if (ex1 == ex2)
                return;
            this.changed = true;
        },
        draw: function() {
            if (this.changed) {
                this.recreate();
            }
            this.layer.representation.scene.getViewMatrix(this.mvMatrix);
            mat4.translate(this.mvMatrix, [
                this.rx,
                -this.ry,
                0//this.layer.depth
            ]);

            mat4.toInverseMat3(this.mvMatrix, this.nMatrix);
            mat3.transpose(this.nMatrix);
            //mat4.transpose(this.nMatrix);

            //this.layer.shader.u4f(this.layer.shader.object_color, this.object_color[0], this.object_color[1], this.object_color[2], this.object_color[3]);
            this.layer.shader.u4fv(this.layer.shader.view_matrix, this.mvMatrix);
            this.layer.shader.u3fv(this.layer.shader.normal_matrix, this.nMatrix);
            this.layer.factory.engine.gl.bindBuffer(this.layer.factory.engine.gl.ELEMENT_ARRAY_BUFFER, this.ind_buffer);

            this.layer.shader.bindBuffer(this.layer.shader.normal, this.norm_buffer);
            this.layer.shader.bindBuffer(this.layer.shader.vertex, this.pos_buffer);
            this.layer.shader.bindBuffer(this.layer.shader.texture, this.text_buffer);

            this.layer.factory.engine.gl.drawElements(this.layer.factory.engine.gl.TRIANGLES, this.ind_buffer.numItems,
                this.layer.factory.engine.gl.UNSIGNED_SHORT, 0);
        },
        addItem: function(x, y, cell) {

        },
        not_free: function(x, y) {
            return (x >=0 && y >= 0 && x < this.field.width && y < this.field.height &&
                this.layer.items.indexOf(this.field.new_contents[y][x]) != -1);
        },
        inner_point: function(x, y) {
            //if this point already inside the field
            return this.not_free(x - 1, y - 1) && this.not_free(x - 1, y) && this.not_free(x, y - 1);
        },
        border_point: function(x, y) {
            return (this.not_free(x - 1, y - 1)<<0) + (this.not_free(x, y - 1)<<1) + (this.not_free(x + 1, y - 1)<<2) +
                (this.not_free(x - 1, y)<<3) + (this.not_free(x + 1, y)<<4) +
                (this.not_free(x - 1, y + 1)<<5) + (this.not_free(x, y + 1)<<6) + (this.not_free(x + 1, y + 1)<<7);
        },
        recreate: function() {
            var points = [];
            var normals = [];
            var textures = [];
            var indices = [];
            for (var y = 0; y < this.layer.representation.block_size; ++y) {
                for (var x = 0; x < this.layer.representation.block_size; ++x) {
                    if (this.not_free(x + this.x, y + this.y)) {
                        var border = this.border_point(x + this.x, y + this.y);
                        if (border != 255){
                            var new_obj = this.layer.get_border(border, x*this.layer.representation.default_cell_size,
                                y*this.layer.representation.default_cell_size, points.length/3);
                            points = points.concat.apply(points, new_obj.points);
                            normals = normals.concat.apply(normals, new_obj.normals);
                            textures = normals.concat.apply(textures, new_obj.textures);
                            indices = indices.concat.apply(indices, new_obj.indices);
                            //tex_coords.concat.apply(points, new_obj.tex_coords);
                        }
                        if (this.inner_point(x + this.x, y + this.y)) {
                            var new_obj = this.layer.get_inner((x - 0.5)*this.layer.representation.default_cell_size,
                                (y - 0.5)*this.layer.representation.default_cell_size, points.length/3);
                            points = points.concat.apply(points, new_obj.points);
                            normals = normals.concat.apply(normals, new_obj.normals);
                            textures = normals.concat.apply(textures, new_obj.textures);
                            indices = indices.concat.apply(indices, new_obj.indices);
                            //tex_coords.concat.apply(points, new_obj.tex_coords);
                        }
                    }
                }
            }
            if (!this.pos_buffer)
                this.pos_buffer = this.layer.factory.createBuffer(points, 3, true);
            else
                this.layer.factory.updateBuffer(this.pos_buffer, points);

            if (!this.norm_buffer)
                this.norm_buffer = this.layer.factory.createBuffer(normals, 3, true);
            else
                this.layer.factory.updateBuffer(this.norm_buffer, normals);

            if (!this.text_buffer)
                this.text_buffer = this.layer.factory.createBuffer(textures, 2, true);
            else
                this.layer.factory.updateBuffer(this.text_buffer, textures);
            //var ind_points = [0, 1, 2, 0, 2, 3];
            if (!this.ind_buffer)
                this.ind_buffer = this.layer.factory.createIndexBuffer(indices, true);
            else
                this.layer.factory.updateIndexBuffer(this.ind_buffer, indices);
            this.changed = false;
        }
    });

    return map3d;
});