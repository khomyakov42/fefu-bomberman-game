var stats;
var gl_view;
var enabled3d = false;
var scene;
var player;
var intervalTrigger;

function webGLFullscreen()
{

}

var fullWindowState = false;
var res;

define(['$', 'sound', 'resources', 'factory', 'map', 'gameobject', 'map2d', 'map3d', 'sprite',
    'model', 'vfx', 'bomb_model', 'gl_matrix', 'glview', 'scene'],
    function($, sound, resources, factory, Map, gameobject, map2d, map3d, sprite, model, vfx, bomb_model) {
        var engine = {
            engine: function(canvas) {
                this.canvas = canvas;
                gl_view = new glView(canvas);
                this.gl_view = gl_view;
                this.gl = gl_view.gl;

                this.w = this.canvas.width;
                this.h = this.canvas.height;
                this.screen_dependent = [];
                var self = this;
                function on_fullscreen_change() {
                    var w = self.w;
                    var h = self.h;
                    if(document.mozFullScreen || document.webkitIsFullScreen) {
                        if (window.outerHeight && window.outerWidth) {
                            w = window.outerWidth;
                            h = window.outerHeight;
                        }
                        else {
                            w = $(window).width();
                            h = $(window).height();
                        }
                    }
                    canvas.width = w;
                    canvas.height = h;
                    //$(self.canvas).css("width", w + "px");
                    //$(self.canvas).css("height", h + "px");
                    self.update_screen(w, h);
                }

                document.addEventListener('mozfullscreenchange', on_fullscreen_change);
                document.addEventListener('webkitfullscreenchange', on_fullscreen_change);

                this.exclude = [];
                this.namespaces = [];

                this.add_namespace('2d');
                this.add_namespace('3d');
            },
            free: function(){
                if (this.intervalTrigger) {
                    window.clearInterval(this.intervalTrigger);
                    this.intervalTrigger = undefined;
                }
                this.res = undefined;
                this.scene = undefined;
                //sound.free();
            }
        };
        _.extend(engine.engine.prototype, {
            add_namespace: function(name) {
                this.namespaces.push(name);
            },
            request_list: function(path) {

            },
            exclude_namespace: function(name) {
                this.exclude.push(name);
            },
            is_loaded: function() {
                return (this.rm && this.rm.loaded && this.factory)
            },
            init: function(d3, resource_list, image_list) {
                if (!d3)
                    this.exclude_namespace('3d');
                else
                    this.exclude_namespace('2d');
                this['3d'] = d3;

                sound.init();
                this.rm = new resources.resource_manager(this);

                var self = this;

                if (this.on_progress)
                    this.rm.on_progress = this.on_progress;

                this.rm.on_finish = function(success) {
                    self.on_resources_loaded(success);
                };
                this.rm.add_path('shaders', '/static/data/shaders');
                this.rm.add_path('textures', '/static/data/textures');
                this.rm.add_path('textures', '/static/data/textures');
                if (sound.inited)
                    this.rm.add_path('sounds', '/static/data/sounds');
                this.rm.load_resource(resource_list);

                this.image_list = image_list;

                //init factory
            },
            on_resources_loaded: function(success) {
                if (!success) {
                    //#TODO call callback
                    return;
                }
                this.factory = new factory.factory(this);
                this.factory.resource_manager = this.rm;
                this.factory.register_class("sprite", sprite.sprite);//#TODO do this automatic
                this.factory.register_class("array_buffer", model.array_buffer);
                this.factory.register_class("index_buffer", model.index_buffer);
                this.factory.register_class("representation2d", map2d.representation2d);
                this.factory.register_class("representation3d", map3d.representation3d);
                this.factory.register_class("map2d.ground_layer", map2d.ground_layer);
                this.factory.register_class("map3d.ground_layer", map3d.ground_layer);
                this.factory.register_class("particle_system", vfx.particle_system);
                this.factory.register_class("bomb_model", bomb_model.bomb_list);
                this.factory.register_class("map2d.dynamic_layer", map2d.dynamic_layer);
                this.factory.register_class("sprite_list", sprite.sprite_list);
                this.factory.register_class("cell_sprite_list", sprite.cell_sprite_list);
                this.factory.register_class("explosion_list", sprite.explosion_list);
                this.factory.load_template_list(this.image_list);

                if (this.on_finish)
                    this.on_finish(true);

                var self = this;

                this.intervalTrigger = setInterval( function(){ // #TODO move this completely to game.js
                    if (!self.is_loaded() || self.paused)
                        return;
                    self.update_handler();
                }, 1000 / 60 );

            },
            free: function() {
                if (this.intervalTrigger) {
                    window.clearInterval(this.intervalTrigger);
                    this.intervalTrigger = undefined;
                }
            },
            update_screen: function(w, h) {
                this.gl_view.update_screen(w, h);
                for (var i = 0; i < this.screen_dependent.length; ++i)
                    this.screen_dependent[i].update_screen(w, h);
            },
            enter_full_screen: function() {
                if(document.mozFullScreen || document.webkitIsFullScreen){
                    if (document.mozCancelFullScreen)
                        document.mozCancelFullScreen(); //Firefox
                    else
                        document.webkitCancelFullScreen(); //Chrome
                }
                else{
                    if (this.canvas.mozRequestFullScreen)
                        this.canvas.mozRequestFullScreen(); //Firefox
                    else
                        this.canvas.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
                }

//...

//now i want to cancel fullscreen
//                document.webkitCancelFullScreen(); //Chrome
//                document.mozCancelFullScreen(); //Firefox
            }
        });
        return engine;
    }
);
