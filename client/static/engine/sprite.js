function extend(child, parent) {
    var F = function(){};
    F.prototype = parent.prototype;
    child.prototype = new F();
    child.prototype.constructor = child;
    child.superclass = parent.prototype;
}

var default_cell_size = 10.0;

define(['_', 'gameobject', 'map', 'gl_matrix'], function(_, gameobject, map){
    var sprite = {
        init: function() {
            var points = [0, 0, 0, default_cell_size, 0, 0, default_cell_size, default_cell_size, 0, 0, default_cell_size, 0];
            var textures = [0, 1, 1, 1, 1, 0, 0, 0];
            var indices = [0, 1, 2, 0, 2, 3];

            this.vertex_buffer = gameobject.createBuffer(points, 3);
            this.texture_buffer = gameobject.createBuffer(textures, 2);
            this.index_buffer = gameobject.createIndexBuffer(indices); // it can freely use buffer for map2d
        },
        drawable: function() {
            this.pos = [0, 0];
            this.mvMatrix = mat4.create();
        },
        sprite: function() {
            this.frame_x = 0;
            this.frame_y = 0;
            this.color = [1, 1, 1, 1];
            sprite.sprite.superclass.constructor.call(this)
        },
        sprite_list: function() {
            sprite.sprite_list.superclass.constructor.call(this)
        },
        cell_sprite_list: function() {
            sprite.cell_sprite_list.superclass.constructor.call(this)
        },
        explosion_list: function() {
            sprite.explosion_list.superclass.constructor.call(this);
        }
    };
    _.extend(sprite.drawable.prototype, {
        draw: function() {
            this.bind();
            this.transform();
        },
        bind: function() {
            this.scene.use(this.shader);
            this.texture.bind(0);
        },
        use: function() {
        },
        transform: function() {
            this.scene.getViewMatrix(this.mvMatrix);
            if (this.flip)
                mat4.rotateX(this.mvMatrix, Math.PI);
            mat4.translate(this.mvMatrix, [
                this.pos[0],
                this.cheat?-this.pos[1]:this.pos[1],
                this.depth]);

            this.shader.u4fv(this.shader.view_matrix, this.mvMatrix);
        },
        bindToObject: function(object) {
            this.bound = object;
        },
        updateBound: function() {
            if (this.bound.pos)
                this.pos = this.bound.pos;
        },
        update: function(t, dt, tick) {
            if (this.bound) {
                this.updateBound();
            }
        }

    });
    extend(sprite.sprite, sprite.drawable);
    _.extend(sprite.sprite.prototype, {
        use_animation: function(animation) {
            if (!animation) {
                this.animation = undefined;
                return;
            }
            this.current_animation = animation;
            this.animation_start_time = new Date().getTime();
            this.animation = this.animations[animation];
            if (this.animation.sound)
                this.animation.sound.play([this.pos[0], this.pos[1], this.depth])
            this.frame = 0;
        },
        updateFrame: function(t, animation) {
            if (!this.current_animation && ! animation)
                return;
            this.animation_to_apply = this.animations[animation?animation:this.current_animation];
            this.update_animation_with_time(t - this.animation_start_time);
        },
        update_animation_with_time: function(time) {
            if (!this.animation_to_apply || !this.animation_to_apply.frames || time < 0)
                return;
            this.frame = Math.floor(time*this.animation_to_apply.fps/1000.0); //new this.animations[animation].frames.length
            if (!this.animation_to_apply.loop && this.frame >= this.animation_to_apply.frames.length) {
                this.frame = this.animation_to_apply.frames.length - 1;
                if (this.animation_to_apply.next_animation)
                    this.use_animation(this.animation_to_apply.next_animation);
            }
            this.frame %= this.animation_to_apply.frames.length;
            if (!this.animation_to_apply.frames[this.frame]) {
                console.error(this.animation_to_apply, this.animation_to_apply.frames, this.animation_to_apply.frames[this.frame], this.frame);
                return;
            }
            this.frame_x = this.animation_to_apply.frames[this.frame][0];
            this.frame_y = this.animation_to_apply.frames[this.frame][1];
        },
        set_animation_progress: function(animation, progress) {
            var anim = this.animations[animation];
            if (!anim)
                return;
            this.frame = Math.floor(anim.frames.length*progress);
            if (!anim.loop && this.frame >= anim.frames.length) {
                this.frame = anim.frames.length - 1;
                /*if (anim.next_animation)
                    this.use_animation(anim.next_animation);*/
            }
            this.frame %= anim.frames.length;
            this.frame_x = anim.frames[this.frame][0];
            this.frame_y = anim.frames[this.frame][1];
        },
        stopAnimation: function() {
            if (!this.current_animation)
                return;
            var animation = this.animations[this.current_animation];
            this.frame_x = animation.frames[0][0];
            this.frame_y = animation.frames[0][1];
            this.current_animation = undefined;
            this.animation = undefined;
        },
        draw: function() {
            this.factory.engine.gl.depthMask(false);
            //sprite.sprite.superclass.draw.apply(this);
            this.bind();
            this.transform();

            //this.shader.applyTextureScale(this.texture);
            this.shader.u2f(this.shader.tile_offset, this.frame_x, this.frame_y);
            this.shader.u4f(this.shader.object_color, this.color[0], this.color[1], this.color[2], this.color[3]);

            this.index_buffer.bind();

            this.texture_buffer.bind(this.shader.texture);
            this.vertex_buffer.bind(this.shader.vertex);

            this.factory.engine.gl.drawElements(this.factory.engine.gl.TRIANGLES,
                this.index_buffer.i.length, this.factory.engine.gl.UNSIGNED_SHORT, 0);
            this.factory.engine.gl.depthMask(true);
        },
        updateBound: function() {
            sprite.sprite.superclass.updateBound.apply(this);
            if (this.bound.last_animation) {
                if (this.last_animation != this.bound.last_animation) {
                    this.last_animation = this.bound.last_animation;
                    if (this.last_animation == 'none') {
                        this.stopAnimation();
                        return;
                    }
                    this.use_animation(this.bound.last_animation);
                }
            }
        },
        update: function(t, dt, tick) {
            sprite.sprite.superclass.update.call(this, t, dt, tick);
            this.updateFrame(t);
        }
    });

    extend(sprite.sprite_list, sprite.sprite);

    _.extend(sprite.sprite_list.prototype, {
        revive: function() {
            //this.use_animation('default');
            if (this.animations.default)
                this.len = this.animations.default.frames.length/(this.animations.default.fps*0.033);
        },
        draw: function() {
            if (!this.bound)
                return;
            this.bind();

            this.shader.u4f(this.shader.object_color, this.color[0], this.color[1], this.color[2], this.color[3]);

            this.index_buffer.bind();

            this.texture_buffer.bind(this.shader.texture);
            this.vertex_buffer.bind(this.shader.vertex);


            for (var i = 0; i < this.bound.length; i++) {
                this.apply_obj(i, this.bound[i]);//
                this.transform();
                this.factory.engine.gl.drawElements(this.factory.engine.gl.TRIANGLES, this.index_buffer.i.length,
                    this.factory.engine.gl.UNSIGNED_SHORT, 0);
            }
        },
        apply_obj: function(id, obj) {
            this.set_animation_progress('default', (this.scene.tick - this.bound[id].created_at)/this.len);
            this.pos = [this.bound[id].x/1000, this.bound[id].y/1000]; ///#TODO fix this shit!!!;
            this.shader.u2f(this.shader.tile_offset, this.frame_x, 0);//this.frame_y);
        },
        updateBound: function() {}
        //
    });

    extend(sprite.cell_sprite_list, sprite.sprite_list);

    _.extend(sprite.cell_sprite_list.prototype, {
        apply_obj: function(id, obj) {
            this.pos = [Math.floor(0.5 + obj.x/10000)*10, Math.floor(0.5 + obj.y/10000)*10]; ///#TODO fix this shit!!!;
            var color = playerColors[id];
            this.shader.u4f(this.shader.object_color, color[0], color[1], color[2], color[3]);//this.frame_y);
        }
    });

    extend(sprite.explosion_list, sprite.sprite_list);

    _.extend(sprite.explosion_list.prototype, {
        revive: function() {

        },
        draw: function() {
            if (!this.bound)
                return;
            this.bind();

            this.shader.u4f(this.shader.object_color, this.color[0], this.color[1], this.color[2], this.color[3]);

            this.index_buffer.bind();

            this.texture_buffer.bind(this.shader.texture);
            this.vertex_buffer.bind(this.shader.vertex);
            for (var i = 0; i < this.bound.length; i++) {
                var pos = [this.bound[i].x/1000, this.bound[i].y/1000];
                var progress = (this.scene.tick - this.bound[i].created_at)/(this.bound[i].destroy_at - this.bound[i].created_at);

                this.pos = [pos[0], pos[1]];
                this.set_animation_progress('center', progress);
                this.shader.u2f(this.shader.tile_offset, this.frame_x, this.frame_y);
                this.transform();
                this.factory.engine.gl.drawElements(this.factory.engine.gl.TRIANGLES, this.index_buffer.i.length,
                    this.factory.engine.gl.UNSIGNED_SHORT, 0);

                //this.frame_y = 2;
                for (var j = -this.bound[i].left_length; j <= this.bound[i].right_length; j++) {
                    if (j == 0)
                        continue;
                    this.set_animation_progress('horizontal', progress);
                    this.pos = [pos[0] + j*default_cell_size, pos[1]];
                    this.shader.u2f(this.shader.tile_offset, this.frame_x, this.frame_y);
                    this.transform();
                    this.factory.engine.gl.drawElements(this.factory.engine.gl.TRIANGLES, this.index_buffer.i.length,
                        this.factory.engine.gl.UNSIGNED_SHORT, 0);
                }
                for (var j = -this.bound[i].up_length; j <= this.bound[i].down_length; j++) {
                    if (j == 0)
                        continue;
                    this.set_animation_progress('vertical', progress);
                    this.pos = [pos[0], pos[1] + j*default_cell_size];
                    this.shader.u2f(this.shader.tile_offset, this.frame_x, this.frame_y);
                    this.transform();
                    this.factory.engine.gl.drawElements(this.factory.engine.gl.TRIANGLES, this.index_buffer.i.length,
                        this.factory.engine.gl.UNSIGNED_SHORT, 0);
                }
            }
        },
        update: function(t, dt, tick) {
        }
    });

    return sprite;
});