

define(['resources', 'gameobject', 'resources', 'glview', '_'], function(resources){
    var factory = {
        factory: function(engine) {
            this.classes = {};
            this.objects = {};
            this.namespaces = [];
            this.exclude = [];
            this.templates = {};
            this.engine = engine;
        }
    };
    _.extend(factory.factory.prototype, {
        add_namespace: function(name) {
            this.namespaces.push(name);
        },
        exclude_namespace: function(name) {
            this.exclude.push(name);
        },
        free: function() {
            for (var i = 0; i < this.objects.length; ++i) {
                if (this.objects[i].free)
                    this.objects[i].free();
            }
            //#TODO clear all arrays
        },
        load_resources: function(resource_list) {
            if (typeof resource_list === "string") {
                return this.engine.rm.get(resource_list);
            }
            var node = {};
            for (var name in resource_list) {
                node[name] = this.load_resources(resource_list[name]);
            }
            return node;
        },
        load_template_list: function(template_list, name_prefix) {
            if (!name_prefix) {
                name_prefix = "";
            }
            if (template_list.class) {
                if (!(template_list.class in this.classes)) {
                    console.log('Unknown class ', template_list);
                    return;
                }

                /*var template = _.extend({}, template_list);
                if (template.resources) {
                    var resources = this.load_resources(template_list.resources);
                    delete template.resources;
                    template.resources = resources;
                }*/
                this.templates[name_prefix] = template_list;
                _.extend(this.templates[name_prefix], {instance_count: 0});

                if (template_list.static) {
                    this.create(name_prefix, name_prefix); //#TODO take order into account
                }

                return;
            }
            for (var name in template_list) {
                if (this.engine.exclude.indexOf(name) == -1) {
                    var prefix = name_prefix;
                    if (this.engine.namespaces.indexOf(name) == -1)
                        prefix += "/" + name;
                    this.load_template_list(template_list[name], prefix);
                }
            }
        },
        register_class: function(name, callback) {
            if (!(name in this.classes)) {
                this.classes[name] = callback;
            }
        },
        get_properties: function(instance, template) {
            if (typeof template != "object") {
                if (typeof template === "string") {
                    if (template.charAt(0) == ":")
                        return this.resource_manager.get(template);
                    if (template.charAt(0) == "/") {
                        var res = this.get(template);//checking for static object
                        if (!res)
                            res = this.create_delayed(template, undefined, instance);
                        return res;
                    }
                }
                return template;
            }
            var result = {};

            if (template && template.length) {
                result = [];
                for (var i = 0; i < template.length; i++) {
                    result.push(this.get_properties(instance, template[i]));
                }
                return result;
            }

            for (var name in template) {
                if (!template[name])
                    result[name] = template[name];
                else
                    result[name] = this.get_properties(instance, template[name]);
            }

            return result;
        },
        base_create: function(template, name) {
            if (!(template in this.templates)) {
                console.error("Unknown template name ", template);
                return undefined;
            }
            if (!name)
                name = template + this.templates[template].instance_count;
            var instance = new this.classes[this.templates[template].class]();
            _.extend(instance, this.get_properties(instance, this.templates[template]));

            instance.name = name;

            instance.factory = this;

            this.objects[name] = instance;

            return instance;
        },
        create_delayed: function(template, name) {
            var instance = this.base_create.apply(this, arguments);
            if (!instance)
                return instance;

            this.to_revive.push({instance:instance, arguments:arguments});
            return instance;
        },
        create: function(template, name) {
            this.to_revive = [];
            var instance = this.base_create.apply(this, arguments);
            if (!instance)
                return instance;

            if (instance.revive) {
                var args = [].concat.apply([], arguments).slice(2, arguments.length);
                instance.revive.apply(instance, args);
            }

            while (this.to_revive.length) {
                var container = this.to_revive.pop();
                var args = [].concat.apply([], container.arguments).slice(2, container.arguments.length);
                container.instance.revive.apply(container.instance, args);
            }

            return instance;
        },
        get: function(name) {
            return this.objects[name];
        },
        //some subobject for creating geometry
        createBuffer: function(array, basis, dynamic) {
            var result = this.engine.gl.createBuffer();
            this.engine.gl.bindBuffer(this.engine.gl.ARRAY_BUFFER, result);
            this.engine.gl.bufferData(this.engine.gl.ARRAY_BUFFER, new Float32Array(array),
                dynamic?this.engine.gl.DYNAMIC_DRAW:this.engine.gl.STATIC_DRAW);
            result.itemSize = basis;
            result.numItems = array.length/basis;

            return result;
        },
        createIndexBuffer: function(array, dynamic) {
            var result = this.engine.gl.createBuffer();
            this.engine.gl.bindBuffer(this.engine.gl.ELEMENT_ARRAY_BUFFER, result);
            this.engine.gl.bufferData(this.engine.gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(array), dynamic?this.engine.gl.DYNAMIC_DRAW:this.engine.gl.STATIC_DRAW);
            result.itemSize = 1;
            result.numItems = array.length;
            return result;
        },
        subBuffer: function(buffer, array) {
            this.engine.gl.bindBuffer(this.engine.gl.ARRAY_BUFFER, buffer);
            this.engine.gl.bufferSubData(this.engine.gl.ARRAY_BUFFER, 0, new Float32Array(array));
            buffer.numItems = array.length/buffer.itemSize;
        },
        updateBuffer: function(buffer, array) {
            this.engine.gl.bindBuffer(this.engine.gl.ARRAY_BUFFER, buffer);
            this.engine.gl.bufferData(this.engine.gl.ARRAY_BUFFER, new Float32Array(array), this.engine.gl.DYNAMIC_DRAW);
            buffer.numItems = array.length/buffer.itemSize;
        },
        updateIndexBuffer: function(buffer, array) {
            this.engine.gl.bindBuffer(this.engine.gl.ELEMENT_ARRAY_BUFFER, buffer);
            this.engine.gl.bufferData(this.engine.gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(array), this.engine.gl.DYNAMIC_DRAW);
            buffer.numItems = array.length;
        }
    });
    return factory;
});