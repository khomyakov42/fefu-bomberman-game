define(['_'], function(_){
    var utils = {
        extend: function(child, parent, child_proto) {
            var F = function(){};
            F.prototype = parent.prototype;
            child.prototype = new F();
            child.prototype.constructor = child;
            child.superclass = parent.prototype;
            if (child_proto)
                _.extend(child.prototype, child_proto);
        }
    };
    return utils;
});
