define(['gl_matrix', '_'], function(){
    var vfx = {
        direction_vector: [[-1, 0], [0, 1], [1, 0], [0, -1]],
        direction_add: [[0, -1], [1, 0], [1, -1], [1, -1]],
        direction_dirs: ['left_length', 'up_length', 'right_length', 'down_length'],
        init:function(){},
        particle_system: function() {
            this.mv_matrix = mat4.create();
        }
    };
    _.extend(vfx.particle_system.prototype, {
        revive: function(explosion) {
            this.px = explosion.x*this.default_cell_size/10000;
            this.py = explosion.y*this.default_cell_size/10000;
            this.ex = explosion;
            this.positions = [];
            this.speeds = [];
            this.accels = [];
            this.life = [];
            var time = (new Date().getTime())/1000.0;
            //if (typeof first_time == "undefined") {
            for (var i = 0; i < 4; i++) {
                var len_x = ((explosion[vfx.direction_dirs[i]])*vfx.direction_vector[i][0] + vfx.direction_add[i][0])*this.default_cell_size;
                var len_y = ((explosion[vfx.direction_dirs[i]])*vfx.direction_vector[i][1] + vfx.direction_add[i][1])*this.default_cell_size;
                for (var k = 0; k < this.count*(explosion[vfx.direction_dirs[i]] + 0.5); k++) {
                    this.positions.push(
                        Math.random()*len_x,
                        Math.random()*len_y, 5);
                    this.speeds.push(0,0,0);
                    this.life.push((7 + explosion.destroy_at - explosion.created_at)*0.033, Math.random()*0.1);
                    this.accels.push(0, 0, 0);
                }
            }

            this.start_time = time;
            this.time = time;
            this.pos_buffer = this.factory.createBuffer(this.positions, 3, false);
            this.speed_buffer = this.factory.createBuffer(this.speeds, 3, false);
            this.accel_buffer = this.factory.createBuffer(this.accels, 3, false);
            this.life_buffer = this.factory.createBuffer(this.life, 2, false);
        },
        update: function(t, dt, tick) {
            this.time = ((t/1000.0 - this.start_time) + Math.max(0, (tick - this.ex.created_at - 7))*0.033)*0.5;
            if (tick > this.ex.destroy_at+15)
                this.deleted = true;
        },
        draw: function() {
            //this.factory.engine.gl.blendFunc(this.factory.engine.gl.SRC_ALPHA, this.factory.engine.gl.ONE);
            this.factory.engine.gl.depthMask(false);
            this.scene.use(this.shader);
            this.texture.bind(0);

            this.scene.getViewMatrix(this.mv_matrix);

            mat4.translate(this.mv_matrix, [
                this.px,
                -this.py,
                0//this.layer.depth
            ]);/*

            mat4.toInverseMat3(this.mvMatrix, this.nMatrix);
            mat3.transpose(this.nMatrix);
            //mat4.transpose(this.nMatrix);*/

            //this.layer.shader.u4f(this.layer.shader.object_color, this.object_color[0], this.object_color[1], this.object_color[2], this.object_color[3]);
            this.shader.u4fv(this.shader.view_matrix, this.mv_matrix);
            this.shader.u1f(this.shader.time, this.time);

            this.shader.bindBuffer(this.shader.life, this.life_buffer);
            this.shader.bindBuffer(this.shader.vertex, this.pos_buffer);
            this.shader.bindBuffer(this.shader.speed, this.speed_buffer);
            this.shader.bindBuffer(this.shader.accel, this.accel_buffer);

            this.shader.u1f(this.shader.active, +(this.scene.tick < this.ex.destroy_at));
            this.shader.u1f(this.shader.w, 8.0);
            this.shader.u1f(this.shader.h, 8.0);

            gl.drawArrays(gl.POINTS, 0, this.pos_buffer.numItems);
            this.factory.engine.gl.depthMask(true);
            //this.factory.engine.gl.blendFunc(this.factory.engine.gl.SRC_ALPHA, this.factory.engine.gl.ONE_MINUS_SRC_ALPHA);
            //this.factory.engine.gl.depthFunc(this.factory.engine.gl.LEQUAL);

        }
    });
    return vfx;
});