var sceneObject = {
    'updateable': 0,
    'drawable': 1
};


define(['_', 'utils', 'gl_matrix', 'resources', 'gameobject', 'map'], function(_, utils){
    var scene = {
        init:function () {},
        scene: function(engine) {
            this.objects = [];
            this.cameras = [];

            this.tick = 0;

            this.engine = engine;

            this.p_matrix = mat4.create();
            this.mvMatrix = mat4.create();
        },
        camera: function (Scene) {
            this.pos = [0, 0, 0];
            this.zoom = 4;
            this.min = [0, 0, 0];
            this.max = [9999, 9999, 9999];
            this.scene = Scene;
        },
        camera2d: function(Scene) {
            scene.camera2d.superclass.constructor.call(this, Scene);
        },
        camera3d: function(Scene) {
            scene.camera2d.superclass.constructor.call(this, Scene);
            this.fov = 60;
            this.aspect = 1;
            this.z_near = 0.1;
            this.z_far = 1000;
        }
    };


    _.extend(scene.scene.prototype, {
        new_object:function(obj, type){
            this.objects.push(obj);
            //obj.res = this.res;
            obj.scene = this;
            if (obj.init)
                obj.init(new Date().getTime());
            //obj.init(gl, t);
            //obj.init(gl, t);
        },
        draw:function(){
            var len = this.objects.length;
            this.engine.gl.clear(this.engine.gl.COLOR_BUFFER_BIT|this.engine.gl.DEPTH_BUFFER_BIT);
            for (var j = 0; j < len; j++)
            {
                var i = j;
                if (this.objects[i].draw && this.objects[i].type != sceneObject.updateable && !this.objects[i].deleted)
                {
                    this.objects[i].draw();
                }
            }
        },
        update:function(t, dt, tick){
            var to_delete = [];

            //mat4.identity(this.mvMatrix);
            if (this.camera)
                this.camera.apply_matrix(this.mvMatrix);


            for (var i = 0; i < this.objects.length; i++)
            {
                if (this.objects[i].update && this.objects[i].type != sceneObject.drawable && !this.objects[i].deleted)
                    this.objects[i].update(t, dt, tick);
                if (this.objects[i].deleted)
                    to_delete.push(i);
            }
            for (var i = 0; i < to_delete.length; i++)
                this.objects.splice(to_delete[i] - i, 1);
        },
        step:function()
        {
            var time = new Date().getTime();
            if (this.lastTime)
            {
                this.update(time, time - this.lastTime, this.tick);//#TODO fix this
                this.draw();
            }
            this.lastTime = time;
        },
        view2d: function(w, h) {
            mat4.identity(this.p_matrix);
            mat4.ortho(0, this.engine.gl.viewportWidth, this.engine.gl.viewportHeight, 0, -1, 1, this.p_matrix);
        },
        getViewMatrix: function(matrix) {
            mat4.set(this.mvMatrix, matrix);
        },
        use: function(shader) {
            if (!shader)
                return;
            shader.use();
            this.engine.gl.uniformMatrix4fv(shader.projection_matrix, 0, this.p_matrix);
        },
        add_camera: function(camera) {
            this.cameras.push(camera);//#TODO may be add named cameras
        },
        use_camera: function(id) {
            if (id >= this.cameras.length || id < 0)
                return;
            this.camera = this.cameras[id];
            this.camera.projection_matrix(this.p_matrix);
        },
        update_screen: function(w, h) {
            this.camera.projection_matrix(this.p_matrix);
        }
    });



    _.extend(scene.camera.prototype, {
        apply_matrix: function(v_matrix) {
            mat4.identity(v_matrix);
        },
        projection_matrix: function(p_matrix) {
            mat4.identity(p_matrix);
        }
    });


    utils.extend(scene.camera2d, scene.camera, {
        apply_matrix: function(v_matrix) {
            scene.camera2d.superclass.apply_matrix.call(this, v_matrix);

            mat4.scale(v_matrix, [this.zoom, this.zoom, 1.0]);
            mat4.translate(v_matrix, [
                -Math.max(Math.min(this.pos[0] - this.scene.engine.gl.viewportWidth/(2*this.zoom), this.max[0]), this.min[0]),
                -Math.max(Math.min(this.pos[1] - this.scene.engine.gl.viewportHeight/(2*this.zoom), this.max[1]), this.min[1]),
                -Math.max(Math.min(this.pos[2], this.max[2]), this.min[2]),
            ]);

        },
        projection_matrix: function(p_matrix) {
            scene.camera2d.superclass.projection_matrix.call(this, p_matrix);
            mat4.ortho(0, this.scene.engine.gl.viewportWidth, this.scene.engine.gl.viewportHeight, 0, -1, 1, p_matrix);
        }
    });


    utils.extend(scene.camera3d, scene.camera, {
        apply_matrix: function(v_matrix) {
            scene.camera3d.superclass.apply_matrix.call(this, v_matrix);
            //var time = (new Date().getTime())/10000.0;
            var asp = this.aspect;
            if (asp > 1)
                asp = 1.0/asp;
            mat4.lookAt([this.pos[0] - 0/*50*Math.cos(time)*/, -this.pos[1]-60*asp/*Math.sin(time)*/, 100*asp], [this.pos[0], -this.pos[1], this.pos[2]], [0, 0, 1], v_matrix);
        },
        projection_matrix: function(p_matrix) {
            scene.camera3d.superclass.projection_matrix.call(this, p_matrix);
            mat4.perspective(this.fov, /*this.aspect*/this.scene.engine.gl.viewportWidth / this.scene.engine.gl.viewportHeight, this.z_near, this.z_far,
                p_matrix);
        }
    });


    return scene;
});
