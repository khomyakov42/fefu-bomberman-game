var gl;

var glView = function(canvas)
{
    this.gl = this.initGL(canvas);
    this.canvas = canvas;
    this.update_screen(this.canvas.width, this.canvas.height);
    this.init();
    gl = this.gl;
}

glView.prototype = {
    init: function() {
        this.gl.enable(this.gl.BLEND);
        this.gl.enable(this.gl.DEPTH_TEST);
        this.gl.depthFunc(this.gl.LEQUAL);
        this.gl.blendFunc(this.gl.SRC_ALPHA, this.gl.ONE_MINUS_SRC_ALPHA);
        this.gl.clearColor(0.0, 0.0, 0.0, 1.0);
    },
    update_screen: function(w, h)
    {
        this.gl.viewportWidth = w;
        this.gl.viewportHeight = h;
        this.width = w;
        this.height = h;
        this.gl.viewport(0, 0, this.width, this.height);
        console.log(this.canvas.width, this.canvas.height, this.width, this.height);
    },
    initGL:function(canvas) {
        var gl;
        try {
            gl = canvas.getContext("experimental-webgl", { antialias: true });
        } catch (e) {
        }
        if (!gl) {
            alert("Could not initialise WebGL, sorry :-(");
        }
        return gl;
    }
};

define(['gl_matrix', 'resources'], function(){
    return {
        init:function () {}
    };
});
