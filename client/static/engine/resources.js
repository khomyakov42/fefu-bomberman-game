var priorities = ['vertex', 'fragment', 'shader'];

define(['$', '_', 'sound', 'utils'], function($, _, sound, utils){
    var resources = {
        resource_manager: function(engine) {
            this.engine = engine;
            this.pathes = {};
            this.resources = [];
            this.types = {};
            this.exclude = [];
            this.namespaces = [];
            this.register_resource('texture', resources.texture);
            this.register_resource('vertex', resources.shader);
            this.register_resource('fragment', resources.shader);
            this.register_resource('shader', resources.shader_program);
            if (sound.inited)
                this.register_resource('sound', sound.sound);
            this.register_resource('tileset', resources.tileset);
        },
        resource: function() {
            this.failed = false;
        },
        texture: function() {
            // creating texture
            resources.texture.superclass.constructor.call(this);
        },
        shader: function() {
            // creating texture
            resources.shader.superclass.constructor.call(this);
        },
        shader_program: function() {
            // creating texture
            resources.shader_program.superclass.constructor.call(this);
        },
        tileset: function() {
            // creating texture
            resources.tileset.superclass.constructor.call(this);
        },
        sound: function() {
            // creating texture
            resources.sound.superclass.constructor.call(this);
        }
    };

    _.extend(resources.resource.prototype, {
        load: function(on_finish) {
            on_finish.call(this.resource_manager, this, this.failed);

        }
    });

    utils.extend(resources.shader, resources.resource, {
        load: function(resource_description, on_finish) {
            var self = this;
            jQuery.ajax(resource_description.file, {
                //async: true,
                success: function(responseText, textStatus, XMLHttpRequest) {
                    if(textStatus != "success") {
                        self.failed = true;
                        resources.shader.superclass.load.call(self, on_finish);
                        return;// null;
                    }
                    self.create_resource({data:responseText, type:resource_description.type});
                    resources.shader.superclass.load.call(self, on_finish);
                }
            });
        },
        create_resource: function(resource_description) {
            if (resource_description.type == "fragment") {
                this.shader = this.resource_manager.engine.gl.createShader(this.resource_manager.engine.gl.FRAGMENT_SHADER);
            } else if (resource_description.type == "vertex") {
                this.shader = this.resource_manager.engine.gl.createShader(this.resource_manager.engine.gl.VERTEX_SHADER);
            } else {
                this.failed = true;
                return false;// null;
            }
            // parse attributes and uniforms from shader

            this.resource_manager.engine.gl.shaderSource(this.shader, resource_description.data);
            this.resource_manager.engine.gl.compileShader(this.shader);

            if (!this.resource_manager.engine.gl.getShaderParameter(this.shader, this.resource_manager.engine.gl.COMPILE_STATUS)) {
                alert(this.resource_manager.engine.gl.getShaderInfoLog(this.shader));
                this.failed = true;
                return false;// null;
            }
            var attributesRegExp = /^\s*attribute\s[a-z0-9_]*\s*([a-z0-9_]*)\s*exports\(([a-z0-9_]*)\)\s*;\s*/igm;
            var uniformsRegExp = /^\s*uniform\s[a-z0-9_]*\s*([a-z0-9_]*)\s*exports\(([a-z0-9_]*)\)\s*;\s*/igm;
            this.attributes = {};
            this.uniforms = {};
            var res;
            while (res = attributesRegExp.exec(resource_description.data))
            {
                this.attributes[res[1]] = res[2];
            }
            while (res = uniformsRegExp.exec(resource_description.data))
            {
                this.uniforms[res[1]] = res[2];
            }
            return true;
        }
    });

    utils.extend(resources.shader_program, resources.resource, {
        load: function(resource_description, on_finish) {
            this.create_resource(resource_description);
            resources.shader_program.superclass.load.call(this, on_finish);
        },
        create_resource: function(resource_description) {
            this.shader = this.resource_manager.engine.gl.createProgram();

            var attributes = {};
            var uniforms = {};
            for (var i = 0; i < resource_description.link.length; i++)
            {
                var shader = this.resource_manager.get(resource_description.link[i]);
                _.extend(attributes, shader.attributes);
                _.extend(uniforms, shader.uniforms);
                this.resource_manager.engine.gl.attachShader(this.shader, shader.shader);
            }

            this.resource_manager.engine.gl.linkProgram(this.shader);
            if (!this.resource_manager.engine.gl.getProgramParameter(this.shader, this.resource_manager.engine.gl.LINK_STATUS)) {
                alert("Could not initialise shaders");
                this.failed = true;
                return false;
            }
            this.attributes = attributes;
            for (var variable in uniforms)
            {
                this[uniforms[variable]] = this.resource_manager.engine.gl.getUniformLocation(this.shader, variable);
            }

        },
        use:function() {
            this.resource_manager.engine.gl.useProgram(this.shader);
            for (var variable in this.attributes)
            {
                this[this.attributes[variable]] = this.resource_manager.engine.gl.getAttribLocation(this.shader, variable);
                this.resource_manager.engine.gl.enableVertexAttribArray(this[this.attributes[variable]]);
            }
            this.resource_manager.current_shader = this;
        },
        u4fv: function(uniform, val) {
            this.resource_manager.engine.gl.uniformMatrix4fv(uniform, 0, val);
        },
        u3fv: function(uniform, val) {
            this.resource_manager.engine.gl.uniformMatrix3fv(uniform, 0, val);
        },
        u4f: function(uniform, a, b, c, d) {
            this.resource_manager.engine.gl.uniform4f(uniform, a, b, c, d);
        },
        u1f: function(uniform, a) {
            this.resource_manager.engine.gl.uniform1f(uniform, a);
        },
        u1i: function(uniform, a) {
            this.resource_manager.engine.gl.uniform1i(uniform, a);
        },
        u2f: function(uniform, val1, val2) {
            this.resource_manager.engine.gl.uniform2f(uniform, val1, val2);
        },
        bindBuffer: function(attribute, pointer) {
            this.resource_manager.engine.gl.bindBuffer(this.resource_manager.engine.gl.ARRAY_BUFFER, pointer);
            this.resource_manager.engine.gl.vertexAttribPointer(attribute, pointer.itemSize, this.resource_manager.engine.gl.FLOAT, false, 0, 0);
        },
        applyTextureScale: function(texture) {
            this.resource_manager.engine.gl.uniformMatrix2fv(this.tile_scale, 0, texture.tile_scale);
        }

    });

    utils.extend(resources.texture, resources.resource, {
        bind: function(index)
        {
            this.resource_manager.engine.gl.activeTexture(this.resource_manager.engine.gl.TEXTURE0 + index);
            this.resource_manager.engine.gl.bindTexture(this.resource_manager.engine.gl.TEXTURE_2D, this.texture);
            this.resource_manager.engine.gl.uniform1i(this.resource_manager.current_shader['sampler' + index], index);
            if (this.resource_manager.current_shader.tile_scale)
                this.resource_manager.engine.gl.uniformMatrix2fv(this.resource_manager.current_shader.tile_scale, 0, this.tile_scale);
        },
        onload: function(resource_description, image) {
            /*this.resource_manager.engine.gl.bindTexture(this.resource_manager.engine.gl.TEXTURE_2D, this.texture);
            this.resource_manager.engine.gl.pixelStorei(this.resource_manager.engine.gl.UNPACK_FLIP_Y_WEBGL, true);

            this.resource_manager.engine.gl.texImage2D(this.resource_manager.engine.gl.TEXTURE_2D, 0,
                this.resource_manager.engine.gl.RGBA, this.resource_manager.engine.gl.RGBA,
                this.resource_manager.engine.gl.UNSIGNED_BYTE, image);

            this.resource_manager.engine.gl.texParameteri(this.resource_manager.engine.gl.TEXTURE_2D,
                this.resource_manager.engine.gl.TEXTURE_MAG_FILTER, this.resource_manager.engine.gl.LINEAR);

            this.resource_manager.engine.gl.texParameteri(this.resource_manager.engine.gl.TEXTURE_2D,
                this.resource_manager.engine.gl.TEXTURE_MIN_FILTER, this.resource_manager.engine.gl.LINEAR);

            this.resource_manager.engine.gl.bindTexture(this.resource_manager.engine.gl.TEXTURE_2D, null);*/
        },
        load: function(resource_description, on_finish) {
            var image = new Image();
            var self = this;
            image.onload = function () {
                self.create_resource({image: image});
                self.onload(resource_description, image);
                resources.texture.superclass.load.call(self, on_finish);
            };
            image.onerror = function() {
                self.failed = true;
                resources.texture.superclass.load.call(self, on_finish);
            };
            image.src = resource_description.file;
            if (!this.tile_scale)
            this.tile_scale = [
                1, 0,
                0, 1];
        },
        create_resource: function(resource_description) {
            this.texture = this.resource_manager.engine.gl.createTexture();
            this.update_texture(resource_description.image);
        },
        update_texture: function(image) {
            this.resource_manager.engine.gl.bindTexture(this.resource_manager.engine.gl.TEXTURE_2D, this.texture);
            this.resource_manager.engine.gl.pixelStorei(this.resource_manager.engine.gl.UNPACK_FLIP_Y_WEBGL, true);

            this.resource_manager.engine.gl.texImage2D(this.resource_manager.engine.gl.TEXTURE_2D, 0,
                this.resource_manager.engine.gl.RGBA, this.resource_manager.engine.gl.RGBA,
                this.resource_manager.engine.gl.UNSIGNED_BYTE, image);

            this.resource_manager.engine.gl.texParameteri(this.resource_manager.engine.gl.TEXTURE_2D,
                this.resource_manager.engine.gl.TEXTURE_MAG_FILTER, this.resource_manager.engine.gl.LINEAR);

            this.resource_manager.engine.gl.texParameteri(this.resource_manager.engine.gl.TEXTURE_2D,
                this.resource_manager.engine.gl.TEXTURE_MIN_FILTER, this.resource_manager.engine.gl.LINEAR);

            this.resource_manager.engine.gl.texParameteri(this.resource_manager.engine.gl.TEXTURE_2D,
                this.resource_manager.engine.gl.TEXTURE_WRAP_S, this.resource_manager.engine.gl.REPEAT);
            this.resource_manager.engine.gl.texParameteri(this.resource_manager.engine.gl.TEXTURE_2D,
                this.resource_manager.engine.gl.TEXTURE_WRAP_T, this.resource_manager.engine.gl.REPEAT);
            this.resource_manager.engine.gl.bindTexture(this.resource_manager.engine.gl.TEXTURE_2D, null);
        },
        free: function() {

        }
    });

    utils.extend(resources.tileset, resources.texture, {
        onload: function(resource_description, image) {
            this.tile_scale = [
                resource_description.tile[0]/image.width, 0,
                0, resource_description.tile[1]/image.height];
            resources.tileset.superclass.onload.call(this, resource_description, image);
        }
    });

    // Resource manager

    _.extend(resources.resource_manager.prototype, {
        add_path: function(name, path) {
            this.pathes["%" + name + "%"] = path;
        },
        register_resource: function(type, callback) {
            this.types[type] = callback;
        },
        add_namespace: function(name) {
            this.namespaces.push(name);
        },
        exclude_namespace: function(name) {
            this.exclude.push(name);
        },
        get_global_path: function(path) {
            for (var p in this.pathes) {
                path = path.replace(p, this.pathes[p]);
            }
            return path;
        },
        load_resource: function(resource_list, name_prefix) {
            if (!name_prefix) {
                name_prefix = "";
            }
            if (resource_list.type) {
                if (!(resource_list.type in this.types)) {
                    console.log('Unknown resource ', resource_list);
                    return;
                }
                var item = resource_list;
                if (item.file)
                    item.file = this.get_global_path(item.file);
                this.resources.push({
                    name: name_prefix,
                    resource: item
                });
                return;
            }
            for (var name in resource_list) {
                //if (this.engine.exclude.indexOf(name) == -1) {
                    var prefix = name_prefix;
                    //if (this.engine.namespaces.indexOf(name) == -1)
                    prefix += ":" + name;
                    this.load_resource(resource_list[name], prefix);
                //}
            }
            if (name_prefix == "") {
                this.resources.sort(function(a, b) {
                    return priorities.indexOf(a.resource.type) - priorities.indexOf(b.resource.type);
                });
                this.resource_list = {};
                this.loaded = 0;
                this.load_next();
            }
        },
        load_next: function()
        {
            if (this.on_progress) {
                this.on_progress(100*this.loaded/this.resources.length);
            }
            if (this.loaded == this.resources.length) {
                this.loaded = true;

                if (this.on_finish) {
                    this.on_finish(true);
                }

                return;
            }
            var resource_description = this.resources[this.loaded];
            var resource = new this.types[resource_description.resource.type]();
            resource.resource_manager = this;
            resource.load(resource_description.resource, this.load);
        },
        load: function(resource, failed) {
            if (failed)
            {
                if (this.on_finish) {
                    this.on_finish(false);
                }
                console.error(resource);
                return;//#TODO some kind of error
            }
            this.resource_list[this.resources[this.loaded++].name] = resource;
            this.load_next();
        },
        get: function(name) {
            return this.resource_list[name];
        }
    });
    return resources;
});