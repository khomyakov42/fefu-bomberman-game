var vertices = [
    0, 0, 0,
    0, 1, 0,
    1, 1, 0,
    1, 0, 0
];
var texture_coords = [
    0, 1,
    0, 0,
    1, 0,
    1, 1
];

var CELL_SIZE = 10000;
var FPS = 1000/33;
var DF = 33;

var playerColors = [
    [1, 0, 0, 1],
    [0, 0, 1, 1],
    [0, 1, 0, 1],
    [1, 1, 0, 1],
    [1, 0, 1, 1],
    [0, 1, 1, 1],
    [1, 0.5, 0, 1],
    [0.5, 0, 1, 1],
    [0, 1, 0.5, 1],
    [0.5, 1, 0, 1]
];

var objectClass = {
    'free':'.',
    'wall':'#',
    'breakable':'%',
    'exploded':'$',
    'bomb':'b',
    'player':'p'
};

var objectOffset = {
    //'.': 0,
    '#': 1,
    '%': 2,
    '$': 3
};
var extrasOffsets = {
    'b':0,
    'f':1,
    'g':2,
    's':3,
    'k':4,
    't':5,
    'i':6,
    'c':7,
    'a':8,
    'n':9,
    'u':10

};

var Player = function(id, pos, cell_size)
{
    this.id = id;
    this.pos = pos;
    this.cell_size = cell_size;
    this.rect = new cell(this.get_position(), cell_size, objectClass.player);
    this.rect.z = 0.2;
    this.rect.offset = [0, 0];
    this.xSpeed = 0;
    this.ySpeed = 0;
    this.frame = 0;
    this.direction = 0;
    this.lookdirection = 0;
    this.speed = 5;
    this.rect.color = playerColors[this.id];
    this.prevPos = [0, 0];
};
_.extend(Player.prototype, {
    init: function(t) {
        this.rect.init(t);
        this.shader = this.scene.res.g('keyed');
        this.texture = this.scene.res.g('player');
    },
    /*draw:function() {
        this.shader.use();
        this.scene.use();
        this.texture.bind();
        this.rect.draw();
    },*/
    get_position:function() {
        return [(this.pos[0] - 0.5), (this.pos[1] - 0.5)];
    },
    update:function(t, dt, tick) {
        if (this.dead) {
            var i = Math.round((t - this.dead_time)/100);
            var a = 3 - Math.floor(i/3), b = (i%3)+3;
            if (i > 10) {
                b = (i%10 > 5) + 6;
                a = 3;
            }
            this.rect.offset = [a, b];
            this.frame_x = b;
            this.frame_y = a;
            return;
        }
        var speed = [0, 0];
        if (this.dir in playerDirections) {
            this.direction = playerDirections[this.dir];
            speed = directions[this.direction];
            speed = [speed[0]*this.speed, speed[1]*this.speed];
        }
        if (this.lookdir in playerDirections) {
            this.lookdirection = playerDirections[this.lookdir];
        }
        this.xSpeed = speed[0]/CELL_SIZE;
        this.ySpeed = speed[1]/CELL_SIZE;

        this.prevPos = [this.pos[0], this.pos[1]];//#TODO fix this

        if (!this.prevPos || (this.prevPos[0] == 0 && this.prevPos[1] == 0))
            return;
        var dv = [(this.xSpeed * dt) / 1000.0, (this.ySpeed * dt) / 1000.0];

        //if (this.pos[0] >= 0)
        var movement = +(dv[0] == 0);
        var delta = Math.abs(dv[movement]);
        if (delta != 0) {
            this.frame = (Math.floor(t/100.0))%3;
            this.rect.offset = [this.lookdirection, this.frame];
            this.frame_x = this.frame;
            this.frame_y = this.lookdirection;

        }
        else {
            this.frame = 0;
            this.rect.offset = [this.lookdirection, this.frame];
            this.frame_x = this.frame;
            this.frame_y = this.lookdirection;
            return;
        }
        var incr = (dv[movement] > 0)*2 - 1;
        var sign = +(incr < 0);
        var collided = false;
        var offset = [0, 0];
        var slide = false;
        var top = Math.ceil(delta);
        for (var i = 0; i != top; i++)//start from one
        {
            if (slide) {
                this.pos = [this.pos[0] + offset[0], this.pos[1] + offset[1]];
                offset = [0, 0];
                slide = false;
            }
            if (movement == 0)//x
            {
                var nextx = Math.floor(this.prevPos[0] + (i + 1)*incr);
                var newPos = [nextx - 0.5, nextx + 1.5];
                var borders = [this.pos[0] + dv[0] + 0.5, -this.pos[0] - dv[0] + 1.5];
                var updated = false;
                if (!this.map.walkable(nextx, Math.floor(this.prevPos[1])) &&
                    borders[sign] > incr*nextx)
                {
                    this.pos[0] = newPos[sign];
                    collided = true;
                    updated = true;
                    break;
                }
                if (!updated && !this.map.walkable(nextx, Math.floor(this.prevPos[1]) - 1) &&
                    borders[sign] > incr*nextx && this.prevPos[1] - 0.5 < Math.floor(this.prevPos[1]))
                {
                    this.pos[0] = newPos[sign];
                    collided = true;
                    slide = true;
                    updated = true;
                }
                if (!updated && !this.map.walkable(nextx, Math.floor(this.prevPos[1]) + 1) &&
                    borders[sign] > incr*nextx && this.prevPos[1] - 0.5 > Math.floor(this.prevPos[1]))
                {
                    this.pos[0] = newPos[sign];
                    collided = true;
                    slide = true;
                    updated = true;
                }
                if (slide)
                {
                    var d = this.prevPos[1] - 0.5 - Math.floor(this.prevPos[1]);
                    var dsign = (d > 0)*2 - 1;
                    if (d < 0.375)//delta
                    {
                        offset[1] = dsign*(incr*nextx - borders[sign]);
                        if (Math.abs(d) < Math.abs(offset[1]))
                            offset[1] = -d;
                    }
                    continue;
                }
                if (i == top - 1)
                    this.pos[0] += incr*(delta - Math.floor(delta));
                else
                    this.pos[0] += incr;
            }
            else
            {
                var nexty = Math.floor(this.prevPos[1] + (i + 1)*incr);
                var newPos = [nexty - 0.5, nexty + 1.5];
                var borders = [this.pos[1] + dv[1] + 0.5, -this.pos[1] - dv[1] + 1.5];
                var updated = false;
                if (!this.map.walkable(Math.floor(this.prevPos[0]), nexty) &&
                    borders[sign] > incr*nexty)
                {
                    this.pos[1] = newPos[sign];
                    collided = true;
                    updated = true;
                    break;
                }
                if (!updated && !this.map.walkable(Math.floor(this.prevPos[0]) - 1, nexty) &&
                    borders[sign] > incr*nexty && this.prevPos[0] - 0.5 < Math.floor(this.prevPos[0]))
                {
                    this.pos[1] = newPos[sign];
                    collided = true;
                    slide = true;
                    updated = true;
                }
                if (!updated && !this.map.walkable(Math.floor(this.prevPos[0]) + 1, nexty) &&
                    borders[sign] > incr*nexty && this.prevPos[0] - 0.5 > Math.floor(this.prevPos[0]))
                {
                    this.pos[1] = newPos[sign];
                    collided = true;
                    slide = true;
                    updated = true;
                }
                if (slide)
                {
                    var d = this.prevPos[0] - 0.5 - Math.floor(this.prevPos[0]);
                    var dsign = (d > 0)*2 - 1;
                    if (d < 0.375)//delta
                    {
                        offset[0] = dsign*(incr*nexty - borders[sign]);
                        if (Math.abs(d) < Math.abs(offset[0]))
                            offset[0] = -d;
                    }
                    continue;
                }
                if (i == top - 1)
                    this.pos[1] += incr*(delta - Math.floor(delta));
                else
                    this.pos[1] += incr;
            }
        }
        if (slide) {
            this.pos = [this.pos[0] + offset[0], this.pos[1] + offset[1]];
            offset = [0, 0];
            slide = false;
        }


        this.rect.pos = this.get_position();

        this.rect.update(t, dt, tick);
    },
    setPosition: function(x, y, speed, direction, lookdirection, dead) {
        this.dead = dead;
        if (this.dead && !this.dead_time)
        {
            this.dead_time = new Date().getTime();
            if (this.res.g('death'))
                this.res.g('death').play([this.pos[0], this.pos[1], 0]);
        }
        if (!this.dead)
            this.dead_time = undefined;
        this.pos = [x/CELL_SIZE + 0.5, y/CELL_SIZE + 0.5];//#TODO replace with desired pos
        this.rect.pos = this.get_position();
        this.speed = speed*60.0/FPS;
        this.dir = direction;
        this.lookdir = lookdirection;
        if (direction in playerDirections)
        {
            this.rect.offset[0] = playerDirections[lookdirection];
            //this.rect.offset[1] = speed;
        }
    }
});


//new gameObject();
var blockSize = 8;
var floorZ = 0.0;
var bonusZ = 0.1;
var cellTypeCount = 4;
var cellSpeciesCount = 8;
var extrasTypeCount = 16;
var additionalCellCount = 8;

var cellTypeIndex = {
    '.': 0,
    'v': 0,
    '^': 0,
    '<': 0,
    '>': 0,
    '*': 0,
    '#': 1,
    '%': 2,
    '$': 3,
    '-': 4
};

var playerDirections = {
    right: 0,
    up: 1,
    left: 2,
    down: 3,
    none: 4
};

var directions = [[1, 0], [0, -1], [-1, 0], [0, 1], [0, 0]];

var default_cell_size = 10.0;

define(['gameobject', 'resources', 'glview', '_'], function(){
    var Map = {
        init:function(){
            cellPointsBuffer = undefined;
            cellTextureBuffer = undefined;
            /* 2d only!!! */
            /* 2d only!!! */
        },


        map: function(contents, world) {
            this.contents = contents;
            this.new_contents = contents;
            this.width = contents[0].length;
            this.height = contents.length;
            this.world = world;
        },


        movable: function(map, pos) {
            this.map = map;
            this.setPos(pos[0], pos[1]);
            this.frame_x = 0;
            this.frame_y = 0;
        },

        player: function(map, pos, id) {
            Map.player.superclass.constructor.call(this, map, pos);
            this.dir = 0;
            this.direction = [0, 0];
            this.look_dir = 0;
            this.id = id;
        },

        representation: function() {
            this.layers = [];
            this.sectors_count = [0, 0];
        }
    };

    _.extend(Map.representation.prototype, {
        draw: function() {
            for (var i = 0; i < this.layers.length; ++i) {
                this.layers[i].draw();//draw clamped??
            }
        },
        revive: function(field) {
            this.field = field;
            this.sectors_count = [Math.ceil(field.width/this.block_size), Math.ceil(field.height/this.block_size)];
        },
        addLayer: function(layer) {//add layer only after adding to the scene
            layer.scene = this.scene;
            this.layers.push(layer);
        }
    });

    _.extend(Map.map.prototype, {
        setRepresentation: function(representation) {
            this.representation = representation;
            this.layers = representation.layers;//representation.sectors;
        },


        applyContents: function(contents) {
            var changed = false;
            for (var i = 0; i < this.height; ++i) {
                for (var j = 0; j < this.width; ++j){
                    if (this.contents[i][j] != contents[i][j]) {
                        var sy = Math.floor(i/this.representation.block_size), sx = Math.floor(j/this.representation.block_size);
                        var ssy = sy*this.representation.block_size, ssx = sx*this.representation.block_size;

                        if (!changed)
                            this.new_contents = contents;
                        changed = true;



                        for (var k = 0; k < this.layers.length; ++k) {
                            this.layers[k].sectors[sy][sx].applyChange(j, i, contents[i][j]);
                            if (this.layers[k].linked) {
                                var bottom = (i+1)%this.representation.block_size == 0 && (i != (this.height - 1));
                                var top = (i)%this.representation.block_size == 0 && (i > 0);
                                var right = (j+1)%this.representation.block_size == 0 && (j != (this.width - 1));
                                var left = (j)%this.representation.block_size == 0 && (j > 0);
                                if (left)
                                    this.layers[k].sectors[sy][sx - 1].applyChange(j, i, contents[i][j]);
                                if (top)
                                    this.layers[k].sectors[sy - 1][sx].applyChange(j, i, contents[i][j]);
                                if (right)
                                    this.layers[k].sectors[sy][sx + 1].applyChange(j, i, contents[i][j]);
                                if (bottom)
                                    this.layers[k].sectors[sy + 1][sx].applyChange(j, i, contents[i][j]);
                                if (top && right)
                                    this.layers[k].sectors[sy - 1][sx + 1].applyChange(j, i, contents[i][j]);
                                if (bottom && right)
                                    this.layers[k].sectors[sy + 1][sx + 1].applyChange(j, i, contents[i][j]);
                                if (top && left)
                                    this.layers[k].sectors[sy - 1][sx - 1].applyChange(j, i, contents[i][j]);
                                if (bottom && left)
                                    this.layers[k].sectors[sy + 1][sx - 1].applyChange(j, i, contents[i][j]);
                            }
                        }

                    }
                }
            }
            if (changed)
                this.contents = contents;
        },


        walkable: function(x, y) {
            if (x < 0 || y < 0 || x >= this.width || y >= this.height)
                return false;
            var obj = this.contents[y][x];
            return !(obj == objectClass.wall || obj == objectClass.breakable || obj == objectClass.exploded);
        }
    });


    _.extend(Map.movable.prototype, {
        update: function(t, dt, tick) {
            var speed = [0, 0];
            speed = this.direction;
            speed = [speed[0]*this.speed, speed[1]*this.speed];

            this.xSpeed = speed[0]/CELL_SIZE;
            this.ySpeed = speed[1]/CELL_SIZE;

            this.prevPos = [this.pos[0], this.pos[1]];//#TODO fix this

            if (!this.prevPos || (this.prevPos[0] == 0 && this.prevPos[1] == 0))
                return;
            var dv = [(this.xSpeed * dt) / 1000.0, (this.ySpeed * dt) / 1000.0];

            //if (this.pos[0] >= 0)
            var movement = +(dv[0] == 0);
            var delta = Math.abs(dv[movement]);
            //this.frame_y = this.lookdirection;
            /*if (delta != 0) {
                this.frame_x = (Math.floor(t/100.0))%3;
            }
            else {
                this.frame_x = 0;
                return;
            }*/
/*            var incr = (dv[movement] > 0)*2 - 1;
            var sign = +(incr < 0);
            var collided = false;
            var offset = [0, 0];
            var slide = false;
            var top = Math.ceil(delta);
            for (var i = 0; i != top; i++)//start from one
            {
                if (slide) {
                    this.pos = [this.pos[0] + offset[0], this.pos[1] + offset[1]];
                    offset = [0, 0];
                    slide = false;
                }
                if (movement == 0)//x
                {
                    var nextx = Math.floor(this.prevPos[0] + (i + 1)*incr);
                    var newPos = [nextx - 0.5, nextx + 1.5];
                    var borders = [this.pos[0] + dv[0] + 0.5, -this.pos[0] - dv[0] + 1.5];
                    var updated = false;
                    if (!this.map.walkable(nextx, Math.floor(this.prevPos[1])) &&
                        borders[sign] > incr*nextx)
                    {
                        this.pos[0] = newPos[sign];
                        collided = true;
                        updated = true;
                        break;
                    }
                    if (!updated && !this.map.walkable(nextx, Math.floor(this.prevPos[1]) - 1) &&
                        borders[sign] > incr*nextx && this.prevPos[1] - 0.5 < Math.floor(this.prevPos[1]))
                    {
                        this.pos[0] = newPos[sign];
                        collided = true;
                        slide = true;
                        updated = true;
                    }
                    if (!updated && !this.map.walkable(nextx, Math.floor(this.prevPos[1]) + 1) &&
                        borders[sign] > incr*nextx && this.prevPos[1] - 0.5 > Math.floor(this.prevPos[1]))
                    {
                        this.pos[0] = newPos[sign];
                        collided = true;
                        slide = true;
                        updated = true;
                    }
                    if (slide)
                    {
                        var d = this.prevPos[1] - 0.5 - Math.floor(this.prevPos[1]);
                        var dsign = (d > 0)*2 - 1;
                        if (d < 0.375)//delta
                        {
                            offset[1] = dsign*(incr*nextx - borders[sign]);
                            if (Math.abs(d) < Math.abs(offset[1]))
                                offset[1] = -d;
                        }
                        continue;
                    }
                    if (i == top - 1)
                        this.pos[0] += incr*(delta - Math.floor(delta));
                    else
                        this.pos[0] += incr;
                }
                else
                {
                    var nexty = Math.floor(this.prevPos[1] + (i + 1)*incr);
                    var newPos = [nexty - 0.5, nexty + 1.5];
                    var borders = [this.pos[1] + dv[1] + 0.5, -this.pos[1] - dv[1] + 1.5];
                    var updated = false;
                    if (!this.map.walkable(Math.floor(this.prevPos[0]), nexty) &&
                        borders[sign] > incr*nexty)
                    {
                        this.pos[1] = newPos[sign];
                        collided = true;
                        updated = true;
                        break;
                    }
                    if (!updated && !this.map.walkable(Math.floor(this.prevPos[0]) - 1, nexty) &&
                        borders[sign] > incr*nexty && this.prevPos[0] - 0.5 < Math.floor(this.prevPos[0]))
                    {
                        this.pos[1] = newPos[sign];
                        collided = true;
                        slide = true;
                        updated = true;
                    }
                    if (!updated && !this.map.walkable(Math.floor(this.prevPos[0]) + 1, nexty) &&
                        borders[sign] > incr*nexty && this.prevPos[0] - 0.5 > Math.floor(this.prevPos[0]))
                    {
                        this.pos[1] = newPos[sign];
                        collided = true;
                        slide = true;
                        updated = true;
                    }
                    if (slide)
                    {
                        var d = this.prevPos[0] - 0.5 - Math.floor(this.prevPos[0]);
                        var dsign = (d > 0)*2 - 1;
                        if (d < 0.375)//delta
                        {
                            offset[0] = dsign*(incr*nexty - borders[sign]);
                            if (Math.abs(d) < Math.abs(offset[0]))
                                offset[0] = -d;
                        }
                        continue;
                    }
                    if (i == top - 1)
                        this.pos[1] += incr*(delta - Math.floor(delta));
                    else
                        this.pos[1] += incr;
                }
            }
            if (slide) {
                this.pos = [this.pos[0] + offset[0], this.pos[1] + offset[1]];
                offset = [0, 0];
                slide = false;
            }*/
        },
        setPos: function(x, y) {
            this.pos = [x*default_cell_size/CELL_SIZE, y*default_cell_size/CELL_SIZE];
        },
        setSpeed: function(speed) {
            this.speed = speed*60.0/FPS
        },
        setDirection: function(dir) {
            if (dir != 'none')
                this.dir = playerDirections[dir];
            this.direction = directions[playerDirections[dir]];
        }
    });

    extend(Map.player, Map.movable);

    _.extend(Map.player.prototype, {
        setDirection: function(dir) {
            if (this.last_animation != dir && !this.dead) {
                this.last_animation = dir=='none'?'none':dir;
            }
            Map.player.superclass.setDirection.call(this, dir);
        },
        setLookDirection: function(dir) {
            if (this.dead)
                return;
            if (dir != 'none')
                this.look_dir = playerDirections[dir];
        },
        setDead: function(dead) {
            this.dead = dead;
            if (this.dead && !this.dead_time) {
                this.last_animation = 'death';
                this.dead_time = new Date().getTime();
            }
            if (!this.dead)
                this.dead_time = undefined;

        },
        update: function(t, dt, tick) {
            if (this.dead) {
                var i = Math.round((t - this.dead_time)/100);
                var a = 3 - Math.floor(i/3), b = (i%3)+3;
                if (i > 10) {
                    b = (i%10 > 5) + 6;
                    a = 3;
                }
                this.frame_y = a;
                this.frame_x = b;
                return;
            }
            Map.player.superclass.update.call(this, t, dt, tick);
        }

    });

    return Map;
});
