define([
    '_',
    '$',
    'gameobject'
], function(_, $, gameobject) {
    var model = {
        array_buffer: function() {

        },
        index_buffer: function() {

        }
    };

    _.extend(model.array_buffer.prototype, {
        revive: function() {
            this.buffer = this.factory.createBuffer(this.v, this.stride);
        },
        bind: function(attribute) {
            this.factory.engine.gl.bindBuffer(this.factory.engine.gl.ARRAY_BUFFER, this.buffer);
            this.factory.engine.gl.vertexAttribPointer(attribute, this.stride, this.factory.engine.gl.FLOAT, false, 0, 0);
        }
    });
    _.extend(model.index_buffer.prototype, {
        revive: function() {
            this.buffer = this.factory.createIndexBuffer(this.i);
        },
        bind: function() {
            this.factory.engine.gl.bindBuffer(this.factory.engine.gl.ELEMENT_ARRAY_BUFFER, this.buffer);
        }
    });
    return model;
});
