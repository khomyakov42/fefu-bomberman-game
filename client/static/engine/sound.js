var sound_context;
var sound_inited = false;

define([
    '$',
    '_'
], function($, _) {
    var sound_library = {
        sound: function() {
            this.context = sound_library.sound_context;
            this.source = sound_library.sound_context.createBufferSource();
            this.source.loop = false;
        },
        sound_pane: function(context) {
            this.context = context;
        },
        init: function() {
            if (this.inited)
                return;

            var AudioContext = window.AudioContext;
            if (!AudioContext && (typeof webkitAudioContext != "undefined"))
                AudioContext = webkitAudioContext;
            if (!AudioContext && (typeof window.webkitAudioContext != "undefined"))
                AudioContext = window.webkitAudioContext;
            if (!AudioContext) {
                this.inited = false;
                return false;//#TODO ERROR
            }
            this.sound_context = new AudioContext();
            this.mainVolume = this.sound_context.createGainNode();
            this.mainVolume.connect(this.sound_context.destination);

            this.inited = true;
        },
        addVolumeControl: function(name) {
            this[name + '_volume'] = undefined;
        },
        load: function(resource_description, on_finish) {
            var sound = new this.sound(this.sound_context, this.mainVolume);
            sound.failed = true;
            var self = this;
            var request = new XMLHttpRequest();
            request.open('GET', file, true);
            request.responseType = 'arraybuffer';
            request.onload = function(e) {
                if(this.status != 200) {
                    callee.apply(success_callback)
                    return;// null;
                }
/**                    var buffer = self.sound_context.decodeAudioData(this.response, function(buffer) {
                    console.log(buffer);
                });*/
                var buffer = self.sound_context.createBuffer(this.response, false);
                sound.failed = false;
                sound.buffer = buffer;
                //sound.source.noteOn(self.sound_context.currentTime);
                if (callee && success_callback)
                    success_callback.apply(callee, [name, sound]);

            };
            request.send();
        },
        setListenerPosition: function(pos) {
            if (!this.inited)
                return;
            this.sound_context.listener.setPosition(pos[0]/10, pos[1]/10, pos[2]/10);
        }
    };
    _.extend(sound_library.sound.prototype, {
        load: function(resource_description, on_finish) {
            if (!sound_library.inited) {
                on_finish.call(this.resource_manager, this, true);
                return;
            }
            this.failed = true;
            this.volume = sound_library.mainVolume;//#TODO depending on sound type
            var self = this;
            var request = new XMLHttpRequest();
            request.open('GET', resource_description.file, true);
            request.responseType = 'arraybuffer';
            request.onload = function(e) {
                if(this.status != 200) {
                    on_finish.call(self.resource_manager, self, self.failed);
                    return;// null;
                }
                var buffer = sound_library.sound_context.createBuffer(this.response, false);
                self.failed = false;
                self.buffer = buffer;
                //sound.source.noteOn(self.sound_context.currentTime);
                on_finish.call(self.resource_manager, self, self.failed);

            };
            request.send();
        },
        play: function(pos) {
            var source = this.context.createBufferSource();

            if (pos) {
                console.log(pos);
                source.panner = this.context.createPanner();

            }
//            source.volume = this.context.createGainNode();

            source.panner.connect(this.volume);

            source.connect(source.panner);
            //source.volume.connect(source.panner);
            source.buffer = this.buffer;

            source.panner.setPosition(pos[0]/10, pos[1]/10, pos[2]);
            source.noteOn(this.context.currentTime);
        }
    });
    return sound_library;
});