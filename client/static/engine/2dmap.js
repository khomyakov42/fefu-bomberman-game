function extend(child, parent) {
    var F = function(){};
    F.prototype = parent.prototype;
    child.prototype = new F();
    child.prototype.constructor = child;
    child.superclass = parent.prototype;
}

var default_cell_size = 10.0;

define(['_', 'gameobject', 'utils', 'map'], function(_, gameobject, utils, Map){
    var map2d = {
        generateTextureCoords: function(hsize, offset, vsize) {
            var result = [];
            var world0 = offset/vsize;
            var world1 = (offset + 1)/vsize;
            for (var i = 0; i < hsize; ++i) {
                var tex_point = [];

                tex_point.push((i + 0)/hsize, world1);
                tex_point.push((i + 1)/hsize, world1);
                tex_point.push((i + 1)/hsize, world0);
                tex_point.push((i + 0)/hsize, world0);

                result.push(tex_point);
            }
            var tex_empty = [];
            for (var i = 0; i < 4; i++)
                tex_empty.push(-1, -1);
            result.push(tex_empty);
            return result;
        },
        layer: function(cell) {
            this.cell = cell;
        },
        ground_layer: function() {
            map2d.ground_layer.superclass.constructor.call(this, map2d.ground_sector)
        },
        dynamic_layer: function() {
            map2d.dynamic_layer.superclass.constructor.call(this, map2d.dynamic_sector)
        },
        dynamic_sector: function(x, y, layer) {
            this.tex_points = [];
            this.size = 0;
            this.vertices = [];
            this.textures = [];
            this.items = layer.items;
            this.changed = true;
            map2d.dynamic_sector.superclass.constructor.call(this, x, y, layer);
        },
        base_sector: function(x, y, layer) {
            this.x = x;
            this.y = y;

            this.layer = layer;

            this.rx = x*this.layer.representation.default_cell_size;
            this.ry = y*this.layer.representation.default_cell_size;
            this.changed = false;
            this.block_size = this.layer.representation.block_size;

            this.field = layer.representation.field;

            for (var i = 0; i < this.block_size; ++i) {
                for (var j = 0; j < this.block_size; ++j) {
                    var cell = undefined;
                    if (j+x < this.field.width && i+y < this.field.height)
                        cell = this.field.contents[i + y][j + x];
//                    console.log(j+x, i+y);
                    this.addItem(j, i, cell);
                }
            }

            this.mvMatrix = mat4.create();
        },
        ground_sector: function(x, y, layer) {
            this.tex_points = [];
            map2d.ground_sector.superclass.constructor.call(this, x, y, layer);
            this.tex_buffer = this.layer.factory.createBuffer(this.tex_points, 2, true);
        },


        representation2d: function() {
            map2d.representation2d.superclass.constructor.call(this);
        }
    };

    _.extend(map2d.layer.prototype, {
        bind: function() {
            this.texture.bind(0);
            this.factory.engine.gl.bindBuffer(this.factory.engine.gl.ELEMENT_ARRAY_BUFFER, this.representation.ind_buffer);
        },
        draw: function() {
            this.representation.scene.use(this.shader);

            this.bind();

            for (var y = 0; y < this.representation.sectors_count[1]; ++y) {
                for (var x = 0; x < this.representation.sectors_count[0]; ++x) {
                    this.sectors[y][x].draw();
                }
            }
        },
        revive: function(representation) {
            this.representation = representation;
            this.sectors = [];
            if (this.init)
                this.init();
            for (var y = 0; y < this.representation.sectors_count[1]; ++y) {
                var line = [];
                for (var x = 0; x < this.representation.sectors_count[0]; ++x) {
                    line.push(new this.cell(x*representation.block_size, y*representation.block_size, this));
                }
                this.sectors.push(line);
            }
        }
    });


    _.extend(map2d.base_sector.prototype, {
        draw: function() {
            this.layer.representation.scene.getViewMatrix(this.mvMatrix);
            if (this.layer.flip)
                mat4.rotateX(this.mvMatrix, Math.PI);
            mat4.translate(this.mvMatrix, [
                this.rx,
                this.ry,
                this.layer.depth
            ]);

            this.layer.shader.u4fv(this.layer.shader.view_matrix, this.mvMatrix);
        },
        addItem: function(x, y, cell) {

        }
    });

    extend(map2d.ground_layer, map2d.layer);
    extend(map2d.dynamic_layer, map2d.layer);

    extend(map2d.ground_sector, map2d.base_sector);
    extend(map2d.dynamic_sector, map2d.base_sector);

    _.extend(map2d.ground_layer.prototype, {
        bind: function() {
            map2d.ground_layer.superclass.bind.call(this);
            this.shader.bindBuffer(this.shader.vertex, this.pos_buffer);
        },
        revive: function(representation) {
            var pos_points = representation.pos_points.concat.apply([], representation.pos_points);

            this.pos_buffer = this.factory.createBuffer(pos_points, 3);

            this.tex_points = map2d.generateTextureCoords(this.items_count, representation.field.world, this.types_count);

            map2d.ground_layer.superclass.revive.call(this, representation);
        }
    });

    _.extend(map2d.dynamic_layer.prototype, {
        revive: function(representation) {
            this.pos_points = representation.pos_points;

            this.tex_points = map2d.generateTextureCoords(this.items_count, 0, this.types_count);

            map2d.dynamic_layer.superclass.revive.call(this, representation);
//            map2d.dynamic_layer.superclass.constructor.call(this, map2d.dynamic_sector, representation, depth)
        }
    });

    _.extend(map2d.dynamic_sector.prototype, {
        build_buffers: function() {
            var vertices = this.vertices.concat.apply([], this.vertices);
            var textures = this.textures.concat.apply([], this.textures);

            if (!this.pos_buffer)
                this.pos_buffer = this.layer.factory.createBuffer(vertices, 3);
            else
                this.layer.factory.updateBuffer(this.pos_buffer, vertices);

            if (!this.tex_buffer)
                this.tex_buffer = this.layer.factory.createBuffer(textures, 2);
            else
                this.layer.factory.updateBuffer(this.tex_buffer, textures);

            //console.log(textures, vertices);
        },
        draw: function() {
            if (this.changed) {
                this.changed = false;
                this.build_buffers();
                //console.log(this.size);
            }
            if (!this.pos_buffer || !this.tex_buffer)
                return;
            map2d.ground_sector.superclass.draw.call(this);

            this.layer.shader.bindBuffer(this.layer.shader.texture, this.tex_buffer);
            this.layer.shader.bindBuffer(this.layer.shader.vertex, this.pos_buffer);

            this.layer.factory.engine.gl.drawElements(this.layer.factory.engine.gl.TRIANGLES, this.size*6,
                this.layer.factory.engine.gl.UNSIGNED_SHORT, 0);
        },
        addItem: function(x, y, cell) {
            this.vertices.push([]);
            this.textures.push([]);
            if (!cell || !(cell in this.items))
                return;
            var index = this.items[cell], pos = x + y*this.block_size;
            this.vertices[pos] = this.layer.representation.pos_points[pos];
            this.textures[pos] = this.layer.tex_points[index];
            this.changed = true;
            this.size++;
        },
        applyChange: function(x, y, cell) {
            var pos = x - this.x + (y - this.y)*this.block_size;
            if (!cell || !(cell in this.items)) {
                if (this.vertices[pos].length) {
                    this.changed = true;
                    this.vertices[pos] = [];
                    this.textures[pos] = [];
                    this.size--;
                }
                return;
            }
            if (!this.vertices[pos].length)
                this.size++;
            var index = this.items[cell];
            //console.log(this.size, this.items[cell], ": ", cell);
            this.vertices[pos] = this.layer.representation.pos_points[pos];
            this.textures[pos] = this.layer.tex_points[index];
            this.changed = true;
        }
    });

    _.extend(map2d.ground_sector.prototype, {
        draw: function() {
            if (this.changed) {
                this.changed = false;
                this.layer.factory.subBuffer(this.tex_buffer, this.tex_points);
            }
            map2d.ground_sector.superclass.draw.call(this);

            this.layer.shader.bindBuffer(this.layer.shader.texture, this.tex_buffer);

            this.layer.factory.engine.gl.drawElements(this.layer.factory.engine.gl.TRIANGLES,
                this.layer.representation.ind_buffer.numItems, this.layer.factory.engine.gl.UNSIGNED_SHORT, 0);
        },
        addItem: function(i, j, cell) {
            var cell = cell;// ?!
            if (!cell || !(cell in this.layer.items))
                cell = '-';
            this.tex_points.push.apply(this.tex_points, this.layer.tex_points[this.layer.items[cell]]);
        },
        applyChange: function(x, y, cell) {
            var tx = x - this.x, ty = y - this.y;
            var count = 4*2;
            var cell = cell;
            if (!(cell in this.layer.items))
                cell = '-';
            this.tex_points.splice.apply(this.tex_points, [(tx + ty*this.block_size)*count, count].concat(this.layer.tex_points[this.layer.items[cell]]));
            this.changed = true;
        }
    });

    utils.extend(map2d.representation2d, Map.representation, {
        revive: function(field) {
            map2d.representation2d.superclass.revive.call(this, field);
            this.pos_points = [];
            for (var y = 0; y < this.block_size; ++y) {
                for (var x = 0; x < this.block_size; ++x) {
                    var pos_point = [];
                    pos_point.push((x + 0)*this.default_cell_size, (y + 0)*this.default_cell_size, 0.0);
                    pos_point.push((x + 1)*this.default_cell_size, (y + 0)*this.default_cell_size, 0.0);
                    pos_point.push((x + 1)*this.default_cell_size, (y + 1)*this.default_cell_size, 0.0);
                    pos_point.push((x + 0)*this.default_cell_size, (y + 1)*this.default_cell_size, 0.0);
                    this.pos_points.push(pos_point);
                }
            }
            //creating index buffer
            var ind_points = [];
            for (var i = 0; i < this.block_size*this.block_size; ++i) {
                ind_points.push(i*4 + 0);
                ind_points.push(i*4 + 1);
                ind_points.push(i*4 + 2);

                ind_points.push(i*4 + 0);
                ind_points.push(i*4 + 2);
                ind_points.push(i*4 + 3);
            }

            this.ind_buffer = this.factory.createIndexBuffer(ind_points);
        }
    });

    return map2d;
});