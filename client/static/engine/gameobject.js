function extend(child, parent) {
    var F = function(){};
    F.prototype = parent.prototype;
    child.prototype = new F();
    child.prototype.constructor = child;
    child.superclass = parent.prototype;
}

define(['gl_matrix', 'glview','resources'], function(){
    var gobject = {
        init:function(gl) {
            this.gl = gl;
        },
        createBuffer: function(array, basis, dynamic) {
            var result = this.gl.createBuffer();
            this.gl.bindBuffer(this.gl.ARRAY_BUFFER, result);
            this.gl.bufferData(this.gl.ARRAY_BUFFER, new Float32Array(array), dynamic?this.gl.DYNAMIC_DRAW:this.gl.STATIC_DRAW);
            result.itemSize = basis;
            result.numItems = array.length/basis;

            return result;
        },
        createIndexBuffer: function(array) {
            var result = this.gl.createBuffer();
            this.gl.bindBuffer(this.gl.ELEMENT_ARRAY_BUFFER, result);
            this.gl.bufferData(this.gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(array), this.gl.STATIC_DRAW);
            result.itemSize = 1;
            result.numItems = array.length;
            return result;
        },
        subBuffer: function(buffer, array) {
            this.gl.bindBuffer(this.gl.ARRAY_BUFFER, buffer);
            this.gl.bufferSubData(this.gl.ARRAY_BUFFER, 0, new Float32Array(array));
            buffer.numItems = array.length/buffer.itemSize;
        },
        updateBuffer: function(buffer, array) {
            this.gl.bindBuffer(this.gl.ARRAY_BUFFER, buffer);
            this.gl.bufferData(this.gl.ARRAY_BUFFER, new Float32Array(array), this.gl.DYNAMIC_DRAW);
            buffer.numItems = array.length/buffer.itemSize;
        },



        object: function(pos, texture, shader) {
            this.pos = pos;
            this.texture = texture;
            this.shader = shader;
        }

    };

    return gobject
});
