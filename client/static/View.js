define(['bn', '_', '$'], function(bn, _, $){
    var View = bn.View.extend({
        template: '',

        ready: function() { },

        render: function(callback) {
            if (!this.template) throw "No template name";
            var self = this;

            _.template(this.template, {self: this}, function(error, html) {
                $(self.el).html(html);
                callback.apply(this, arguments);
            });
        },
        free: function() {}
    });

    return View;
});
